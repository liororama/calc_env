"""
This script runs under a resource allocation and actually runs stages of a calculation.

The command line arguments are:
sys.argv[1] - path to calc_env environment
sys.argv[2] - the id of the calculation to run
sys.argv[3:] - names of stages to do
All values are wrapped by repr(x), and unwrapped by eval(x, {}, {})

"""

import sys
import os

print "PYTHONPATH:      {0!r}".format(os.environ.get("PYTHONPATH"))
print "sys.path:        {0!r}".format(sys.path)

scompy_src_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(scompy_src_dir)

from scompy.environment import Environment
from scompy.calculation import Calculation

if __name__ == '__main__':
    print "============= calc_env_runner.py starting ============="
    print "PYTHONPATH:      {0!r}".format(os.environ.get("PYTHONPATH"))
    print "__file__:        {0!r}".format(__file__)
    print "__package__:     {0!r}".format(__package__)
    print "sys.argv:        {0!r}".format(sys.argv)

    calc_env_path, calc_id = (eval(x, {}, {}) for x in sys.argv[1:3])
    stage_list = list(eval(x, {}, {}) for x in sys.argv[3:])

    print "calc_env_path:   {0!r}".format(calc_env_path)
    print "calc_id:         {0!r}".format(calc_id)
    print "stages:           {0!r}".format(repr(stage_list))

    env = Environment(calc_env_path)
    calc = Calculation.from_exisiting(calc_id)
    resource_allocation = env.rm.get_current_allocation()
    calc.do_stages(stage_list, resource_allocation)
