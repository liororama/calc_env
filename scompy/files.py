"""
"""

import os
from os.path import join, isfile


class NoSuchFile(Exception):
    pass


class Files(object):
    def __init__(self, path):
        self.path = os.path.realpath(path)

    def __getitem__(self, key):
        fn = join(self.path, key)
        if not isfile(fn):
            raise NoSuchFile(fn)
        else:
            return open(fn).read()

    def __setitem__(self, key, value):
        fn = join(self.path, key)
        open(fn, 'w').write(value)
