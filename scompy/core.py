"""
"""

import datetime
from dateutil import tz

from externals.trac_core import Interface
from structures import CalcRecord, RMJobStatus


# core.now should always be used to get current time, this way we can later
# change the way we store times.
now = datetime.datetime.utcnow

NODE_SEPARATOR = '/'


def to_local_timezone(dt):
    """
    Convert from whatever is returned by core.now (or None), to something in the
    local timezone

    """
    if dt is None:
        return None
    else:
        return dt.replace(tzinfo=tz.gettz('UTC')).astimezone(tz.tzlocal())


class ICalcDBAdmin(Interface):
    """
    Provide a database admin implementation
    """

    def init_db(self, **kwargs):
        """
        Initialize the db from scratch.
        """


class HierarchyError(Exception):
    pass


class CalculationRemoveError(Exception):
    pass


class ICalcDB(Interface):
    """
    Provide a database implementation.
    """

    def connect(self, **kwargs):
        """
        Connect to the db.
        """

    def insert_calc(self, calc_record, hpath=None):
        """
        Insert a new calculation record to the database and return it.

        calc_record.id is ignored and overwritten in place.
        calc_record.parent_id must be the id of an existing calculation or None,
        otherwise a ValueError will be raised.

        hpath can be used to relate the calculation with a specific hierarchy
        node. hpath=None means the calculation isn't related to a hierarchy
        node. HierarchyError is raised if hpath doesn't exist.

        """

    def get_calc(self, calc_id):
        """
        Get a CalcRecord for the given calc_id and return it.

        Raise ValueError if not found.

        """

    def update_calc(self, calc_record):
        """
        Update the database according to the given record.

        The id of the given record is used to identify the record to update.
        ValueError is raised if calc_record.id or calc_record.parent_id are not
        id's of existing calculations, or if they are equal (a calculation
        cannot be it's own parent).

        """

    def remove_calc(self, calc_id):
        """
        Remove a calculation from the database.

        Raises ValueError if calc_id isn't the id of an existing calculation.
        Raises CalculationRemoveError if trying to remove a calculation that has
        already been cloned.

        """

    def get_stages(self, calc_id):
        """
        Get a stages dictionary for all stages of a ginev calculation.

        The result is a dictionary mapping stage names to StageRecord objects.
        Raise ValueError if the calc_id isn't valid, and return an empty
        dictionary if there are no stages for an existing calculation.

        """

    def set_stage(self, stage_record):
        """
        Insert or update the database according to the given stage record, and return it.

        The calc_id and name of the given record are used to identify the record
        to update. ValueError is raised if stage_record.calc_id is not the id of
        an existing calculation. A new stage record will be inserted to the
        database in case it does not already exist.

        """

    def delete_stage(self, stage_record):
        """
        Delete a stage from the database according to the given stage record.

        The name of the stage and the id of the calculation can be used to identify
        the stage. ValueError is raised in case the stage an not be found.
        """

    def lock(self, calc_id):
        """
        Try to lock a calculation, and return success status.

        This method returns instantly, i.e. does not wait for a lock, and
        returns True if the lock was successfully obtained, and False if not.

        Raises ValueError if calc_id isn't the id of an existing calculation.

        """

    def unlock(self, calc_id):
        """
        Unlock the calculation and return instantly.

        This does nothing if it isn't locked.

        Raises ValueError if calc_id isn't the id of an existing calculation.

        """

    def all_calculations(self, user=None):
        """
        Return a generator of CalcRecord objects of all calculations in the database.
        """

    def locked_calculations(self):
        """
        Return a list of currently locked calculations
        """

    def new_node(self, hpath, create_parents=False):
        """
        Create a new node in the hierarchy.

        hpath is the full hierarchy path to the node.
        If create_parents is False, the operation will raise HierarcyError if the
        parent doesn't exist. If create_parents is True, all needed parents will
        be created.

        """

    def move_node(self, src, dest, create_parents=False):
        """
        Move a node to a new name, and possibly a new parent.

        The new parent must already exist, unless create_parents is True, and
        then it will be created.

        """

    def remove_node(self, hpath, remove_children=False):
        """
        Remove a node.

        If the node has children, HierarchyError will be raised unless
        remove_children is True, in which case all the subtree will be removed
        recursively.

        Calculations associated with the nodes will not be removed, but will
        become node-less.

        """

    def list_node_children(self, hpath):
        """
        Return an iterator of names of children of the given node.

        node is given by full hierarchy path, and the results are child names,
        relative to the given node. Raises HierarcyError if hpath doesn't exist.

        """

    def get_node_from_calc(self, calc_record):
        """
        Returns the hpath to the node that the given calc is related to.

        Returns None if the given calculation isn't related to any hierarchy
        node.

        """

    def get_calc_from_node(self, hpath):
        """
        Returns the CalcRecord of the calculation related to the given hpath.

        None is returned if the given hpath isn't related to any node.
        HierarcyError is raied if the given  hpath doesn't exist.

        """


class ResourceManagerError(Exception):
    pass


class IRMJob(object):
    """
    Interface provided by resource manager job object.
    """

    def to_string(self):
        """
        Convert to string representation and return it.

        Result can be transferred to IResourceManager.job_from_string.

        """

    def get_status(self):
        """
        Query the resource manager for status, and return RMJobStatus value.
        """

    def kill(self):
        """
        Kill a live job.

        Raise ResourceManagerError (or any exeption inheriting from it) on error.

        """

    def suspend(self):
        """
        Suspend a running job.

        Raise ResourceManagerError (or any exeption inheriting from it) on error.

        """

    def resume(self):
        """
        Resume a suspended job.

        Raise ResourceManagerError (or any exeption inheriting from it) on error.

        """


class IResourceAllocation(object):
    """
    Interface provided by resource manager resource allocation object.

    Resource allocation objects are given to ICalc.run implementations.

    """

    def get_hosts(self):
        """
        Return a sequence or generator of allocated host names.

        Items in the return value represent process allocations. So if the
        resource allocation has 4 processes, two on node1 and two on node2, the
        result should be: ('node1', 'node1', 'node2', 'node2')

        """

    def shrink(self, np=1):
        """
        Shrink the reseource allocation to the given number of processes.
        """


class NotInResourceAllocationError(ResourceManagerError):
    pass


class IResourceManager(Interface):
    """
    Provide a resource manager implementation.
    """

    def get_current_allocation(self):
        """
        Return a resource allocation object for the current allocation.

        Assume we are in a resource allocation, and obtain the
        allocation information from environmnet variables. Raise
        NotInResourceAllocationError if called while not in a resource
        allocation.

        The returned object implements IResourceAllocation.

        """

    def submit(self, cmd, np, work_dir, output_filename, **extra):
        """
        Submit a job to be run and return a resource manager job object.

        cmd - A sequence of program arguments (the first is the program to run).
        np - Number of processes required.
        work_dir - The working dir in which the job should be started. cmd is
                   also looked in this directory.
        output_filename - The running job's stdout and stderr will be directed
                          to this file (relative to work_dir).
        extra - Extra arguments specific to a resource manager implementation
        (for example, partition, account, etc...)

        The returned object implements IRMJob.
        """

    def job_from_string(self, s):
        """
        Return a resource manager job object created from a string representation.

        Raise ValueError if the string can not be converted to a leagal job.

        """


class ICalcFS(Interface):
    """
    Provide file system related functionality.
    """
    def new_calc_dir(self, calc_id):
        """
        Create a new calculation directory and return the path to it.
        """


class Stage(object):
    """
    Base class for classes that define a stage in a calculation

    A stage implementation should be stateless.
    Methods are called with the current working directory set to the calculation directory.

    """

    def get_np(self):
        """
        Return the number processes needed for the stage.
        """
        return 1

    # TODO should we put get_extra_rm_info here and not in ICalc?

    def go(self, resource_allocation):
        """
        Perform the stage, and return success status.

        This method can either be called locally, in which case resource_allocation
        will be None, or called from within the resource manager allocation environment
        (e.g. SLURM_HOST.. is defined), in which case resource_allocation will
        be an object implementing IResourceAllocation. This method is blocking
        and should return only when the stage is done or if it fails.

        The return value should be True if the stage finished successfully, or
        False if the stage failed.

        Note that this method can do just about anything, including to create
        and launch new calculations using the calc_env Python API.

        """
        return True

    def reset(self, status):
        """
        Reset the stage, and return True/False result

        This hook allows the calcspec writer to perform calculation operations
        in order to allow the stage to "go" again. For example calling "make clean"
        """
        return True

    # TODO prerequisites?

    # TODO cancel


class CalcSpecError(Exception):
    pass


class ICalc(object):
    """
    Define a specific calculation for the core of calc_env.

    Each calculation specification should define a single ICalc implementation.
    An ICalc implementation should be stateless.
    Methods are called with the current working directory set to the calculation directory.

    Apart from these methods, an ICalc implementation would have several stages,
    which are nested classes that are subclasses of Stage.

    """

    def get_extra_rm_info(self, extra_rm_info):
        """
        Process extra_rm_info and return the extra_rm_info that should be passed to IResourceManager.submit.

        For example, setting slurm_account if it is not set.

        """

    def clone(self, source_dir):
        """
        Clone the calculation directory from source_dir.

        source_dir should exist.

        Assume that the clone can not write inside source_dir (it may be that
        user A clones a calculation owned by user B).

        TODO: what to return

        """

    def reset(self, status):
        """
        Reset the calculation.

        This method is called when a reset operation takes place. A reset
        operation changes the calculation status back to IN_PREPERATION from
        either DONE, FAILED or CANCELED. This method is called just before reset
        operation and its return value is used to determine if the operation
        should be allowed.

        In case this operation is allowd this method can also cleanup the
        calculation directory, for example removing result files.

        If an exception is raised by this method the reset operation will not be
        performed and the calculation status will remain as it was.

        """

    def get_go_sequence(self):
        """
        A go squence is a sequence of stages to run (call their go method) in order
        to run the calculation. The go sequence is made of two lists, the first, is
        the stages to run localy (in a blocking mode) the second is the stages to run
        via the resource manager (in a non blocking mode)

        Example:
         [["stage1"], ["stage2", "stage3"]]
        stage1 should run in a blocking mode (localy), and stage2 and stage3 should run
        in a non blocking mode (via the RM). The order the stages should run is stage1, then
        stage2 and finally stage3.

        """

    def get_status_line(self):
        """
        Return a string with status information.

        For example: Current timestep and physical time.

        """
