
import sys
import os
from pprint import pformat
import subprocess
import time


from scompy.core import Stage


class Calc(object):
    """
    An ICalc implementation for testing purposes.

    """

    def get_extra_rm_info(self, extra_rm_info):
        """
        Process extra_rm_info and return the extra_rm_info that should be passed to IResourceManager.submit.
        """
        if 'job_name' in extra_rm_info:
            extra_rm_info['job_name'] += '2_3' # to get 'testing_1_2_3'
        else:
            extra_rm_info['job_name'] = 'test_calc'

        return extra_rm_info

    def clone(self, source_dir):
        """
        Clone the calculation directory to target_dir.
        """
        target_dir = os.getcwd()
        os.chdir(source_dir)
        retcode = subprocess.call("cp -f * " + target_dir, shell=True)
        os.chdir(target_dir)
        return True

    def get_go_sequence(self):
        return [["stage1"], ["stage2", "stage3"]]

    def get_status_line(self):
        return "OK"

    class stage1(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            for i in range(1,3):
                print "Stage 1 - i = ", i
                time.sleep(1)
            return True

        def reset(self, status):
            return True

    class stage2(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            for i in range(1,10):
                print "Stage 2 - i = ", i
                time.sleep(1)
            return True

        def reset(self, status):
            return True

    class stage3(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            for i in range(1,15):
                print "Stage 3 - i = ", i
                time.sleep(1)
            return True

        def reset(self, status):
            return True

    class stage4(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            for i in range(1,2):
                print "Stage 4 - i = ", i
                time.sleep(1)
            return True

        def reset(self, status):
            return True
