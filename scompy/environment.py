"""
"""
import os
import sys
import logging
import socket
import pickle

import structures
# TODO change the * to the necessary Classes
from externals.trac_core import *
from externals.trac_config import *
from externals.trac_log import logger_handler_factory

from core import *
from loader import load_components


CONFIG_FILENAME = 'config.ini'

# The name of the python module expected to be present in a calculation
# directory. Assuming the file ends with .py suffix
CALCSPEC_MODULE_NAME = 'calc'

# Name of class expected to be in the calcspec module file.
CALCSPEC_CLASS_NAME = 'Calc'

# Name of subdirectory of calculation directories that contains calcspec and other calc_env specific files
CALC_INTERNAL_DIR_NAME = 'scompy_internal'

# The python script used to run calculations inside the rm
CALC_ENV_RUNNER = 'calc_env_runner.py'

# Name of plugins directory in the environment directory
ENV_PLUGINS_DIR = 'plugins'


def get_calcspec_internal_path():
    return os.path.join(CALC_INTERNAL_DIR_NAME, CALCSPEC_MODULE_NAME + ".py")


class BasicEnvironment(Component, ComponentManager):
    """
    This class is the necessary parts from Trac's Environment class
    """

    components_section = ConfigSection('components',
        """This section is used to enable or disable components
        provided by plugins, as well as by Trac itself. The component
        to enable/disable is specified via the name of the
        option. Whether its enabled is determined by the option value;
        setting the value to `enabled` or `on` will enable the
        component, any other value (typically `disabled` or `off`)
        will disable the component.

        The option name is either the fully qualified name of the
        components or the module/package prefix of the component. The
        former enables/disables a specific component, while the latter
        enables/disables any component in the specified
        package/module.

        Consider the following configuration snippet:
        {{{
        [components]
        trac.ticket.report.ReportModule = disabled
        webadmin.* = enabled
        }}}

        The first option tells Trac to disable the
        [wiki:TracReports report module].
        The second option instructs Trac to enable all components in
        the `webadmin` package. Note that the trailing wildcard is
        required for module/package matching.

        To view the list of active components, go to the ''Plugins''
        page on ''About Trac'' (requires `CONFIG_VIEW`
        [wiki:TracPermissions permissions]).

        See also: TracPlugins
        """)

    # Class parameters from the config
    log_file = Option('logging', 'log_file', 'trac.log',
                """If `log_type` is `file`, this should be a path to the
                log-file. Relative paths are resolved relative to the `log`
                directory of the environment.""")

    log_level = Option('logging', 'log_level', 'WARN',
                """Level of verbosity in log.

                Should be one of (`CRITICAL`, `ERROR`, `WARN`, `INFO`, `DEBUG`).""")

    def init(self, path):
        """
        read config file and init members
        """
        ComponentManager.__init__(self)

        self.path = os.path.realpath(path)
        config_filename = os.path.join(self.path, CONFIG_FILENAME)
        if not os.path.isfile(config_filename):
            raise ConfigurationError("Configuration file {0!r} not found.".format(config_filename))
        self.config = Configuration(config_filename)

        # The regular logger for debugging and general information
        self.log, stdout_hndlr = logger_handler_factory(logtype='stdout', logfile=self.log_file, level=self.log_level, logid="env")

        self.log.debug('-' * 20 + ' environment startup ' + '-' * 32)
        self.log.debug("Environment path = {0}".format(self.path))
        self.log.debug("Log file was found at: {0}".format(config_filename))

        # Load plugins from the plugins directory
        plugins_dirs = [os.path.join(self.path, ENV_PLUGINS_DIR),
                        os.path.join(os.path.dirname(__file__), ENV_PLUGINS_DIR)]
        self.log.debug("Plugins dir [{0}]".format(plugins_dirs))
        load_components(self, plugins_dirs)

    def set_log_level(self, lvl):
        """ Set log level of calc_env """
        self.log.setLevel(lvl)

    def get_plugins_dir(env):
        """
        Return the path to the `plugins` directory of the environment.
        """
        plugins_dir = os.path.realpath(os.path.join(env.path, ENV_PLUGINS_DIR))
        return os.path.normcase(plugins_dir)

    def component_activated(self, component):
        """Initialize additional member variables for components.

        Every component activated through the `Environment` object
        gets three member variables: `env` (the environment object),
        `config` (the environment configuration) and `log` (a logger
        object)."""
        component.env = self
        component.config = self.config
        component.log = self.log

    def _component_name(self, name_or_class):
        name = name_or_class
        if not isinstance(name_or_class, basestring):
            name = name_or_class.__module__ + '.' + name_or_class.__name__
        return name.lower()

    @property
    def _component_rules(self):
        try:
            return self._rules
        except AttributeError:
            self._rules = {}
            for name, value in self.components_section.options():
                if name.endswith('.*'):
                    name = name[:-2]
                self._rules[name.lower()] = value.lower() in ('enabled', 'on')
            return self._rules

    def is_component_enabled(self, cls):
        """Implemented to only allow activation of components that are
        not disabled in the configuration.

        This is called by the `ComponentManager` base class when a
        component is about to be activated. If this method returns
        `False`, the component does not get activated. If it returns
        `None`, the component only gets activated if it is located in
        the `plugins` directory of the environment.
        """

        # Right now we just return True, to enable all components.
        # TODO: Think about disabling of compponents. Do we need it? If not,
        # remove all code related to it. If we do, then implement what we need.
        return True

        component_name = self._component_name(cls)

        # Disable the pre-0.11 WebAdmin plugin
        # Please note that there's no recommendation to uninstall the
        # plugin because doing so would obviously break the backwards
        # compatibility that the new integration administration
        # interface tries to provide for old WebAdmin extensions
        if component_name.startswith('webadmin.'):
            self.log.info("The legacy TracWebAdmin plugin has been "
                          "automatically disabled, and the integrated "
                          "administration interface will be used "
                          "instead.")
            return False

        rules = self._component_rules
        cname = component_name
        while cname:
            enabled = rules.get(cname)
            if enabled is not None:
                return enabled
            idx = cname.rfind('.')
            if idx < 0:
                break
            cname = cname[:idx]

        # By default, all components in the trac package are enabled
        return component_name.startswith('trac.') or None

    def enable_component(self, cls):
        """Enable a component or module."""
        self.log.info("Enabling component {0}".format(str(cls)))
        self._component_rules[self._component_name(cls)] = True


class Environment(BasicEnvironment):
    """
    A singleton object that provides all other main objects (IResourceManager, ICalcDB, ICalcFS).

    This object is the only one that access the specific site configuration file.

    Members:
    rm - resource manager instance
    db - calculation db manager instance
    fs - calculation filesystem manager instance

    """

    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            env = super(Environment, cls).__new__(cls, *args, **kwargs)
            env.init(*args, **kwargs)
            cls._instance = env  # only if no exceptions were raised above
        else:
            assert len(args) == len(kwargs) == 0, (
                "Tried to create a new Environment when there is an existing one without calling reset_singleton")
        return cls._instance

    @classmethod
    def reset_singleton(cls):
        """
        Throw away the old _instance. Use with care (only use should be for testing).
        """
        if cls._instance is not None:
            cls._instance.db.disconnect()
            cls._instance = None

    db = ExtensionOption('calc_env', 'db', ICalcDB, None,
                         """Name of the component implementing `ICalcDB`, which is used
                         for db operations.""")

    db_admin = ExtensionOption('calc_env', 'db_admin', ICalcDBAdmin, 'MySQLCalcDBAdmin',
                         """Name of the component implementing `ICalcDBAdmin`, which is used
                         for db management operations.""")

    rm = ExtensionOption('calc_env', 'rm', IResourceManager, None,
                         """Name of the component implementing `IResourceManager`, which is used
                         for accessing the resource manager of the cluster.""")

    fs = ExtensionOption('calc_env', 'fs', ICalcFS, None,
                         """Name of the component implementing `ICalcFS`, which
                         is used to manage the calculations directory tree on the
                         filesystem.""")

    # Event logging parameters
    event_log_syslog_facility = Option('event_log', "syslog_facility", None,
                            """ This parameter set the log facility to use when sending
                            high level events notifications to syslog""" )

    event_log_syslog_server = Option('event_log', "syslog_server", "/dev/log",
                            """ Server address to use for high level events logging""" )

    event_log_tcp_server = Option('event_log', "tcp_server", "localhost",
                                  """ Nane or IP of server running a specific calc_env event logger """)

    event_log_tcp_port = Option('event_log', "tcp_port", logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                                  """ Port number to use when connecting to a specific calc_env event logger """)

    def _setup_event_log(self):
        """
        Setup the event log - which is the log where notification about events are sent to

        En event is an operation such as calculation clone, or calculation submit which
        internaly contaisn many other operations.
        The idea is to have a separate log for high level calc_env events
        """
        self.log.debug("Preparing event log")

        # Creating the logger
        self.event_log = logging.getLogger("events")
        self.event_log.setLevel(logging.INFO)
        self.event_log.propagate = False

        # Format of message
        format = 'event: %(message)s'
        #format = '%(asctime)s ' + format
        datefmt = ''
        formatter = logging.Formatter(format, datefmt)

        # The syslog handler
        if self.event_log_syslog_facility != "":
            self.log.debug("Setting syslog handler server:{0} facility:{1}".format(self.event_log_syslog_server, self.event_log_syslog_facility))
            syslog_handler = logging.handlers.SysLogHandler(self.event_log_syslog_server,
                                                         facility=self.event_log_syslog_facility)
            syslog_handler.setLevel(logging.INFO)
            syslog_handler.setFormatter(formatter)
            self.event_log.addHandler(syslog_handler)

        # The tcp handler
        if self.event_log_tcp_server:
            self.log.debug("Setting tcp log handler server:{0} port:{1}".format(self.event_log_tcp_server,
                                                                                self.event_log_tcp_port))
            socket_handler = logging.handlers.SocketHandler(self.event_log_tcp_server,
                                                            self.event_log_tcp_port)
            socket_handler.setLevel(logging.INFO)
            socket_handler.setFormatter(formatter)
            self.event_log.addHandler(socket_handler)

    def __init__(self, *args, **kwargs):
        pass

    def _test_components(self):
        try:
            str(self.db)
        except AttributeError, e:
            self.log.error("Error: environment.db component was not loaded correctly: {0}".format(str(e)))
            raise Exception("Error: environment.db component was not loaded correctly")

        try:
            str(self.rm)
        except AttributeError, e:
            self.log.error("Error: environment.rm component was not loaded correctly: {0}".format(str(e)))
            raise Exception("Error: environment.rm component was not loaded correctly")
        try:
            str(self.fs)
        except AttributeError, e:
            self.log.error("Error: environment.fs component was not loaded correctly: {0}".format(str(e)))
            raise Exception("Error: environment.fs component was not loaded correctly")

    def init(self, path):
        """
        read config file and init members
        """
        super(Environment, self).init(path)
        # Checking that components are in place
        self._test_components()

        self.uid = os.getuid()
        self._setup_event_log()

        self.log.debug("About to connect to db")
        #self.log.debug("\n\n\nPYTHON PATH: {0}".format(str(sys.path)))
        self.db.connect()
        self.log.debug("After db connect (db plugin is ok)")

        #self.rm.verify()  # TODO add some kind of verify to RM so to check that rm is configured correctly.

    def log_event(self, event):
        """
        Log high level events
        """

        # Filling the missing fields
        if event.uid is None:
            event.uid = self.uid
        if event.pid is None:
            event.pid = os.getpid()
        if event.host is None:
            event.host = socket.gethostname()

    def init_calcdir(self, calc_path):
        scompy_dir = os.path.join(calc_path, CALC_INTERNAL_DIR_NAME)
        if not os.path.isdir(scompy_dir):
            os.makedirs(scompy_dir)

        initfile = os.path.join(scompy_dir, '__init__.py')
        if not os.path.isfile(initfile):
            open(initfile, 'w').write('# Auto generated by Environment.init_calcdir\n')

    def load_calcspec(self, calc_path):
        """
        Load calculation specification from the given path and return a TBD object.

        NOT YET:
            The returned object can be "casted" to interface implementations via
            ICalc(load_calcspec(clac_path)).

        """
        calc_path = os.path.abspath(calc_path)
        if not os.path.isdir(calc_path):
            raise ImportError("Tried to load calcspec from non-existing directory {0}".format(calc_path))

        self.log.debug("load_calcspec: Loading from {0!r}".format(calc_path))

        #add_to_sys_path = os.path.dirname(calc_path)
        add_to_sys_path = calc_path
        self.log.debug("load_calcspec: Add to syspath {0!r}".format(add_to_sys_path))
        sys.path.append(add_to_sys_path)
        try:
            name = CALC_INTERNAL_DIR_NAME + '.' + CALCSPEC_MODULE_NAME
            self.log.debug("load_calcspec: Loading module {0!r}".format(name))
            try:
                __import__(name)
            except ImportError, e:
                raise ImportError("Error importing {0} from calculation directory {1}: {2}".format(
                    name, calc_path, e))

            self.log.debug("load_calcspec: Returning Calc from {0!r}".format(sys.modules[name]))
            reload(sys.modules[name])
            return sys.modules[name].Calc()
        finally:
            sys.path.remove(add_to_sys_path)
            if add_to_sys_path in sys.path_importer_cache:
                del sys.path_importer_cache[add_to_sys_path]

    def _unload_pkg(self, pkg_name):
        self.log.debug("_unload_pkg: Unloading package {0!r}.".format(pkg_name))
        deleted = {}
        for mod in sys.modules.keys():
            if mod.startswith(pkg_name + '.') or mod == pkg_name:
                self.log.debug("_unload_pkg: Removing {0!r}={1!r} from sys.modules.".format(
                    mod, sys.modules[mod]))
                if sys.modules[mod] is not None:
                    deleted[mod] = sys.modules[mod]
                del sys.modules[mod]
        self.log.debug("_unload_pkg: Refcounts: {0!r}".format(
            list((k, sys.getrefcount(deleted.pop(k)) - 1) for k in sorted(deleted.keys())),
        ))

    def unload_calcspec(self, calcspec):
        """
        Try to unimport modules loaded when calcspec was loaded.

        calcspec is a value returned by load_calcspec.

        TODO: Change this when we add caching.

        """
        pkg_name = type(calcspec).__module__.split('.')[0]
        self.log.debug("unload_calcspec: Unloading calcspec that came from {0!r}, in package {1!r}.".format(
            type(calcspec).__module__, pkg_name))
        self.log.debug("unload_calcspec: Refcount of calcspec: {0!r}".format(
            sys.getrefcount(calcspec) - 1))
        self._unload_pkg(pkg_name)

    def get_do_stages_cmd(self, calc_id, stage_list):
        """
        Return the cmd for running calculation given by calc_id

        The result is a sequence of strings a'la subprocess.Popen

        """
        scompy_src_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        return ([
            #'env', 'PYTHONPATH={0}'.format(scompy_src_dir),
                 'python',
                os.path.realpath(os.path.join(os.path.dirname(__file__), CALC_ENV_RUNNER))] +
                [repr(x) for x in (self.path, calc_id, ) + tuple(stage_list)]
                )



def load_calcspec(calc_path):
    """
    Load calculation specification from the given path and return a TBD object.

    NOT YET:
        The returned object can be "casted" to interface implementations via
        ICalc(load_calcspec(clac_path)).

    """
    calc_path = os.path.abspath(calc_path)
    if not os.path.isdir(calc_path):
        raise ImportError("Tried to load calcspec from non-existing directory {0}".format(calc_path))

    #self.log.debug("load_calcspec: Loading from {0!r}".format(calc_path))

    #add_to_sys_path = os.path.dirname(calc_path)
    add_to_sys_path = calc_path
    #self.log.debug("load_calcspec: Add to syspath {0!r}".format(add_to_sys_path))
    sys.path.append(add_to_sys_path)
    try:
        name = CALC_INTERNAL_DIR_NAME + '.' + CALCSPEC_MODULE_NAME
        #self.log.debug("load_calcspec: Loading module {0!r}".format(name))
        try:
            __import__(name)
        except ImportError, e:
            raise ImportError("Error importing {0} from calculation directory {1}: {2}".format(
                name, calc_path, e))

        #self.log.debug("load_calcspec: Returning Calc from {0!r}".format(sys.modules[name]))
        reload(sys.modules[name])
        return sys.modules[name].Calc()
    finally:
        sys.path.remove(add_to_sys_path)
        if add_to_sys_path in sys.path_importer_cache:
            del sys.path_importer_cache[add_to_sys_path]
