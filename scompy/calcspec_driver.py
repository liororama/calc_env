"""
This file is used to interact with calspec in a subprocess.

It provides several "functions", all called via the single function
call_in_subprocess.

This file has a "main" part, which is used by call_in_subprocess to call all
other functions defined in this module.

Arguments and return values are passed using repr/eval, so they can only be
simple nested lists/dictionaries of strings, integers, and floats (i.e. no objects or functions).

"""

import sys
import subprocess
import os
import tempfile

scompy_src_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(scompy_src_dir)

from scompy.environment import Environment, CALC_INTERNAL_DIR_NAME
from scompy.core import Stage
from scompy.core import CalcSpecError


calcspec_info_cache = {}
call_method = "fork"


def call_calcspec(env_path, calc_path, outerr_name, func_name, args):
    # TODO - for some strange reason some func_name methods does not work in
    #        fork mode. The better case is to use fork instead of subprocess for
    #        all methods. For now we use fork only for fast methods (e.g. get_info)
    #        where the delay caused by fork->exec (subprocess) is too big comparing
    #        to the runtime of the method itself.
    # Below is the piece of code allowing to move all methods to fork or to subprocess
    #if call_method == "subprocess":
    #    return call_in_subprocess(env_path, calc_path, outerr_name, func_name, args)
    #elif call_method == "fork":
    #    return call_in_fork(env_path, calc_path, outerr_name, func_name, args)
    #else:
    #    raise CalcSpecError("Error: call_methods {0} is not supported".format(call_methods))

    # Only get_info is using the fork method
    if call_method == "fork":
        if func_name == "get_info":
            return call_in_fork(env_path, calc_path, outerr_name, func_name, args)
        else:
            return call_in_subprocess(env_path, calc_path, outerr_name, func_name, args)
    elif call_method == "subprocess":
        return call_in_subprocess(env_path, calc_path, outerr_name, func_name, args)
    else:
        raise Exception("No recognized call_method: {0}".format(call_method))


def call_in_subprocess(env_path, calc_path, outerr_name, func_name, args):
    """
    Spawn a subprocess used to call the requested function.

    """
    scompy_src_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    calc_path = os.path.realpath(calc_path)
    internal_path = os.path.join(calc_path, CALC_INTERNAL_DIR_NAME)
    fd, result_fn = tempfile.mkstemp(dir=internal_path)
    os.close(fd)
    stdout_fn = os.path.join(internal_path, outerr_name + ".out")
    stderr_fn = os.path.join(internal_path, outerr_name + ".err")
    cmd = (["python", os.path.realpath(__file__)] +
           [repr(x) for x in (env_path, result_fn, func_name, args)])
    status = subprocess.call(
        cmd,
        stdout=open(stdout_fn, 'w'),
        stderr=open(stderr_fn, 'w'),
        stdin=open(os.devnull),
        cwd=calc_path
    )
    if status == 0:
        f = open(result_fn)
        result = eval(f.read(), {}, {})
        f.close()
        os.remove(result_fn)
        return result
    elif status == 3:
        raise CalcSpecError(
            "Error go sequence contain undefined stage: calcspec path {0!r}, error file =  {1!r} ".format(
                calc_path, stderr_fn))
    else:
        raise CalcSpecError(
            "Error occured while using calcspec in {0!r}, look at: {1!r}, {2!r}".format(
                calc_path, stdout_fn, stderr_fn))


def child_worker(env_path, result_fn, func_name, args):
    """
    Handle loading calcspec and running the desired calcspec method in a way
    suitable for a child process
    """
    # Environment is already initialized, we dont need the env_path
    env = Environment()
    # Disconnecting from the db
    env.db.disconnect()
    calcspec = env.load_calcspec(".")

    if check_go_sequence(env, calcspec) is False:
        os._exit(3)

    try:
        if func_name == "reset_stage" and False:
            #result = getattr(calcspec, args[0])().reset(args[1])
            result = reset_stage(env, calcspec, *args)
        else:
            result = globals()[func_name](env, calcspec, *args)
    except Exception, e:
        print "Error in running command:{0}".format(func_name), e
        os._exit(1)

    f = open(result_fn, 'w')
    f.write(repr(result))
    f.close()


def call_in_fork(env_path, calc_path, outerr_name, func_name, args):
    """
    Use fork to spawn a process which will run calcspec
    """
    scompy_src_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    calc_path = os.path.realpath(calc_path)
    internal_path = os.path.join(calc_path, CALC_INTERNAL_DIR_NAME)
    fd, result_fn = tempfile.mkstemp(dir=internal_path)
    os.close(fd)
    stdout_fn = os.path.join(internal_path, outerr_name + ".out")
    stderr_fn = os.path.join(internal_path, outerr_name + ".err")

    env = Environment()
    env.db.disconnect()

    pid = os.fork()
    if pid != 0:
        env.db.connect()
        # In parent
        (pid, status) = os.waitpid(pid, 0)
        #print "Parent: childe done pid= {0} status={1}".format(pid, status)
    else:
        #print "In Child: out=", stdout_fn

        try:

            so = open(stdout_fn, 'a+')
            se = open(stderr_fn, 'a+')
            si = open(os.devnull)


            #sys.stdout.close()
            #sys.stderr.close()
            #sys.stdin.close()

            #print "\n\n\nSTDIN fileno: {0}\n\n\n".format(sys.stdin.fileno())
            os.dup2(si.fileno(), 0)
            os.dup2(so.fileno(), 1)
            os.dup2(se.fileno(), 2)

            os.chdir(calc_path)
            child_worker(env_path, result_fn, func_name, args)    #code

            so.close()
            se.close()
            si.close()
            os._exit(0)

        except:
            import traceback
            sys.stderr.write(traceback.format_exc())
            os._exit(3)

    # Only parent should be here
    if status == 0:
        f = open(result_fn)
        result = eval(f.read(), {}, {})
        f.close()
        os.remove(result_fn)
        return result
    elif status == 3:
        raise CalcSpecError(
            "Error go sequence contain undefined stage: calcspec path {0!r}, error file =  {1!r} ".format(
                calc_path, stderr_fn))
    else:
        was_signal = os.WIFSIGNALED(status)
        exit_status = os.WEXITSTATUS(status)
        print "was_signal: {0}  exit_status: {1}".format(was_signal, exit_status)
        raise CalcSpecError(
            "Error occured while using calcspec in {0!r}, look at: {1!r}, {2!r} status={3!r}".format(
                calc_path, stdout_fn, stderr_fn, status))


def get_extra_rm_info(env, calcspec, extra_rm_info):
    return calcspec.get_extra_rm_info(extra_rm_info)


def clone(env, calcspec, target_dir):
    return calcspec.clone(target_dir)


def reset(env, calcspec):
    return calcspec.reset()


def get_go_sequence(env, calcspec):
    return calcspec.get_go_sequence()


def get_status_line(env, calcspec):
    return calcspec.get_status_line()


def reset_stage(env, calcpsec, stage_name, stage_current_status):
    """
    Reset the requested stage, return status (True/False).
    """
    return getattr(calcspec, stage_name)().reset(stage_current_status)


def do_stage(env, calcspec, stage_name, in_resource_allocation):
    """
    Run the requested stage, return status (True/False).
    """
    if in_resource_allocation:
        print "Getting allocation"
        resource_allocation = env.rm.get_current_allocation()
        print "After getting allocation"
    else:
        resource_allocation = None
    return getattr(calcspec, stage_name)().go(resource_allocation)


def do_stages(env, calcspec, stage_list, in_resource_allocation):
    """
    Run the requested stages, return status (True/False)
    """
    for stage in stage_list:
        result = do_stage(env, calcspec, stage, in_resource_allocation)
        if result is False:
            return False
    return True


def get_stage_info(stage):
    return dict(
        np=stage.get_np(),
    )


def get_info(env, calcspec):
    """
    Return a dictionary mapping with calculation information.

    The dictionary so far:
    info['stages'] = {'build': {'np': 1}, 'run': {'np': 100}}

    """
    info = dict()
    info["stages"] = dict()
    for name in dir(calcspec):
        print "Checking name: {0}".format(name)
        value = getattr(calcspec, name)
        if isinstance(value, type):
            print "Is instance"
            print "Class value: ", value
            if isinstance(value, Stage):
                print "Is subclass"

            if issubclass(value, Stage):
                print "Is subclass"

        if isinstance(value, type) and issubclass(value, Stage):
            print "Adding {0} to stage list".format(name)
            info["stages"][name] = get_stage_info(value())

    info["go_sequence"] = calcspec.get_go_sequence()
    return info


import pprint
def check_go_sequence(env, calcspec):

    print "Checking go sqeuence:", calcspec
    calc_info = get_info(env, calcspec)
    print "Calc Info:", pprint.pprint(calc_info)
    for go_stage in (calc_info["go_sequence"][0] + calc_info["go_sequence"][1]):
        if go_stage not in calc_info["stages"]:
            print "Error when checking go_sequence stage: {0} is not in stage list".format(go_stage)
            print "Stages:"
            for s in calc_info["stages"]:
                print "S: {0}".format(s)
            return False


# The main part. this is used by the call_in_subprocess method which exec
# this file so __main__ is done
if __name__ == '__main__':
    env_path, result_fn, func_name, args = (eval(x, {}, {}) for x in sys.argv[1:])

#    print "Env path:", env_path
#    print "\n\n\nPython path:", sys.path, "\n\n\n"

    env = Environment(env_path)
    calcspec = env.load_calcspec(".")
#    print "Result-FN:", result_fn
#    print "Running:", func_name
    if check_go_sequence(env, calcspec) is False:
        sys.exit(3)
    try:
        result = globals()[func_name](env, calcspec, *args)
    except:
        raise  # so that the error will appear in the error file
#    print "Type:", type(result)
#    print "Result:", result
#    print "Repr-Result:", repr(result)
    open(result_fn, 'w').write(repr(result))
