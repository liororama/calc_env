"""
Permissions related classes
"""

from externals.trac_core import Interface, Component, implements
from externals.trac_config import ExtensionOption, OrderedExtensionsOption


class PermissionError(Exception):
    """Insufficient permissions to complete the operation"""
    def __init__(self, action, calc_id):
        self.action = action
        self.calc_id = calc_id

    def __str__(self):
        return "Current user cannot perform {0} on calc_id={1}".format(self.action, self.calc_id)

    def __repr__(self):
        return "PermissionError({0!r}, {1!r})".format(self.action, self.calc_id)


class IPermissionPolicy(Interface):
    """
    A security policy provider used for fine grained permission checks.
    """

    def check_permission(self, action, calc_record):
        """
        Check if the action can be performed by the current user on the calculation.

        :param action: the name of the permission - currently 'READ' or 'WRITE'
        :param calc_record: the calc_record of calculation on which the check applies.

        :return: `True` if action is allowed, `False` if action is denied,
                 or `None` if indifferent. If `None` is returned, the next
                 policy in the chain will be used, and so on.

        """


class FilesystemPermissionPolicy(Component):
    """
    Policy that allows / denies permission based on filesystem permissions to
    the calculation path.
    """

    implements(IPermissionPolicy)

    def check_permission(self, action, calc_record):
        """
        Check if the action can be performed by the current user on the calculation.

        :param action: the name of the permission - currently 'READ' or 'WRITE'
        :param calc_record: the calc_record of calculation on which the check applies.

        :return: `True` if action is allowed, `False` if action is denied,
                 or `None` if indifferent. If `None` is returned, the next
                 policy in the chain will be used, and so on.

        """
        from os import access, R_OK, W_OK, X_OK

        if action == 'READ':
            mode = X_OK
        elif action == 'WRITE':
            mode = W_OK
        else:
            assert False
        return access(calc_record.path, mode)


class PermissionSystem(Component):
    """Permission management sub-system."""

    required = True

    policies = OrderedExtensionsOption(
        'calc_env', 'permission_policies',
        IPermissionPolicy,
        'FilesystemPermissionPolicy',
        False,
        """List of components implementing `IPermissionPolicy`, in the order in
        which they will be applied. Defaults to `FilesystemPermissionPolicy`""",
    )

    def check_permission(self, action, calc_record):
        """
        Return True if permission to perform action for the given calculation is allowed for the current user.
        """
        for policy in self.policies:
            decision = policy.check_permission(action, calc_record)
            if decision is not None:
                if not decision:
                    self.log.debug("{0} denies performing {1} on calc_id={2}".format(
                        policy.__class__.__name__,
                        action,
                        calc_record.id))
                return decision
        self.log.debug("No policy allowed performing {0} on calc_id={1}".format(
            action, calc_record.id))
        return False

    def require_permission(self, action, calc_record):
        """
        Raise a PermissionError if we don't have permission to perform action on the given calculation.
        """
        if not self.check_permission(action, calc_record):
            raise PermissionError(action, calc_record.id)
