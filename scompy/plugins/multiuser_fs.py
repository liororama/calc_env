import os
from textwrap import dedent
from os.path import expanduser, join, abspath

import scompy.structures
from scompy.externals.trac_core import Component, implements
from scompy.externals.trac_config import Option

from scompy.core import *


def get_path(calc_id):
    return 'calc_{0}'.format(calc_id)


class MultiUserFS(Component):
    """
    Provide a simple implementation of ICalcFS for a multiple users filesystem
    layout.

    """

    implements(ICalcFS)

    calculations_path = Option(
        'MultiUserFS', 'calculations_path', None, dedent(
            """The path for the calculations directory tree relative to the
            user's home directory"""))

    def new_calc_dir(self, calc_id):
        """
        Create a new calculation directory and return the path to it.
        """
        path = abspath(expanduser(join("~", self.calculations_path, get_path(calc_id))))
        os.makedirs(path)
        # TODO set permissions (chmod) path according to per user config file
        # TODO if we are cloning, we should also clone calc path permissions
        return path
