
import os
import re
import subprocess
import time

from scompy.structures import RMJobStatus

# TODO change the * to the necessary Classes
from scompy.externals.trac_core import *

from scompy.core import ResourceManagerError, NotInResourceAllocationError, IResourceAllocation, IResourceManager

from scompy.environment import Environment

#job_status_dict = dict((s, s) for s in dir(JobStatus) if not s.startswith('_'))

# TODO - ADD all possible slurm job status to here and in case we get a value not in
#        this dict we will raise exception.
job_status_dict = {
    'RUNNING': RMJobStatus.RUNNING,
    'PENDING': RMJobStatus.PENDING,
    'SUSPENDED': RMJobStatus.SUSPENDED
}

# Backport of subprocess.check_output for python 2.6
if not hasattr(subprocess, 'check_output'):
    def check_output(*popenargs, **kwargs):
        r"""Run command with arguments and return its output as a byte string.

        Backported from Python 2.7 as it's implemented as pure python on stdlib.

        >>> check_output(['/usr/bin/python', '--version'])
        Python 2.6.2
        """
        process = subprocess.Popen(stdout=subprocess.PIPE, *popenargs, **kwargs)
        output, unused_err = process.communicate()
        retcode = process.poll()
        if retcode:
            cmd = kwargs.get("args")
            if cmd is None:
                cmd = popenargs[0]
            error = subprocess.CalledProcessError(retcode, cmd)
            error.output = output
            raise error
        return output
    setattr(subprocess, 'check_output', check_output)


def parse_slurm_tasks_per_node_str(tpn_str):
    """
    Parse a tasks per node string and return a list of numbers of tasks per node

    The string is of the format found in the SLURM_TASKS_PER_NODE environment
    variable. This is a comma seperated list of either integers or expression of
    the form N(xM) where N is the number of tasks and M is the number of nodes
    with the same number of tasks. Example 32(x4) means there are 4 nodes with
    32 tasks on each such node.

    TODO: Should this return a generator and not a full list? Think 10^5 nodes.

    """

    tpn_list = []
    parts = tpn_str.split(",")
    for p in parts:
        # The part is in the for of N(xM) where N is the number of tasks and M is the number of
        # nodes with the same tasks number
        m = re.match("(\d+)\(x(\d+)\)", p)
        if m:
            task_num = int(m.group(1))
            node_multiplier = int(m.group(2))
        else:
            # A single int representing the number of tasks for a single node
            task_num = int(p)
            node_multiplier = 1
        tpn_list.extend([task_num] * node_multiplier)
    return tpn_list


class SlurmRMError(ResourceManagerError):
    pass


class SlurmResourceAllocation(object):
    """
    Resource allocation object provided by the SlurmRM
    """

    def __init__(self, rm, slurm_node_allocation_str, slurm_tasks_per_node_str):
        # self.slurm_rm = rm # TODO: can we remove this?
        self.log = rm.log
        self.node_allocation_str = slurm_node_allocation_str
        self.tasks_per_node_str = slurm_tasks_per_node_str

    def get_hosts(self):
        """
        Return a generator for allocated host names.

        Items in the return value represent process allocations. So if the
        resource allocation has 4 processes, two on node1 and two on node2, the
        result should be: ('node1', 'node1', 'node2', 'node2')

        """

        self.log.debug("Getting hosts for allocation: nodes={0!r} tasks_per_node={1!r}".format(
            self.node_allocation_str, self.tasks_per_node_str))

        node_list_str = subprocess.check_output(["scontrol", "show", "hostnames", self.node_allocation_str])
        node_list = node_list_str.splitlines()

        self.log.debug("Got node list: {0}".format(node_list))

        tasks_per_node_list = list(parse_slurm_tasks_per_node_str(self.tasks_per_node_str))
        self.log.debug("Got tasks per node list: {0}".format(tasks_per_node_list))

        for node, tasks_per_node in zip(node_list, tasks_per_node_list):
            for t in range(tasks_per_node):
                yield node

    def shrink(self, np=1):
        """
        Shrink the reseource allocation to the given number of processes.
        """
        assert False # TODO


class SlurmJob(object):
    """
    Implementation of the IRMJob interface for Slurm
    """

    def __init__(self, job_id, log=None):
        self.job_id = job_id

        if not log:
            self.log = Environment().log
        else:
            self.log = log

    def __str__(self):
        return "{name}({job_id!r})".format(name=type(self).__name__, job_id=self.job_id)

    def __repr__(self):
        return "{name}({job_id!r}, {log!r})".format(name=type(self).__name__, **self.__dict__)

    def to_string(self):
        """
        Convert to string representation and return it.

        Result can be transferred to IResourceManager.job_from_string.

        """

        return str(self)

    def get_status(self):
        """
        Query the resource manager for status, and return RMJobStatus value.
        """

        try:
            job_info_str = subprocess.check_output(["scontrol", "-o", "show", "job", str(self.job_id)])
            self.log.debug("Output of scontrol show job: [{0}]".format(job_info_str))
            m = re.match(".*\s+JobState=(\w+)\s+", job_info_str)
            if m:
                job_status_str = m.group(1)
                self.log.debug("Found matching JobState {0}".format(job_status_str))
                return job_status_dict.get(job_status_str, RMJobStatus.NOT_FOUND)

        except subprocess.CalledProcessError, e:
            return RMJobStatus.NOT_FOUND

    def kill(self):
        """
        Kill a live job.
        """

        self.log.debug("Canceling job: {0}".format(self.job_id))
        process = subprocess.Popen(["scancel", str(self.job_id)],
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   )
        scancel_output, scancel_err = process.communicate()
        scancel_returncode = process.returncode

        if scancel_returncode != 0 or scancel_err != "":
            self.log.error("Error trying to cancel job {0}".format(self.job_id))
            raise SlurmRMError("Error in cancelling job {0} [{1}]".format(self.job_id, scancel_err))

    def suspend(self):
        """
        Suspend a running job.
        """

    def resume(self):
        """
        Resume a suspended job.
        """


class SlurmRM(Component):
    """
    Provide a SLURM resource manager implementation.
    """

    implements(IResourceManager)

    def get_current_allocation(self):
        """
        Return a resource allocation object for the current allocation.

        Assume we are in a resource allocation, and obtain the
        allocation information from environmnet variables. Raise
        NotInResourceAllocation if called while not in a resource allocation.

        The returned object implements IResourceAllocation.

        """

        self.log.debug("Getting resource allocation: {0}".format(os.environ))
        if "SLURM_JOB_NODELIST" not in os.environ:
            raise NotInResourceAllocationError("Error, not in resource allocation")
        node_list_env = os.environ["SLURM_JOB_NODELIST"]
        tasks_per_node_env = os.environ["SLURM_TASKS_PER_NODE"]
        return SlurmResourceAllocation(self, node_list_env, tasks_per_node_env)

    def submit(self,
               cmd,
               np,
               work_dir,
               output_filename,
               partition=None,
               account=None,
               constraint=None,
               job_name=None,
               ):
        """
        Submit a job to be run and return a SlurmJob object.

        cmd - A sequence of program arguments (the first is the program to run).
        np - Number of processes required.
        work_dir - The working dir in which the job should be started. cmd is
                   also looked in this directory.
        output_filename - The running job's stdout and stderr will be directed
                          to this file (relative to work_dir).
        job_name - job name (--job-name)
        partition - slurm partition (--partition)
        account - slurm account to use (--account)
        constraint - slurm constraint (--constraint)
        """

        # convert cmd to list
        if isinstance(cmd, basestring):
            cmd = [cmd]
        else:
            cmd = list(cmd)

        # Pepare the arguments
        self.log.debug("submit: __file__ = {0!r}".format(__file__))
        plugin_dir = os.path.dirname(os.path.realpath(__file__))
        # TODO: Check if this works for all slurm version, or we need a .sh
        # script.
        slurm_runner_path = os.path.join(plugin_dir, "slurm_runner.sh")

        sbatch_cmd_list = ["sbatch",
                           "--ntasks={0}".format(np),
                           "--workdir={0}".format(work_dir),
                           "--output={0}".format(output_filename),
                           "--open-mode=append",
                          ]
        if job_name:
            sbatch_cmd_list += ["--job-name={0}".format(job_name)]
        if partition:
            sbatch_cmd_list += ["--partition={0}".format(partition)]
        if account:
            sbatch_cmd_list += ["--account={0}".format(account)]
        if constraint:
            sbatch_cmd_list += ["--constraint={0}".format(constraint)]

        sbatch_cmd_list += [slurm_runner_path] + cmd

        # Submit the command
        self.log.debug("submit: calling sbatch {0}".format(sbatch_cmd_list))
        process = subprocess.Popen(sbatch_cmd_list,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   cwd=work_dir,
                                   )
        sbatch_output, sbatch_err = process.communicate()
        sbatch_returncode = process.returncode
        self.log.debug("submit: SBATCH retcode: {0}".format(sbatch_returncode))
        self.log.debug("submit: SBATCH output: [{0}]".format(sbatch_output))
        self.log.debug("submit: SBATCH err: [{0}]".format(sbatch_err))

        if sbatch_returncode != 0:
            raise ResourceManagerError("Error calling sbatch", sbatch_err)

        # Parse out the job id and return the SlurmJob object
        # Output example: Submitted batch job 3
        m = re.match("Submitted batch job (\d+)", sbatch_output)
        if m:
            job_id = int(m.group(1))
            self.log.debug("submit: Detected job_id={0}".format(job_id))
            return SlurmJob(job_id, self.log)
        else:
            self.log.error("submit: Failed to extract job id from sbatch output [{0}]".format(sbatch_output))
            raise CalculationSubmitError("Invalid sbatch_output", sbatch_output, sbatch_err)

    def job_from_string(self, s):
        """
        Return a resource manager job object created from a string representation.
        """
        return eval(s, dict(SlurmJob=SlurmJob), {})

    def cancel_all_jobs(self):

        subprocess.check_call(["scancel", "-u", os.environ["USER"]])
        time.sleep(1)
