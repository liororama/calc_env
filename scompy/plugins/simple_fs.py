import os

import scompy.structures
# TODO change the * to the necessary Classes
from scompy.externals.trac_core import *
from scompy.externals.trac_config import *

from scompy.core import *


def get_path(calc_id):
    return 'calc_{0}'.format(calc_id)


class SimpleFS(Component):
    """
    Provide a simple implementation of ICalcFS
    """

    implements(ICalcFS)

    root = PathOption('SimpleFS', 'calc_root', None,
        """The root directory for the calculations directory tree""")

    def new_calc_dir(self, calc_id):
        """
        Create a new calculation directory and return the path to it.
        """
        # TODO users
        path = os.path.join(self.root, get_path(calc_id))
        os.makedirs(path)
        return path
