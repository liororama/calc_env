#!/bin/bash

#
# A wrapper script for running the calc_env_runner.py via slurm.
# Slurm accepts only shell scripts to run via sbatch
#
$@

#CALC_ENV_DIR=$1
#export PYTHONPATH=$CALC_ENV_DIR

# Running the calc_env_runner
#python $CALC_ENV_DIR/calc_env_runner.py $*
