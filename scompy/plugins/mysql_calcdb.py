import socket
import os
import sys
import datetime
import posixpath

from scompy.structures import StageStatus, LockInfo, CalcRecord, StageRecord
# TODO change the * to the necessary Classes
from scompy.externals.trac_core import *
from scompy.externals.trac_config import *

from scompy.core import ICalcDB, ICalcDBAdmin
from scompy.core import now, NODE_SEPARATOR, HierarchyError, CalculationRemoveError

import MySQLdb as mysql

from sqlalchemy import Column, Integer, Float, String, Boolean, DateTime, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import CheckConstraint, UniqueConstraint, PrimaryKeyConstraint
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.exc import IntegrityError

Base = declarative_base()


# dict used to convert status so that it can be checked with is and not ==
status_dict = dict((s, s) for s in dir(StageStatus) if not s.startswith('_'))
status_dict[None] = None # TODO is this needed?


class SQLLockInfo(Base):
    """
    Contains information about the calc_record lock
    """
    __tablename__ = 'lock_info_tbl'

    calc_id = Column(Integer, primary_key=True)
    pid = Column(Integer)
    host = Column(String(64))
    start = Column(DateTime)

    #@classmethod
    #def from_lock_info(cls, lock_info):
    #    """
    #    Generate an SQLLockInfo from LockInfo
    #    """
    #    sql_li = cls()
    #    sql_li.update(lock_info)
    #    return sq_cr

    def to_lock_info(self):
        """
        Generate a compatible LockInfo
        """
        return LockInfo(
            calc_id=self.calc_id,
            pid=self.pid,
            host=self.host,
            timestamp=self.start)

    #def update(self, lock_info):
    #    """
    #    Update the SQLLockInfo from a LockInfo
    #    """
    #    self.pid = lock_info.pid
    #    self.host = lock_info.host
    #    self.start = lock_info.timestamp


_time_field = 'time_{0}'.format
_microsecond_field = 'time_{0}_microsecond'.format


def get_time(obj, name):
    """
    Return a datetime.dateime value or None
    """
    t = getattr(obj, _time_field(name))
    if t is None:
        return None
    microsecond = getattr(obj, _microsecond_field(name))
    return datetime.datetime(
        year=t.year,
        month=t.month,
        day=t.day,
        hour=t.hour,
        minute=t.minute,
        second=t.second,
        microsecond=microsecond,
        )


def set_time(obj, name, t):
    """
    Set time to a datetime.datetime value or None
    """
    if t is None:
        datetime_without_microsecond = None
        microsecond = None
    else:
        datetime_without_microsecond = datetime.datetime(
            year=t.year,
            month=t.month,
            day=t.day,
            hour=t.hour,
            minute=t.minute,
            second=t.second
            )
        microsecond = t.microsecond
    setattr(obj, _time_field(name), datetime_without_microsecond)
    setattr(obj, _microsecond_field(name), microsecond)


class SQLCalcRecord(Base):
    """
    SQL representation of CalcRecord
    """

    __tablename__ = 'calc_tbl'
    __table_args__ = (
        CheckConstraint('calc_id != parent_id', name='check1'),
        {'mysql_engine': 'InnoDB'}
        )

    calc_id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(256))
    description = Column(String(1024))
    parent_id = Column(Integer, ForeignKey('calc_tbl.calc_id'))
    uid = Column(Integer)
    path = Column(String(1024))
    time_create = Column(DateTime)
    time_create_microsecond = Column(Integer)
    calc_lock = Column(Boolean)

    @classmethod
    def from_calc_record(cls, calc_rec):
        """
        Generate an SQLCalcRecord from CalcRecord
        """
        sq_cr = SQLCalcRecord()
        sq_cr.update(calc_rec)
        return sq_cr

    def to_calc_record(self):
        """
        Generate a compatible CalcRecord object
        """
        return CalcRecord(
            id=self.calc_id,
            title=self.title,
            description=self.description,
            parent_id=self.parent_id,
            uid=int(self.uid),
            path=self.path,
            time_create=get_time(self, 'create'),
            )

    def update(self, calc_rec):
        """
        Update the SQLCalcRecord from a CalcRecord
        """
        self.title = calc_rec.title
        self.description = calc_rec.description
        self.parent_id = calc_rec.parent_id
        self.uid = calc_rec.uid
        self.path = calc_rec.path
        set_time(self, 'create', calc_rec.time_create)


class SQLStageRecord(Base):
    """
    SQL representation of StageRecord
    """

    __tablename__ = 'stage_tbl'
    __table_args__ = (
        #UniqueConstraint('calc_id', 'name'),
        PrimaryKeyConstraint('calc_id', 'name'),
        {'mysql_engine': 'InnoDB'}, )

    calc_id = Column(Integer, ForeignKey('calc_tbl.calc_id'))
    name = Column(String(32))
    status = Column(String(32))
    rm_job = Column(String(1024))
    time_submit = Column(DateTime)
    time_submit_microsecond = Column(Integer)
    time_begin = Column(DateTime)
    time_begin_microsecond = Column(Integer)
    time_end = Column(DateTime)
    time_end_microsecond = Column(Integer)

    @classmethod
    def from_stage_record(cls, stage_record):
        """
        Generate an SQLStageRecord from StageRecord object
        """
        sql_sr = cls()
        sql_sr.calc_id = stage_record.calc_id
        sql_sr.name = stage_record.name
        sql_sr.update(stage_record)
        return sql_sr

    def to_stage_record(self):
        """
        Generate a compatible StageRecord object
        """
        return StageRecord(
            calc_id=self.calc_id,
            name=self.name,
            status=status_dict[self.status],
            rm_job=self.rm_job,
            time_submit=get_time(self, 'submit'),
            time_begin=get_time(self, 'begin'),
            time_end=get_time(self, 'end'),
            )

    def update(self, stage_record):
        """
        Update the SQLStageRecord from a StageRecord
        """
        self.status = stage_record.status
        self.rm_job = stage_record.rm_job
        set_time(self, 'submit', stage_record.time_submit)
        set_time(self, 'begin', stage_record.time_begin)
        set_time(self, 'end', stage_record.time_end)


class SQLNodeRecord(Base):
    """
    SQL representation of hierarchy nodes.

    See
    http://docs.sqlalchemy.org/en/rel_0_8/orm/relationships.html#adjacency-list-relationships,
    and the adjacency_list example from the sqlalchemy sources.

    """

    __tablename__ = 'node_tbl'
    __table_args__ = (
        CheckConstraint('node_id != parent_id', name='check1'),
        {'mysql_engine': 'InnoDB'}
        )

    node_id = Column(Integer, primary_key=True, autoincrement=True)
    parent_id = Column(Integer, ForeignKey('node_tbl.node_id'))
    name = Column(String(256))
    # uid = Column(Integer)
    children = relationship("SQLNodeRecord",
                            backref=backref('parent', remote_side=node_id),
                            # children will be represented as a dictionary
                            # on the "name" attribute.
                            collection_class=attribute_mapped_collection('name'),
                            )

    calc_id = Column(Integer, ForeignKey('calc_tbl.calc_id'))
    calc = relationship("SQLCalcRecord", backref=backref("node", uselist=False))

    def __repr__(self):
        return "SQLNodeRecord(name={0!r}, node_id={1!r}, parent_id={2!r})".format(
            self.name,
            self.node_id,
            self.parent_id
            )

    def dump(self, _indent=0):
        return ("   " * _indent + repr(self) +
                "\n" + \
                "".join([
                    c.dump(_indent +1)
                    for c in self.children.values()]
                ))


#
#  * Generate the schema tables from scratch
#  * Get the parameters for the connection from the config file
#
class MySQLCalcDBAdmin(Component):
    """
    Provide implementation of ICalcDBAdmin for management of the db
    """

    implements(ICalcDBAdmin)

    db_server = Option('MySQLCalcDB', 'server', None,
                """Name of db server host""")

    db_user = Option('MySQLCalcDB', 'user', None,
                """Name of db user""")

    db_pass = Option('MySQLCalcDB', 'password', None,
                """Password to db""")

    db_schema = Option('MySQLCalcDB', 'schema', None,
                """The schema name in the db server""")

    def init_db(self, verbose=False):
        """
        Initialize the datebase and create tables.
        """
        try:
            db_uri = "mysql://{0}:{1}@{2}/{3}".format(self.db_user, self.db_pass,
                                                      self.db_server, self.db_schema)
            self.log.info("Initialazing DB [{0}]".format(db_uri))
            engine = create_engine(db_uri, echo=verbose)
            Base.metadata.drop_all(engine)
            Base.metadata.create_all(engine)

            # create the root hierarchy node
            Session = sessionmaker(bind=engine)
            session = Session()
            root = SQLNodeRecord(name='', parent=None)
            session.add(root)
            session.commit()
            session.close()

            engine.dispose()
            return True

        except Exception, e:
            print "Error %s:" % e.args[0]  # TODO, shouldn't use print
            return False

    def clear_tests_table(self, test_prefix="_cet_"):
        """
        Remove all tables that starts with test_prefix
        """

        conn_props = {}
        conn_props["user"] = self.db_user
        conn_props["passwd"] = self.db_pass

        cur = None
        try:
            cur = mysql.connect(**conn_props).cursor()
            if not self.exec_sql(cur, "SELECT SCHEMA_NAME FROM information_schema.SCHEMATA WHERE SCHEMA_NAME = '%s'" % (self.db_schema)):
                self.log.error("DB %s does not exist" % (self.db_schema))
                return -1

            table_count = self.exec_sql(cur, "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '%s'" % (self.db_schema))
            self.log.info("Found %s tables" % (table_count))
            if not table_count:
                self.log.info("Check to make sure you provided valid login credentials if you expected tables to exist in this database")
            else:
                tables_to_drop = [table for table in cur.fetchall() if table[0].startswith(test_prefix)]
                while len(tables_to_drop) > 0:
                    table = tables_to_drop.pop(0)
                    try:
                        self.exec_sql(cur, "DROP TABLE %s.%s " % (self.db_schema, table[0]))
                    except:
                        tables_to_drop.append(table)
        except mysql._mysql.OperationalError, e:
            self.log.error(e)
        finally:
            if cur:
                cur.close()

    def rename_tables(self, prefix, test_prefix="_cet_"):
        """
        Rename all tables in the db (except tables starting with test_prefix)
        to start with test_prefix+prefix.
        Use only for testing.
        """

        conn_props = {}
        conn_props["user"] = self.db_user
        conn_props["passwd"] = self.db_pass

        cur = None
        try:
            cur = mysql.connect(**conn_props).cursor()
            if not self.exec_sql(cur, "SELECT SCHEMA_NAME FROM information_schema.SCHEMATA WHERE SCHEMA_NAME = '%s'" % (self.db_schema)):
                self.log.error("DB %s does not exist" % (self.db_schema))
                return -1

            #if not exec_sql(cur, "SELECT SCHEMA_NAME FROM information_schema.SCHEMATA WHERE SCHEMA_NAME = '%s'" % (self.db_schema)):
            #    if options.create_database:
            #        exec_sql(cur, "CREATE DATABASE %s" % (new_db))
            #  else:
            #    logging.error("DB %s does not exist." % (old_db))
            #    return -2

            #trigger_count = exec_sql(cur, "SELECT TRIGGER_NAME FROM information_schema.TRIGGERS WHERE TRIGGER_SCHEMA = '%s'" % (old_db))
            #logging.info("Found %s triggers" % (trigger_count))
            #if trigger_count:
            #  for trigger in cur.fetchall():
            #    if options.drop_triggers:
            #      exec_sql(cur, "DROP TRIGGER %s.%s;" % (old_db, trigger[0]))
            #    else:
            #      logging.error("Unable to perform rename because of trigger %s.%s" % (old_db, trigger[0]))
            #      return -3

            table_count = self.exec_sql(cur, "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '%s'" % (self.db_schema))
            self.log.info("Found %s tables" % (table_count))
            if not table_count:
                self.log.info("Check to make sure you provided valid login credentials if you expected tables to exist in this database")
            else:
                for table in cur.fetchall():
                    if table[0].startswith(test_prefix):
                        continue
                    self.exec_sql(cur, "RENAME TABLE %s.%s TO %s.%s%s_%s" %
                                  (self.db_schema, table[0], self.db_schema, test_prefix, prefix, table[0]))
        except mysql._mysql.OperationalError, e:
            self.log.error(e)
        finally:
            if cur:
                cur.close()

    def exec_sql(self, cur, sql):
        self.log.debug(sql)
        return cur.execute(sql)

    def test_connection(self):
        """
        Testing connection to the db. For debugging purposes
        """
        conn_props = {}
        conn_props["user"] = self.db_user
        conn_props["passwd"] = self.db_pass
        conn_props["host"] = self.db_server

        self.log.info("========= Connecting db using: ============")
        self.log.info("user   = {0}".format(self.db_user))
        self.log.info("pass   = {0}".format(self.db_pass))
        self.log.info("schema = {0}".format(self.db_schema))
        self.log.info("server = {0}".format(self.db_server))
        cur = None
        try:
            cur = mysql.connect(**conn_props).cursor()
            if not self.exec_sql(cur, "SELECT SCHEMA_NAME FROM information_schema.SCHEMATA WHERE SCHEMA_NAME = '%s'" % (self.db_schema)):
                self.log.error("DB schema [%s] does not exist" % (self.db_schema))
                return False

            table_count = self.exec_sql(cur, "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '%s'" % (self.db_schema))
            self.log.info("Found {0} tables".format(table_count))
            if not table_count:
                self.log.info("Check to make sure you provided valid login credentials if you expected tables to exist in this database")
                return False

            for table in cur.fetchall():
                self.log.info("Table: {0}".format(table))

        except mysql._mysql.OperationalError, e:
            self.log.error(e)
        finally:
            if cur:
                cur.close()


class MySQLCalcDB(Component):
    """
    Provide a MYSQL database implementation.
    """

    implements(ICalcDB)

    # Configuration options for mysql
    db_server = Option('MySQLCalcDB', 'server', 'localhost',
                """Name of db server host""")

    db_user = Option('MySQLCalcDB', 'user', None,
                """Name of db user""")

    db_pass = Option('MySQLCalcDB', 'password', None,
                """Password to db""")

    db_schema = Option('MySQLCalcDB', 'schema', None,
                """The schema name in the db server""")

    def connect(self, **kwargs):
        """
        Connect to the db.
        """
        db_uri = "mysql://{0}:{1}@{2}/{3}".format(self.db_user, self.db_pass,
                                                self.db_server, self.db_schema)
        self.log.debug("Connecting to mysqldb: {0}".format(db_uri))
        self.log.debug("db_user: {0}".format(self.db_user))

        self._engine = create_engine(db_uri, echo=False)

        Session = sessionmaker(bind=self._engine)
        self._session = Session()

    def disconnect(self):
        self._session.close()
        self._engine.dispose()

    def _session_reopen(self):
        self._session.close()
        Session = sessionmaker(bind=self._engine)
        self._session = Session()

    def insert_calc(self, calc_record, hpath=None):
        """
        Insert a new calculation record to the database and return it.

        calc_record.id is ignored and overwritten in place.
        calc_record.parent_id must be the id of an existing calculation or None,
        otherwise a ValueError will be raised.

        hpath can be used to relate the calculation with a specific hierarchy
        node. hpath=None means the calculation isn't related to a hierarchy
        node. HierarchyError is raised if hpath doesn't exist.

        """

        # Check if parent_id != None and is in the DB
        if calc_record.parent_id is not None:
            try:
                self.get_calc(calc_record.parent_id)
            except ValueError, e:
                raise ValueError("parent_id: {0} for new calc_record does not exist in table".format(
                    calc_record.parent_id))
        if hpath is not None:
            node = self._get_node_from_hpath(hpath)
        else:
            node = None
        sq_cr = SQLCalcRecord.from_calc_record(calc_record)
        self._session.add(sq_cr)
        self._session.flush()
        calc_record.id = sq_cr.calc_id
        if node is not None:
            # TODO check that node doesn't already have a calc associated
            node.calc = sq_cr
            self._session.add(node)
        self._session.commit()
        return calc_record

    def get_calc(self, calc_id):
        """
        Get a CalcRecord for the given calc_id and return it.
        """

        # TODO - check if the line should be here or at another location
        #        without this line updates done to the calc_record by other processes
        #        are not seen by the current process.
        self._session_reopen()

        sq_cr = self._session.query(SQLCalcRecord).filter(SQLCalcRecord.calc_id == calc_id).first()
        if sq_cr is None:
            raise ValueError("No such calculation id: {0}".format(calc_id))

        return sq_cr.to_calc_record()

    def update_calc(self, calc_record):
        """
        Update the database according to the given record.

        The id of the given record is used to identify the record to update.

        """
        # Todo : what to do in case of non existin calc_recrod to add it ??
        # The case where id = parent_id
        if calc_record.id == calc_record.parent_id:
            raise ValueError("id: {0} == parent_id: {1} in update".format(calc_record.id, calc_record.parent_id))

        # The case where parent_id does not exists (and is not None)
        if calc_record.parent_id is not None:
            try:
                self.get_calc(calc_record.parent_id)
            except ValueError, e:
                raise ValueError("parent_id: {0} does not exist in table".format(calc_record.parent_id))

        sql_cr = self._session.query(SQLCalcRecord).filter(SQLCalcRecord.calc_id == calc_record.id).first()
        if sql_cr is None:
            raise ValueError("no such id: {0}".format(calc_record.id))

        sql_cr.update(calc_record)

        self._session.commit()

    def remove_calc(self, calc_id):
        """
        Remove a calculation from the database.

        Raises ValueError if calc_id isn't the id of an existing calculation.
        Raises CalculationRemoveError if trying to remove a calculation that has
        already been cloned.

        """
        self._session_reopen()
        sq_cr = self._session.query(SQLCalcRecord).filter(SQLCalcRecord.calc_id == calc_id).first()
        if sq_cr is None:
            raise ValueError("No such calculation id: {0}".format(calc_id))
        if sq_cr.node:
            self._session.add(sq_cr.node)
            sq_cr.node.calc = None
        self._session.delete(sq_cr)
        try:
            self._session.commit()
        except IntegrityError:
            raise CalculationRemoveError("Calculation with id={0} has been cloned and cannot be removed.".format(calc_id))

    def get_stages(self, calc_id):
        """
        Get a stages dictionary for all stages of a given calculation.

        The result is a dictionary mapping stage names to StageRecord objects.
        Raise ValueError if the calc_id isn't valid, and return an empty
        dictionary if there are no stages for an existing calculation.
        """
        self._session_reopen()

        # make sure the calc_id is valid
        self.get_calc(calc_id)

        # create the dictionary to return
        result = dict()
        for sql_stage_record in self._session.query(SQLStageRecord).filter(SQLStageRecord.calc_id == calc_id).all():
            result[sql_stage_record.name] = sql_stage_record.to_stage_record()

        return result

    def set_stage(self, stage_record):
        """
        Insert or update the database according to the given stage record, and return it.

        The calc_id and name of the given record are used to identify the record
        to update. ValueError is raised if stage_record.calc_id is not the id of
        an existing calculation. A new stage record will be inserted to the
        database in case it does not already exist.

        """
        self._session_reopen()  # TODO is this needed here? this is a set function

        # make sure the calc_id is valid
        self.get_calc(stage_record.calc_id)

        # try to get an existing sql record
        # Note: using multiple filter operation instead of a single one with
        #       expressions separted via commans since it only works for sqlalchemy > 0.75
        sql_sr = self._session.query(SQLStageRecord).filter(
            SQLStageRecord.calc_id == stage_record.calc_id).filter(
            SQLStageRecord.name == stage_record.name).first()

        if sql_sr is None:
            # insert a new record
            sql_sr = SQLStageRecord.from_stage_record(stage_record)
            self._session.add(sql_sr)
            self._session.flush()
        else:
            # update an existing record
            sql_sr.update(stage_record)

        self._session.commit()
        return stage_record

    def delete_stage(self, stage_record):
        self._session_reopen()  # TODO is this needed here? this is a set function

        # make sure the calc_id is valid
        self.get_calc(stage_record.calc_id)

        # Delete all
        # Note: using multiple filter operation instead of a single one with
        #       expressions separted via commans since it only works for sqlalchemy > 0.75
        sql_sr = self._session.query(SQLStageRecord).filter(
            SQLStageRecord.calc_id == stage_record.calc_id).filter(
            SQLStageRecord.name == stage_record.name).first()
        if sql_sr is None:
            raise ValueError("no such stage {0} id: {1}".format(
                stage_record.name, stage_record.calc_id))
        self._session.delete(sql_sr)
        self._session.flush()
        self._session.commit()
        return True

    def all_calculations(self, user=None):
        """
        Return a generator of ids of all calculations in the database.
        """
        self._session_reopen()

        if user is None:
            for sql_rec in self._session.query(SQLCalcRecord).all():
                yield sql_rec.to_calc_record()
        else:
            for sql_rec in self._session.query(SQLCalcRecord).filter_by(uid=user).all():
                yield sql_rec.to_calc_record()

    def lock(self, calc_id):
        """
        Try to lock a calculation, and return success status.

        This method returns instantly, i.e. does not wait for a lock, and
        returns True if the lock was successfully obtained, and False if not.

        """
        self._session_reopen()

        sql_rec = self._session.query(SQLCalcRecord).filter(SQLCalcRecord.calc_id == calc_id).first()
        if sql_rec is None:
            raise ValueError("No such ID = {0}".format(calc_id))

        if sql_rec.calc_lock is True:
            lock_info = self.get_lock_info(calc_id)
            self.log.debug("Lock on calc_id {0} is already taken".format(calc_id))
            if lock_info is None:
                self.log.debug("Lock info is None (maybe it was already unlocked")
            else:
                self.log.debug("Pid: {0}, Host: {1}".format(lock_info.pid, lock_info.host))
            self._session.rollback()
            return False
        else:
            sql_rec.calc_lock = True
            lock_info = SQLLockInfo()
            lock_info.calc_id = calc_id
            lock_info.host = socket.gethostname()
            lock_info.pid = os.getpid()
            lock_info.start = now()
            self._session.add(lock_info)
            self._session.flush()
            self._session.commit()
            self.log.debug("Lock granted on calc_id {0} pid={0} host={0} ".format(
                calc_id, lock_info.pid, lock_info.host))
            return True

    def unlock(self, calc_id):
        """
        Unlock the calculation and return instantly.

        This does nothing if it isn't locked.

        """
        self._session_reopen()
        sql_rec = self._session.query(SQLCalcRecord).filter(SQLCalcRecord.calc_id == calc_id).first()
        if sql_rec is None:
            raise ValueError("No such ID = {0}".format(calc_id))

        if sql_rec.calc_lock is True:
            self.log.debug("Unlock: id={0} (lock is True, unlocking)".format(calc_id))
            sql_rec.calc_lock = False
            self._session.query(SQLLockInfo).filter(SQLLockInfo.calc_id==calc_id).delete()
            self._session.commit()
        else:
            self.log.debug("Unlock: id={0} (lock is False, doing nothing)".format(calc_id))
            self._session.rollback()

    def get_lock_info(self, calc_id):
        """
        Get a LockInfo for the given calc_id (if it is locked).
        """

        # TODO - check if the line should be here or at another location
        #        without this line updates done to the calc_record by other processes
        #        are not seen by the current process.
        self._session_reopen()

        sq_li = self._session.query(SQLLockInfo).filter(SQLLockInfo.calc_id == calc_id).first()
        if sq_li is None:
            return None

        return sq_li.to_lock_info()

    def locked_calculations(self):
        """
        Return a list of calc_records which are currently locked
        """
        self._session_reopen()
        locked_sql_calcs = self._session.query(SQLCalcRecord).filter(SQLCalcRecord.calc_lock == True)
        locked_calcs = []
        for locked_sql_calc in locked_sql_calcs:
            #self.log.debug("Got locked calc: {0}".format(locked_sql_calc.calc_id))
            locked_calcs.append(locked_sql_calc.to_calc_record())
        return locked_calcs

    def _get_node_from_hpath(self, hpath):
        """
        Get an SQLNodeRecord by absolute hpath.
        """
        node = self._session.query(SQLNodeRecord).filter(SQLNodeRecord.name == '').filter(SQLNodeRecord.parent_id == None).first()
        current = ['']
        names = hpath.split(NODE_SEPARATOR)
        for name in names:
            if name == '':
                continue
            current.append(name)
            if name in node.children:
                node = node.children[name]
            else:
                raise HierarchyError("Node {0!r} does not exist.".format(
                    NODE_SEPARATOR.join(current)))
        return node

    def _get_hpath_from_node(self, node):
        position = []
        while node is not None:
            position.insert(0, node.name)
            node = node.parent
        return NODE_SEPARATOR.join(position)

    def new_node(self, hpath, create_parents=False):
        """
        Create a new node in the hierarchy.

        hpath is the full path to the node.
        If create_parents is False, the operation will raise HierarchyError if the
        parent doesn't exist. If create_parents is True, all needed parents will
        be created.

        """
        self._session_reopen()
        parent_hpath, ignore, name = hpath.rpartition(NODE_SEPARATOR)
        if not create_parents:
            parent = self._get_node_from_hpath(parent_hpath)
        else:
            parent = self._get_node_from_hpath('/')
            for n in parent_hpath.split(NODE_SEPARATOR):
                if n == '':
                    continue
                if n in parent.children:
                    parent = parent.children[n]
                else:
                    parent = SQLNodeRecord(name=n, parent=parent)
                    self._session.add(parent)
        if name in parent.children:
            raise HierarchyError("Node {0!r} already exists.".format(hpath))
        else:
            node = SQLNodeRecord(name=name, parent=parent)
            self._session.add(node)
            self._session.commit()

    def move_node(self, src, dst, create_parents=False):
        """
        Move a node to a new name, and possibly a new parent.

        The new parent must already exist, unless create_parents is True, and
        then it will be created.

        """
        self._session_reopen()

        src = src.rstrip('/')
        dst = dst.rstrip('/')

        src_path, src_name = posixpath.split(src)
        dst_path, dst_name = posixpath.split(dst)

        if src_path == dst_path:
            node = self._get_node_from_hpath(src)
            node.name = dst_name
            self._session.commit()
        if src_path != dst_path:
            try:
                new_parent = self._get_node_from_hpath(dst_path)
            except HierarchyError:
                if create_parents:
                    new_parent = self.new_node(dst_path, True)
                else:
                    raise
            # the following is needed as new_node changed session
            self._session_reopen()
            node = self._get_node_from_hpath(src)
            new_parent = self._get_node_from_hpath(dst_path)
            node.parent = new_parent
            node.name = dst_name
            self._session.commit()

    def _remove_node_helper(self, node):
        """
        Doesn't reopen or commit the session, and the node and all it's
        children.

        """
        for c in node.children.itervalues():
            self._remove_node_helper(c)
        self._session.delete(node)

    def remove_node(self, hpath, remove_children=False):
        """
        Remove a node.

        If the node has children, HierarchyError will be raised unless
        remove_children is True, in which case all the subtree will be removed
        recursively.

        Calculations associated with the nodes will not be removed, but will
        become node-less.

        """
        self._session_reopen()
        node = self._get_node_from_hpath(hpath)
        if node.parent_id is None:
            raise HierarchyError("Cannot remove root node.")
        if len(node.children) > 0 and not remove_children:
            raise HierarchyError("Node {0!r} has children.".format(hpath))
        self._remove_node_helper(node)
        self._session.commit()

    def list_node_children(self, hpath):
        """
        Return a generator of names of children of the given node.

        node is given by full hpath, and the results are child names, relative to
        the given node.

        """
        self._session_reopen()
        return self._get_node_from_hpath(hpath).children.iterkeys()

    def get_node_from_calc(self, calc_record):
        """
        Returns the hpath to the node that the given calc is related to.

        Returns None if the given calculation isn't related to any hierarchy
        node.

        """
        self._session_reopen()
        sq_cr = self._session.query(SQLCalcRecord).filter(SQLCalcRecord.calc_id == calc_record.id).first()
        if sq_cr is None:
            raise ValueError("No such calculation id: {0}".format(calc_id))
        if sq_cr.node is None:
            return None
        else:
            return self._get_hpath_from_node(sq_cr.node)

    def get_calc_from_node(self, hpath):
        """
        Returns the CalcRecord of the calculation related to the given hpath.

        None is returned if the given hpath isn't related to any node.
        HierarcyError is raied if the given hpath doesn't exist.

        """
        self._session_reopen()
        node = self._get_node_from_hpath(hpath)
        if node.calc is None:
            return None
        else:
            return node.calc.to_calc_record()
