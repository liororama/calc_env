import inspect
import textwrap
import re

from externals.recordtype import recordtype, NO_DEFAULT, _check_name


def source_with_defaults_and_doc(cls, doc=""):
    res = re.sub(
        '(?m)(?<=def __init__)(.*):$',
        inspect.formatargspec(*inspect.getargspec(cls.__init__)) +
        ":\n" +
        '\n'.join(' ' * 12 + l for l in ('"""' + textwrap.dedent(doc) + '\n"""').splitlines()),
        cls._source)
    res = res.replace(' ' * 8 + '"' + cls.__doc__ + '"',
                      '\n'.join(' ' * 8 + l for l in ('"""' + cls.__doc__ + '\n' + textwrap.dedent(doc) + '\n"""').splitlines()))
    return res


def enum_source(typename, possible_values, doc=""):
    _check_name(typename, is_type_name=True)
    seen_names = set()
    for v in possible_values:
        if not isinstance(v, basestring):
            raise ValueError("Possible enum values must be strings: "
                             "{0!r}".format(v))
        _check_name(v, False, seen_names)
        seen_names.add(v)
    result = "class {0}(object):\n".format(typename)
    result += '\n'.join("    " + l for l in ('"""' + textwrap.dedent(doc) + '\n"""').splitlines())
    for v in possible_values:
        result += "\n    {0} = {0!r}".format(v)
    return result

sources = [

    source_with_defaults_and_doc(
        recordtype("LockInfo",
                   ("calc_id", "pid", "host", "timestamp"),
                   default=None),
        """\
        calc_id - calc id of locked calculation
        pid - pid of locking process
        host - hostname of locking process
        timestamp - datetime of lock acquire time
        """),

    source_with_defaults_and_doc(
        recordtype("CalcRecord",
                   ("id",
                    "title",
                    "description",
                    "parent_id",
                    "uid",
                    "path",
                    "time_create",
                   ),
                   default=None),
        """\
        id - auto-generated - a unique number
        title - user-provided - short name of the calc
        description - user-provided description
        parent_id - id of parent (can be None)
        uid - UID of the owner of this calculation
        path - Path to the directory of this calculation
        time_create - Time of creation
        """),

    source_with_defaults_and_doc(
        recordtype("StageRecord",
                   ("calc_id",
                    "name",
                    "status",
                    "rm_job",
                    "time_submit",
                    "time_begin",
                    "time_end",
                   ),
                   default=None),
        """\
        calc_id - id of the relevant calculation record
        name - name of the stage
        status - a StageStatus value
        rm_job - string representation of the rm_job
        time_submit - Time of submission to resource manager
        time_begin - Time of beginning of execution
        time_end - Time of end of execution
        """),

    enum_source("StageStatus",
                ("SUBMITTED",
                 "RUNNING",
                 "DONE",
                 "FAILED",
                ),
                """Possible stage statuses"""),

    enum_source("RMJobStatus",
                ("PENDING",
                 "RUNNING",
                 "SUSPENDED",
                 "NOT_FOUND",
                ),
                """Possible results of IRMJob.get_status"""),

    ]

print '\n\n'.join(sources)
