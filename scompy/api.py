"""
Python API for calc_env

Providing the Calculation class to be used by the user
"""
import os
import subprocess
import time
import dateutil.parser
import datetime
import sys
import select
import signal
import logging
from pwd import getpwuid

import environment

from structures import StageStatus
from environment import Environment
from calculation import Calculation as InternalCalculation
from core import to_local_timezone
from files import Files
from permissions import PermissionError

# Name of per user directory for private configurations
USER_CONFIG_DIR = ".scompy"

# Name of user start code file
USER_STARTUP_FILE = "start.py"

# Name of user private scompy config file
USER_SCOMPY_CONFIG = "config.py"


def load_user_scompy_config():
    """
    Load the user private configuration file
    """
    user_home = os.environ["HOME"]
    user_cfg_dir = os.path.join(user_home, USER_CONFIG_DIR)
    if not os.path.isdir(user_cfg_dir):
        return

    user_config_file = os.path.join(user_cfg_dir, USER_SCOMPY_CONFIG)
    if not os.path.exists(user_config_file):
        return
    g = {}
    l = {}
    try:
        execfile(user_config_file, g, l)
    except Exception, e:
        print "Error loading user private config file from {0}".format(user_config_file)
        print e
        return

    user_config = {}
    if "scompy_dir" in l:
        user_config['scompy_dir'] = l['scompy_dir']
    return user_config


def load_user_startup_code():
    user_home = os.environ["HOME"]
    user_cfg_dir = os.path.join(user_home, USER_CONFIG_DIR)
    if not os.path.isdir(user_cfg_dir):
        return

    user_start_file = os.path.join(user_cfg_dir, USER_STARTUP_FILE)
    if not os.path.exists(user_start_file):
        return
    sys.path.append(user_cfg_dir)
    file_to_import = USER_STARTUP_FILE[:-3]
    __import__(file_to_import)


def debug_on():
    """
    Setting/Unsetting the debug level of the system
    """
    environment.Environment().set_log_level(logging.DEBUG)


def debug_off():
    """
    Turrning off debug
    """
    environment.Environment().set_log_level(logging.INFO)


def from_existing(id):
    """
    Return a Calculation object from the given id
    """
    return Calculation.from_exisiting(id)


def from_hpath(hpath):
    """
    Return a Calculation object from the given hpath
    """
    env = Environment()
    cr = env.db.get_calc_from_node(hpath)
    if cr is None:
        return None  # TODO maybe raise exception?
    else:
        return Calculation.from_exisiting(cr.id)


def walk(top, topdown=True):
    """Hierarchy node tree generator (a'la os.walk).

    For each node in the hierarchy tree rooted at top (including top
    itself), yields a 3-tuple

        hpath, children, calc

    hpath is a string, the hierarchy path to the node.

    children is a list of names of child nodes of the node given by hpath.

    calc is the calculation associated to the node in hpath, or None if there isn't one.

    If optional arg 'topdown' is true or not specified, the triple for a
    node is generated before the triples for any of its children
    (nodes are generated top down).  If topdown is false, the triple
    for a node is generated after the triples for all of its
    children(nodesare generated bottom up).

    When topdown is true, the caller can modify the children list in-place
    (e.g., via del or slice assignment), and walk will only recurse into the
    children whose names remain in the children list; this can be used to prune
    the search, or to impose a specific order of visiting. Modifying children
    when topdown is false is ineffective, since the nodes in children have
    already been generated by the time children itself is generated.

    Example:

    import scompy.api
    for hpath, children, calc in scompy.api.walk('/path/to/some_node'):
        if calc:
            print hpath
            print calc
            print
        if 'backup' in children: # don't visit nodes named backup
            children.remove('backup')


    l = [calc for hpath, child, calc in api.walk('tests3') if calc]

    """
    from posixpath import join

    env = Environment()

    children = list(env.db.list_node_children(top))
    try:
        calc = env.db.get_calc_from_node(top)
    except PermissionError:
        calc = None
    if calc is not None:
        calc = Calculation.from_exisiting(calc.id) # convert to api.Calculation
    if topdown:
        yield top, children, calc
    for name in children:
        path = join(top, name)
        for x in walk(path, topdown):
            yield x
    if not topdown:
        yield top, children, calc


def move_node(src, dst, create_parents=False):
    """
    Move a node to a new name, and possibly a new parent.

    The new parent must already exist, unless create_parents is True, and
    then it will be created.

    """
    env = Environment()
    env.db.move_node(src, dst, create_parents)


def remove_node(self, hpath, remove_children=False):
    """
    Remove a node.

    If the node has children, HierarchyError will be raised unless
    remove_children is True, in which case all the subtree will be removed
    recursively.

    Calculations associated with the nodes will not be removed, but will
    become node-less.
    """
    # TODO maybe add interactive questioning like "Are you sure?"
    env = Environment()
    env.db.remove_node(hpath, remove_children=False)


def my_calcs():
    """
    Return all calculations of current user
    """
    uid = os.getuid()
    return Calculation.all_calculations(user=uid)


def my_running_calcs():
    """
    Return a list of all the calculations of the current user which are running
    """


def get_folder_size(folder):
    total_size = os.path.getsize(folder)
    for item in os.listdir(folder):
        itempath = os.path.join(folder, item)
        if os.path.isfile(itempath):
            total_size += os.path.getsize(itempath)
        elif os.path.isdir(itempath):
            total_size += getFolderSize(itempath)
    return total_size


def timedelta_to_str(time_delta):
    tsec = int(time_delta.total_seconds())
    hours = tsec/3600
    minutes = (tsec%3600)/60
    seconds = tsec - hours*3600 - minutes*60
    return "{0}:{1:02d}:{2:02d}".format(hours, minutes, seconds)


def running_time(ic):
    """
    Calculate the running time of an internal calculation, return printable string

    ic - is InternalCalculation
    """
    #print "Status:", ic.status

    if ic.status is CalcStatus.RUNNING:
        return timedelta_to_str(datetime.datetime.utcnow() - ic.times.start_run)
    elif ic.status is CalcStatus.DONE:
        return timedelta_to_str(ic.times.done - ic.times.start_run)
    else:
        return "-----"


def psr(calc_list):
    """
    Print status report
    """
    print status_report(calc_list)


class APICalcStatus(object):

    SUBMITTED = 'SUBMITTED'
    RUNNING = 'RUNNING'
    DONE = 'DONE'
    FAILED = 'FAILED'


def _calc_status_str(ic):
    """
    Return a human readable status string of calculation and runtime

    SUBMITTED | RUNNING | DONE | FAILED

    ###ready   : when no stage in the go sequence in done|waiting| fail| running

    waiting : when stages in the go sequence are running or waiting not all are
              done and there is no fail.
    running : when there is one stage in the go_sequence which is running
    done    : when all stages in the go sequence are in done
    fail    : when one stage in the go_sequence is in fail

    runtime - the accumulated running time of stages in the second part of the go
              sequence. up to the first stage which is failed or up to the end

    """
    stages_info = ic.stages
    go_sequence = ic.go_sequence

    go_s_len_1 = len(go_sequence[0])
    go_s_len_2 = len(go_sequence[1])
    go_s_len = go_s_len_1 + go_s_len_2

    running = 0
    submitted = 0
    done = 0
    failed = 0

    # Counting the number of go_sequence stages in each status
    for i in range(0, go_s_len):
        si = stages_info[i]
        if si["status"] == StageStatus.RUNNING:
            running += 1
        elif si["status"] == StageStatus.FAILED:
            failed += 1
        elif si["status"] == StageStatus.DONE:
            done += 1
        elif si["status"] == StageStatus.SUBMITTED:
            submitted += 1

    # Setting the calculation status according to the stages status
    status_str = "N/A"
    if done == go_s_len:
        status_str = APICalcStatus.DONE
    elif failed > 0:
        status_str = APICalcStatus.FAILED
    elif running > 0:
        status_str = APICalcStatus.RUNNING
    elif submitted > 0:
        status_str = APICalcStatus.SUBMITTED

    time_str = "N/A"
    time_delta = None
    if status_str is APICalcStatus.DONE:
        time_delta = stages_info[go_s_len-1]['time_end'] - stages_info[go_s_len_1]['time_begin']
    elif status_str is APICalcStatus.SUBMITTED:
        time_delta = None
    elif status_str is APICalcStatus.RUNNING:
        time_delta = datetime.datetime.utcnow() - stages_info[go_s_len_1]['time_begin']
    elif status_str is APICalcStatus.FAILED:

        failed_stage_index = g_s_len_1
        for i in range(g_s_len_1, go_s_len):
            if stages_info[i]['status'] == StageStatus.FAILED:
                break
            failed_stage_index += 1
        time_delta = stages_info[failed_stage_index]['time_end'] - stages_info[go_s_len_1]['time_begin']

    if time_delta is not None:
        time_str = timedelta_to_str(time_delta)

    return (status_str, time_str)

# the old status_report
#
#def status_report(calc_list):
#    """
#    Print a status report about the provided list of jobs
#    """
#    total_inprep = 0
#    total_submited = 0
#    total_running = 0
#    total_done = 0
#    total = 0
#
#    report = ""
#    report += "{0:<10s} {1:<8s} {2:<20s} {3:<15s} {4:<10s} {5:<30s}\n".format(
#        "ID", "Parent", "Title", "Status", "RunTime", "Calc Status")
#
#    for c in calc_list:
#        ic = InternalCalculation.from_exisiting(c.id)
#        total += 1
#
#        status_str, runtime_str = _calc_status_str(ic)
#
#        # Checking the status and updating the apropriate counter accordingly
#        #status = ic.status
#        #if status is CalcStatus.IN_PREPARATION:
#        #    total_inprep += 1
#        #elif status is CalcStatus.SUBMITTED:
#        #    total_submited += 1
#        #elif status is CalcStatus.RUNNING:
#        #    total_running += 1
#        #elif status is CalcStatus.DONE:
#        #    total_done += 1
#
#        #runtime = running_time(ic)
#
#        report += "{0:<10} {1:<8} {2:<20s} {3:<15s} {4:<10s} {5:<30s}\n".format(
#            c.id, ic.parent_id, ic.title, status_str, runtime_str, ic.get_status_line())
#
#    #report += "\n"
#    #report += "Total {0}: Running {1} Done: {2}: Waiting: {3} In Prep {4}\n".format(
#    #    total, total_running, total_done, total_submited, total_inprep)
#
#    return report


# the new multithreaded status_report
def _get_calcspec_status(ic):
    return ic.get_status_line()


def status_report(calc_list, max_pool=32):
    """
    Print a status report about the provided list of jobs
    """
    from multiprocessing.dummy import Pool

    ic_list = [InternalCalculation.from_exisiting(c.id) for c in calc_list]
    status_x_list = [_calc_status_str(ic) for ic in ic_list]

    if max_pool == 0:
        calcspec_status_list = [ic.get_status_line() for ic in ic_list]
    else:
        pool = Pool(processes=min((max_pool, len(calc_list))))
        calcspec_status_list = pool.map(_get_calcspec_status, ic_list)

    status_report_lines = ["{0:<10} {1:<8} {2:<20s} {3:<15s} {4:<10s} {5:<30s}".format(
        ic.id,
        ic.parent_id,
        ic.title,
        status_x[0],
        status_x[1],
        calcspec_status
    ) for ic, status_x, calcspec_status in zip(ic_list, status_x_list, calcspec_status_list)]

    report = ""
    report += "{0:<10s} {1:<8s} {2:<20s} {3:<15s} {4:<10s} {5:<30s}\n".format(
        "ID", "Parent", "Title", "Status", "RunTime", "Calc Status")
    report += "\n".join(status_report_lines)
    return report


def test_stdin(period=0.0):
    if select.select([sys.stdin, ], [], [], period)[0]:
        return True
    else:
        return False

global_got_sig = False


def signal_handler(signum, frame):
    #print 'Signal handler called with signal', signum
    global global_got_sig
    global_got_sig = True


def watch(calc_list, count=0, period=5, printer=status_report, clear=True):
    """
    An utility method to periodicall print the status of a list of jobs

    period - the number of seconds to wait between sleeps
    printer - a printing function that accepts a calc_generator and return a string
    clear - Clear the screen (True/False)
    """
    # Set the signal handler and a 5-second alarm
    signal.signal(signal.SIGINT, signal_handler)

    global global_got_sig
    index = 0
    report = ""
    while True:
        report = ""
        index += 1

        report += "========= count: {0}  sleep time : {1} =========\n".format(index, period)
        report += printer(calc_list)

        # The clear is done just before the final print, so we have a clean change
        # of display without noticing the delays which the printer may have
        if clear:
            os.system("clear")
        print report
        time.sleep(period)
        if global_got_sig:
            #print "Got siganl"
            global_got_sig = False
            signal.signal(signal.SIGINT, signal.SIG_DFL)
            break

        if index == count:
            break

    signal.signal(signal.SIGINT, signal.SIG_DFL)


def _locks_printer(dummy):
    """
    Print status about currently locked calculations
    """
    report = ""
    report += "{0:<10} {1:<10} {2:<10} {3:<10}\n".format("ID", "PID", "HOST", "START")
    for c in Calculation.locked_calcs():
        li = environment.Environment().db.get_lock_info(c.id)
        report += "{0:<10} {1:<10} {2:<10} {3:<20}\n".format(c.id, li.pid, li.host, str(li.timestamp))
    return report


def monitor_locks(printer=_locks_printer, count=0, period=5, clear=True):
    """
    An utility method to periodically print information about locks
    """
    watch(None, printer=printer, count=count, period=period, clear=clear)


# Some helper command to do actions on list of calculation objects
def create_all_applier(method, doc=None):
    def on_all(seq, *args, **kwargs):
        for obj in seq:
            getattr(obj, method)(*args, **kwargs)
    on_all.__doc__ = doc
    return on_all

go = create_all_applier('go', "run go() for all calculations")
reset = create_all_applier('reset', "run reset() for all calculations")
cancel = create_all_applier('stop', "Cancel all calculations")


def _calc_property(name):
    def fget(self):
        c = InternalCalculation.from_exisiting(self.id)
        return getattr(c, name)

    def fset(self, value):
        c = InternalCalculation.from_exisiting(self.id)
        setattr(c, name, value)

    return property(fget, fset, None, getattr(InternalCalculation, name).__doc__)

#from IPython.core.autocall import IPyAutocall


class Calculation(object):
    """
    Providing users access to the calculation abilities.
    """


    # Definition of the class data members as properties (get,set) from the internal calculation
    title = _calc_property("title")
    description = _calc_property("description")
    parent_id = _calc_property("parent_id")
    uid = _calc_property("uid")
    path = _calc_property("path")
    stages = _calc_property("stages")

    @property
    def hpath(self):
        """Get the calculation hpath"""
        env = Environment()
        calc_record = env.db.get_calc(self.id)
        return env.db.get_node_from_calc(calc_record)

    def name(self, ):
        pass

    @classmethod
    def from_exisiting(cls, calc_id):
        """
        Return a calculation object for an existing calculation id.
        This routine only try to create internal calculation. This means
        the calculation exists
        """

        ic = InternalCalculation.from_exisiting(calc_id)

        return cls(calc_id)

    @classmethod
    def all_calculations(cls, user=None):
        """
        Return a generator of Calculation objects.
        """
        env = Environment()
        for ic in InternalCalculation.all_calculations(user):
            yield cls(ic.id)

    @classmethod
    def locked_calcs(cls):
        """
        Return a list of locked calculations
        """
        for ic in InternalCalculation.locked_calculations():
            yield cls(ic.id)

    @classmethod
    def by_parent_id(cls, parent_id):
        for ic in InternalCalculation.all_calculations():
            if ic.parent_id == parent_id:
                yield cls(ic.id)

    @classmethod
    def new(cls, title, description, hpath):
        """
        Create a new python api calculation

        TODO maybe open an editor for description?
        """
        env = Environment()
        if hpath is not None:
            env.db.new_node(hpath, True)
        ic = InternalCalculation.new(title=title, description=description, hpath=hpath)
        return cls(ic.id)

    def __init__(self, calc_id):
        """
        Creating a Calculation object
        """
        # generating an internal object in order to check that the calc_id
        # actually exists
        ic = InternalCalculation.from_exisiting(calc_id)

        self.id = calc_id
        self.files = Files(ic.path)

    def get_stage(self, stage):
        ic = InternalCalculation.from_exisiting(self.id)
        return ic.get_stage(stage)

    def _time_fmt(self, t):
        if t is None:
            return "-"
        return t.strftime('%Y-%m-%d %H:%M:%S')

    def __str__(self):
        """
        Print a calculation
        """

        ic = InternalCalculation.from_exisiting(self.id)

        s = ""
        s += "HPath:      {0}\n".format(self.hpath)
        s += "ID:         {0}\n".format(ic.id)
        s += "Path        {0}\n".format(ic.path)
        s += "Title:      {0}\n".format(ic.title)
        s += "Parent:     {0}\n".format(ic.parent_id)
        s += "Owner:      {0}\n".format(getpwuid(ic.uid).pw_name)
        s += "Status:     {0}\n".format(ic.get_status_line())

        # Adding stages information
        stages_info = ic.stages
        s += "Stages:\n"
        go_sequence = ic.go_sequence
        for stage_info in stages_info:
            if stage_info["name"] in go_sequence[0]:
                go_sequence_char = "l"
            elif stage_info["name"] in go_sequence[1]:
                go_sequence_char = "r"
            else:
                go_sequence_char = " "

            s += "{0:<2}  {1:<20} {2:<10} {3:<19} {4:<19} {5:<19}\n".format(
                go_sequence_char,
                stage_info["name"], stage_info["status"],
                self._time_fmt(stage_info["time_submit"]),
                self._time_fmt(stage_info["time_begin"]),
                self._time_fmt(stage_info["time_end"]))

        return s

    def __repr__(self):
        ic = InternalCalculation.from_exisiting(self.id)

        return "Calculation(calc_id={0})".format(ic.id)

    def do(self, stage_name):
        """
        Do a stage locally
        """
        ic = InternalCalculation.from_exisiting(self.id)
        ic.do_stage(stage_name, None)

    def submit(self, stage_name, **extra_rm_info):
        """
        Submit a stage to the resource manager
        """
        ic = InternalCalculation.from_exisiting(self.id)
        ic.submit_stages(stage_name, **extra_rm_info)

    def go(self, **extra_rm_info):
        """
        Submit the calculation
        """
        ic = InternalCalculation.from_exisiting(self.id)
        ic.go(**extra_rm_info)

    def clone(self, hpath):
        """
        Clone the calculation, returning a new api.calculation object
        """
        c = InternalCalculation.from_exisiting(self.id)
        env = Environment()
        if hpath is not None:
            env.db.new_node(hpath, True)
        c2 = c.clone(hpath=hpath)
        return self.__class__(c2.id)

    def reset(self, stage_name=None):
        """
        Reset the stage removing any indication of it being run before, allowing it
        to run again.

        If stage_name = None, then all the stages in the go_sequence of the calculation
        are reset
        """

        ic = InternalCalculation.from_exisiting(self.id)
        if stage_name is not None:
            ic.reset_stage(stage_name)
        else:
            ic.reset()

    def cancel(self):
        """
        Cancel the calculation
        """

    def cd(self):
        """
        Change current directory to the calculation directory
        """
        os.chdir(self.path)

    def du(self):
        """
        Calculate the size of the calculation directory and return the results in MB
        """
        return float(get_folder_size(self.path))/(1024.0*1024.0)

    def tail_stage(self, stage, filename=None, lines=10):
        """
        Run tail on specific stage
        """
        ic = InternalCalculation.from_exisiting(self.id)

        if filename is None:
            filename = os.path.join(environment.CALC_INTERNAL_DIR_NAME,  "stage_" + stage + ".out")
        filename = os.path.join(self.path, filename)
        if not os.path.isfile(filename):
            print "file to tail [0] does not exist".format(filename)
            return False

        print "Tailing: stage: {0}".format(stage)
        print "         file:  {0}".format(filename)
        print "-"*50
        p = subprocess.Popen(["tail", "-n{0}".format(lines), "-f", filename])
        was_running = False
        try:
            while True:
                stage_obj = ic.get_stage(stage)
                if stage_obj is None or stage_obj.status is not StageStatus.RUNNING:
                    msg =  'Stage "{0}" is not running {1}'.format(stage, "anymore" if was_running is True else "")
                    print '-'*len(msg)
                    print msg
                    print '-'*len(msg), "\n"

                    p.terminate()
                    return True
                else:
                    was_running = True
                time.sleep(1)
            p.wait()
        except KeyboardInterrupt:
            p.terminate()
            return False

    def tail_go(self):
        """
        Tail the go sequence stages
        """
        ic = InternalCalculation.from_exisiting(self.id)

        stages_info = ic.stages
        go_sequence = ic.go_sequence
        for stage_name in (go_sequence[0] + go_sequence[1]):
            stage = ic.get_stage(stage_name)
            if stage is None or stage.status is StageStatus.FAILED or stage.status is StageStatus.DONE:
                print "Stage '{0}' is not running".format(stage_name)
                continue

            self.tail_stage(stage_name)

    def tail(self, stage=None, filename=None, lines=10):

        #if filename is None:
        #    filename = InternalCalculation.OUTPUT_FILENAME
        #filename = os.path.join(self.path, filename)

        ic = InternalCalculation.from_exisiting(self.id)
        stages_info = ic.stages

        try:
            while True:
                for stage_info in stages_info:
                    stage = ic.get_stage(stage_info["name"])
                    if stage is not None and stage.status is StageStatus.RUNNING:
                        res = self.tail_stage(stage_info["name"], filename=filename, lines=lines)
                        if res is False:
                            print "Stopping tail"
                            return
                time.sleep(1)
                print ".",
                sys.stdout.flush()
        except KeyboardInterrupt:
            print "Stopping tail"


    def edit(self, file_name=None, editor=None):
        """
        Edit a file from the calculation directory.
        If the file_name argument is not given and the calcspec for the calculation
        provides a default_edit file this file is edited
        """

        if file_name is None:
            print "No file was provided"
            return

        path = os.path.join(self.path, file_name)
        editor = os.environ.get("EDITOR", "vi")
        p = subprocess.Popen(editor + " " + file_name, shell=True) # TODO fix this to [] of arguments
        pid, sts = os.waitpid(p.pid, 0)
