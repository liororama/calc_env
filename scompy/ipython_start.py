#! /usr/bin/env python

# Starting the scompy system

import sys
import os
import argparse
import logging

import scompy.environment
from scompy.environment import Environment
from scompy.api import Calculation
from scompy.api import Calculation as Calc

import scompy.api as api
from scompy.api import debug_on, debug_off
from scompy.api import from_existing
from scompy.api import my_calcs
from scompy.api import my_running_calcs
from scompy.api import watch
import scompy.version
version = scompy.version.__version__

calc_env_banner = """
===========================================
Scompy - the power to compute
v: {0}
===========================================

Type scompy_101 to print a few help lines

        `""==,,__
        `"==..__"=..__ _    _..-==""_
             .-,`"=/ /\ \""/_)==""``
            ( (    | | | \/ |
             \ '.  |  \;  \ /
              |  \ |   |   ||
         ,-._.'  |_|   |   ||
        .\_/\     -'   ;   Y
       |  `  |        /    |-.
       '. __/_    _.-'     /'
   jgs        `'-.._____.-'
""".format(version)

description_str = """
scompy: An python environment for managment of scientifc calculations

scompy is a ....
"""

authors_str = """ Authors: Oded Padon, Lior Amar """

SYSTEM_NAME = "scompy"


def parse_cmd_line():
    # parse command line options
    parser = argparse.ArgumentParser(description=description_str, epilog=authors_str)

    # General options
    parser.add_argument("-d", "--debug", action="store_true", default=False, dest="debug",
                        help="Turn on debugging")
    parser.add_argument("--no-banner", action="store_true", default=False, dest="no_banner",
                        help="Do not print banner")
    parser.add_argument("--no-user", action="store_true", default=False, dest="no_user_startup",
                        help="Skip user startup files")

    parser.add_argument("--autocall", action="store_true", default=False, dest="autocall",
                        help="Turn on IPython autocall on full")

    parser.add_argument("env_dir", nargs='?', action="store", default=None,
                        help="The scompy environment directory")
    options = parser.parse_args()
    return options


class scompy_101(object):
    """
    A Pseuodo object that provides the calcenv_101 repr
    """
    def __repr__(self, ):

        return """
Usefull commands:
-------------------------------------
    c1 = from_existing(1)
    c2 = c1.clone()
    c2.cd()
    c2.edit()
    c2.submit()
    print c2

    """
scompy_101 = scompy_101()


def display_scompy_banner(options):
    print calc_env_banner
    print "Environment directory: {0}".format(options.env_dir)


def main():

    options = parse_cmd_line()
    user_config = api.load_user_scompy_config()
    # TODO do this in a more organized fasion and add more parameters
    if options.env_dir is None and 'scompy_dir' in user_config:
        options.env_dir = user_config['scompy_dir']

    env = Environment(options.env_dir)
    if options.debug:
        env.set_log_level(logging.DEBUG)

    ## The banner
    ## Nothing should be printed after this !!!
    if not options.no_banner:
        display_scompy_banner(options)

    # Allowing user defined code to run here
    if not options.no_user_startup:
        api.load_user_startup_code()

    #Running IPython
    from IPython.config.loader import Config

    try:
        get_ipython
    except NameError:
        banner=exit_msg=''
        cfg = Config()
        prompt_config = cfg.PromptManager
        prompt_config.in_template = 'Scompy <\\#>: '
        prompt_config.in2_template = '   .\\D.: '
        prompt_config.out_template = 'Out<\\#>: '
    else:
        banner = '*** Nested interpreter ***'
        exit_msg = '*** Back in main IPython ***'
        cfg = Config()

    # Turning on auto call in ipython
    if options.autocall:
        cfg.TerminalInteractiveShell.autocall = 2

    # Loading heirarchy compleation
    import scompy.ipy_completer
    scompy.ipy_completer.load_ipython_extension()

    return True

if __name__ == "__main__":
    res = main()
    int_res = 0
    if res is True:
        int_res = 0
    else:
        int_res = 1
    sys.exit(int_res)
