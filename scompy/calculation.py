"""
This module contains the Calculation class.
"""

import os
import datetime
import traceback
import shutil
import time

from core import now, ICalc
from structures import CalcRecord, StageRecord, StageStatus, RMJobStatus
import events
from environment import Environment
from environment import CALC_INTERNAL_DIR_NAME, CALCSPEC_MODULE_NAME
from permissions import PermissionSystem, PermissionError
import calcspec_driver


class CalculationStageError(Exception):
    pass


class CalculationResetError(Exception):
    pass


class LockError(Exception):
    """
    An error raised when a lock is required but could not be obtained.
    """
    pass


class lazy(object):
    """
    Lazy property decorator

    Taken from: http://code.activestate.com/recipes/363602-lazy-property-evaluation/

    Example usage:

    class SomeClass(object):
        @lazy
        def someprop(self):
            print 'Actually calculating value'
            return 13

    """
    def __init__(self, calculate_function):
        self._calculate = calculate_function

    def __get__(self, obj, _=None):
        if obj is None:
            return self
        value = self._calculate(obj)
        setattr(obj, self._calculate.func_name, value)
        return value


def _calc_record_property(name, readonly=False, doc=None):
    def fget(self):
        return getattr(self._calc_record, name)

    def fset(self, value):
        self._require_write_permission()
        setattr(self._calc_record, name, value)
        self._save_to_db()

    if readonly:
        fset = None

    return property(fget, fset, None, doc)


class Calculation(object):
    """
    A calculation in the calc_env infrastructure.

    Members:
    calc_record - CalcRecord of this calculation
    cc - Object implementing ICalc. Loaded from the calculation specifications.
    env - Environment instance.

    """

    OUTPUT_FILENAME = "calc_out.txt"

    # Public accessors with automatic save to db
    id = _calc_record_property("id", readonly=True, doc="ID of calculation")
    title = _calc_record_property("title", readonly=False, doc="Title of calculation")
    description = _calc_record_property("description", readonly=False, doc="Description of calculation")
    parent_id = _calc_record_property("parent_id", readonly=True, doc="Parent ID")
    uid = _calc_record_property("uid", readonly=True, doc="UID of user owning calculation")
    path = _calc_record_property("path", readonly=True, doc="Path to calculation directory")
    time_create = _calc_record_property("time_create", readonly=True, doc="Time of creation of the calculation")

    @property
    def go_sequence(self):
        """
        Return the go sequence of this calculation
        """
        calc_info = self.cc.get_info()
        return calc_info["go_sequence"]

    @property
    def stages(self):
        """
        Get stages information of this calculation
        The information is combined from the db and from calcspec
        """

        calc_info = self.cc.get_info()
        stages_info = calc_info["stages"].copy()
        db_stages = self._get_db_stages()

        # Adding db info over the stages_info obtained from calc spec
        for stage_name in stages_info:
            stages_info[stage_name]["name"] = stage_name
            if stage_name in db_stages:
                stages_info[stage_name]['status'] = db_stages[stage_name].status
                stages_info[stage_name]['time_submit'] = db_stages[stage_name].time_submit
                stages_info[stage_name]['time_begin'] = db_stages[stage_name].time_begin
                stages_info[stage_name]['time_end'] = db_stages[stage_name].time_end
            else:
                stages_info[stage_name]['status'] = None
                stages_info[stage_name]['time_submit'] = None
                stages_info[stage_name]['time_begin'] = None
                stages_info[stage_name]['time_end'] = None

        # Generating a list of stages dicts in the order of the go_sequence + other
        ordered_stages_info = []
        go_sequence = calc_info['go_sequence']

        # Firs adding the stages according to the go sequence stages
        for stage_name in (go_sequence[0] + go_sequence[1]):
            ordered_stages_info.append(stages_info[stage_name])

        # Adding the rest of the stages
        for stage_name in stages_info:
            if stage_name in (go_sequence[0] + go_sequence[1]):
                continue
            ordered_stages_info.append(stages_info[stage_name])

        return ordered_stages_info

    @classmethod
    def from_exisiting(cls, calc_id):
        """
        Return a calculation object for an existing calculation.
        """
        env = Environment()
        calc_record = env.db.get_calc(calc_id)
        return cls(env, calc_record)

    @classmethod
    def new(cls, title="", description="", parent_id=None, hpath=None):
        """
        Create a new calculation and return a Calculation object.

        """
        env = Environment()
        calc_record = CalcRecord(title=title,
                                 description=description,
                                 parent_id=parent_id,
                                 uid=env.uid,
                                 path="",
                                 time_create=now())
        calc_record = env.db.insert_calc(calc_record, hpath)
        calc_record.path = env.fs.new_calc_dir(calc_record.id)
        env.init_calcdir(calc_record.path)
        env.db.update_calc(calc_record)  # to save the path in the db
        return cls(env, calc_record)

    @classmethod
    def all_calculations(cls, user=None):
        """
        Return a generator of Calculation objects.
        """
        env = Environment()
        for calc_record in Environment().db.all_calculations(user):
            try:
                yield cls(env, calc_record)
            except PermissionError:
                pass

    @classmethod
    def locked_calculations(cls):
        """
        Return a generator of locked calculation objects
        """
        env = Environment()
        for calc_record in Environment().db.locked_calculations():
            try:
                yield cls(env, calc_record)
            except PermissionError:
                pass

    def __init__(self, env, calc_record):
        """
        Initialize a new Calculation.

        This is only called by the new and from_existing class methods, and
        never directly.

        """
        ps = PermissionSystem(env)
        ps.require_permission('READ', calc_record)
        self._write_permission = ps.check_permission('WRITE', calc_record)
        self.env = env
        self._calc_record = calc_record
        self.log = env.log  # TODO maybe a new logger?

    def _require_write_permission(self):
        if not self._write_permission:
            raise PermissionError('WRITE', self.id)

    def _save_to_db(self):
        self._require_write_permission()
        self.env.db.update_calc(self._calc_record)

    def _get_from_db(self):
        self._calc_record = self.env.db.get_calc(self.id)

    @lazy
    def cc(self):
        """
        cc is a lazy property to enable the creation of a new calculation without a calcspec.
        """
        return CalcSpecWrapper(self.env, self.path)

    def clone(self, hpath=None):
        """
        Clone calculation and returned the new child.

        TODO: Should cc.clone be a stage? be called in a subprocess?

        """

        new_calc = Calculation.new(title=self.title,
                                   description=self.description,
                                   parent_id=self.id,
                                   hpath=hpath,
                                   )

        # Generating the internal dir and copying the calc.py to the scompy dir
        internal_dir = os.path.join(new_calc.path, CALC_INTERNAL_DIR_NAME)
        if not os.path.isdir(internal_dir):
            os.mkdir(internal_dir)
        dest_calcspec_file = os.path.join(internal_dir, CALCSPEC_MODULE_NAME + ".py")
        if not os.path.isfile(dest_calcspec_file):
            src_calcspec_file = os.path.join(self.path, CALC_INTERNAL_DIR_NAME, CALCSPEC_MODULE_NAME + ".py")
            shutil.copyfile(src_calcspec_file, dest_calcspec_file)

        # Running the clone hook
        new_calc.cc.clone(self.path)
        #self.cc.clone(new_calc.path)

        # Adding the internal directory of scompy with the calcspec module

        self.env.log_event(events.Clone(self, new_calc))
        return new_calc

    def _lock(self):
        """
        Try to lock a calculation, and return success status.

        This method returns instantly, i.e. does not wait for a lock, and
        returns True if the lock was successfully obtained, and False if not.

        """
        # Trying to aquire the lock several times
        for i in range(1, 30):
            res = self.env.db.lock(self.id)
            if res:
                return True
            self.log.debug("Failed to obtain lock id={0} count={1}".format(self.id, i))
            time.sleep(0.5)
        return self.env.db.lock(self.id)

    def _require_lock(self):
        if not self._lock():
            raise LockError("Couldn't get lock for calculation {0}.".format(self.id))

    def _unlock(self):
        """
        Unlock the calculation and return instantly.

        This does nothing if it isn't locked.

        """
        self.env.db.unlock(self.id)

    def go(self, **extra_rm_info):
        """
        Run the calculation stages provided by the calcspec in the go_sequence property

        First the blocking stages are performed. If succesful, then the non blocking stages
        are submitted via the RM.
        Returns True if blocking stages performed correctly and submitting the non blocking
        stages was ok. Else and exception is thrown.
        """
        self._require_write_permission()

        go_sequence = self.cc.get_info()['go_sequence']
        self.log.debug("Calculation go_sequence = " + str(go_sequence))

        # Running the blocking stages
        for stage in go_sequence[0]:
            self.log.debug("Performing go on [{0}]".format(stage))
            self.do_stage(stage, None)

        # Submitting non blocking stages to rm
        if len(go_sequence[1]) > 0:
            self.log.debug("Submitting non blocking stages")
            self.submit_stages(go_sequence[1])

        return True

    def _get_db_stages(self):
        return self.env.db.get_stages(self.id)

    def _set_db_stage(self, stage):
        self.env.db.set_stage(stage)

    def _delete_db_stage(self, stage):
        return self.env.db.delete_stage(stage)

    def get_stage(self, stage_name):
        db_stages = self._get_db_stages()
        if stage_name in db_stages:
            return db_stages[stage_name]
        return None

    def submit_stages(self, stage_arg, **extra_rm_info):
        """
        Submit the calculation to run using the resource manager.

        extra_rm_info will be passed to ICalc.get_extra_rm_info and then to IResourceManager.submit.

        TODO high level events

        """

        # Taking care of the stages to submit (a string or a list)
        if isinstance(stage_arg, list):
            stage_list = stage_arg
        else:
            stage_list = [stage_arg]

        self._require_write_permission()
        self._require_lock()
        try:
            # Making sure the stages to run are not defined in the db
            db_stages = self._get_db_stages()
            for stage in stage_list:
                if stage in db_stages:
                    raise CalculationStageError(
                        "Cannot submit stage with existing DB record. calc_id={1}, stage_name={2!r}".format(
                        self.id, stage))

            # Getting the maximal np from all stages to submit
            calc_info = self.cc.get_info()
            np = max(calc_info['stages'][s]['np'] for s in stage_list)
            self.log.debug("Maximal np = {0}".format(np))
            extra_rm_info = self.cc.get_extra_rm_info(extra_rm_info)
            cmd = self.env.get_do_stages_cmd(self.id, stage_list)
            self.log.debug("Extra args: {0}".format(str(extra_rm_info)))
            rm_job = self.env.rm.submit(cmd=cmd,
                                        np=np,
                                        work_dir=self.path,
                                        output_filename=os.path.join(self.path, self.OUTPUT_FILENAME), # TODO
                                        **extra_rm_info)
            for stage in stage_list:
                stage_rec = StageRecord(self.id, stage)
                stage_rec.status = StageStatus.SUBMITTED
                stage_rec.time_submit = now()
                stage_rec.rm_job = rm_job.to_string()
                self._set_db_stage(stage_rec)
                self.log.debug(
                    "Calculation.submit_stage: After setting status to submitted. calc_id={0}, stage_name={1!r}".format(
                    self.id, stage))
        finally:
            self._unlock()

    def do_stage(self, stage_name, resource_allocation):
        """
        Run the requested stage.

        This is called either from inside a resource allocation, in which case
        resource_allocation is an object implementing IResourceAllocation, or
        locally, in which case resource_allocation is None.

        TODO high level events

        """
        self._require_write_permission()
        self._require_lock()
        try:
            stages = self._get_db_stages()
            if stage_name in stages:
                stage = stages[stage_name]
                if stage.status is not StageStatus.SUBMITTED:
                    raise CalculationStageError(
                        "Tried to execute a stage with status={0}, not SUBMITTED. calc_id={1}, stage_name={2!r}".format(
                            stage.status, self.id, stage_name))
            else:
                stage = StageRecord(self.id, stage_name)

            stage.status = StageStatus.RUNNING
            stage.time_begin = now()
            self._set_db_stage(stage)
            self.log.debug("Calculation.do_stage: After setting status to Running {0!r}.".format(stage_name))
        finally:
            self._unlock()

        self.log.info("Calculation.do_stage: Executing stage {0!r}.".format(stage_name))
        res = False
        try:
            in_resource_allocation = True if resource_allocation else False
            res = self.cc.do_stage(stage_name, in_resource_allocation)
        except:
            self.log.error(
                "Exception raised during stage execution. calc_id={0}, stage_name={1!r}, traceback:\n{2}".format(
                    self.id, stage_name, traceback.format_exc()))
            raise
        finally:
            self.log.info("Calculation.do_stage: calc_id={0}, stage_name={1!r}, stage finished with res={1}".format(
                self.id, stage_name, res))
            self._require_lock()
            try:
                stage.status = StageStatus.DONE if res else StageStatus.FAILED
                stage.time_end = now()
                self._set_db_stage(stage)
                self.log.debug("Calculation.do_stage: After setting status {0!r}.".format(stage))
            finally:
                self._unlock()
        return res

    def do_stages(self, stage_list, resource_allocation):
        """
        Do the given stages one by one, and return success status.

        Stops if any of the stages fails.

        """
        self.log.debug("Doing stages: {0}".format(repr(stage_list)))
        for stage in stage_list:
            res = self.do_stage(stage, resource_allocation)
            if not res:
                self.log.error("Error: do_stage for {0} returned False".format(stage))
                return False
        return True

    def reset_stage(self, stage_name):
        """
        Reset the stage enabling it to "go" again

        This method cal the calcpsec stage reset method to determine if the
        status of the calculation should be changed.
        """

        self._require_write_permission()
        self._require_lock()
        res = False
        try:
            # Getting the stage and checking if is capable of beeing reset
            stages = self._get_db_stages()
            if stage_name in stages:
                stage = stages[stage_name]
                if stage.status is not StageStatus.DONE and stage.status is not StageStatus.FAILED:
                    raise CalculationResetError(
                        "Tried to  reset a stage with status={0}, calc_id={1}, stage_name={2!r}".format(
                            stage.status, self.id, stage_name))
            else:
                self.log.info("Stage {0} for calc {1} was never done silent reset is done".format(
                    stage_name, self.id))
                return

            # Calling the reset on the self.cc
            # TODO : check if the lock should be held during the cc.reset_stage operation
            res = self.cc.reset_stage(stage_name, stage.status)
            if res is True:
                self.log.info("Reset result from calcspec {0}".format(res))
                self._delete_db_stage(stage)
            else:
                self.log.error("Error resetting stage: {0} calc_id {1}".format(
                    stage_name, self.id))
                raise CalculationResetError("Error in reset of calcpsec stage {0} id={1}".format(
                    stage_name, self.id))
        finally:
            self._unlock()

    def reset(self):
        """
        Reset all the stages in the go sequence of the calculation.
        The order is from last stage to first
        """

        go_sequence = self.cc.get_info()['go_sequence']
        self.log.debug("Resetting: go_sequence = " + str(go_sequence))

        # Running the blocking stages
        for stage_name in (list(reversed(go_sequence[1])) + list(reversed(go_sequence[0]))):
            self.log.debug("Resetting [{0}]".format(stage_name))
            self.reset_stage(stage_name)

        # TODO FIX EVENTS self.env.log_event(events.Reset(self))

    def get_status_line(self):
        return self.cc.get_status_line()


def _in_path(path, f):
    from functools import wraps

    @wraps(f)
    def wrapper(*args, **kwds):
        cwd = os.getcwd()
        os.chdir(path)
        try:
            return f(*args, **kwds)
        finally:
            os.chdir(cwd)
    return wrapper


class CalcWrapper(object):
    """
    A wrapper around an implementation of ICalc.

    This takes care of changing directories.

    """
    def __init__(self, path, impl):
        # Add wrapped methods for all ICalc methods
        self._impl = impl
        for name in ICalc.__dict__.iterkeys():
            if callable(getattr(ICalc, name)):
                setattr(self, name, _in_path(path, getattr(impl, name)))


class CalcSpecWrapper(object):
    """
    A wrapper around calcspec

    passes thru a subprocess

    Files tree:

    calculation_path
    |-- scompy
    |   |-- calc.py # the calcspec
    |   |-- stage_{stage_name}.{out|err}
    |   |-- calc.{out|err} # output & error for all other functions

    """

    def __init__(self, env, calc_path):
        self.env = env
        self.calc_path = calc_path

    def get_extra_rm_info(self, extra_rm_info):
        return calcspec_driver.call_calcspec(
            self.env.path, self.calc_path,
            "calc",
            "get_extra_rm_info",
            (extra_rm_info, ),
        )

    def clone(self, target_dir):
        return calcspec_driver.call_calcspec(
            self.env.path, self.calc_path,
            "calc",
            "clone",
            (target_dir, ),
        )

    def reset(self):
        return calcspec_driver.call_calcspec(
            self.env.path, self.calc_path,
            "calc",
            "reset",
            (),
        )

    def get_info(self):
        return calcspec_driver.call_calcspec(
            self.env.path, self.calc_path,
            "calc",
            "get_info",
            (),
        )

    def do_stage(self, stage_name, in_resource_allocation):
        return calcspec_driver.call_calcspec(
            self.env.path, self.calc_path,
            "stage_" + stage_name,
            "do_stage",
            (stage_name, in_resource_allocation),
        )

    def reset_stage(self, stage_name, current_stage_status):
        return calcspec_driver.call_calcspec(
            self.env.path, self.calc_path,
            "stage_" + stage_name,
            "reset_stage",
            (stage_name, current_stage_status),
        )

    def get_status_line(self):
        return calcspec_driver.call_calcspec(
            self.env.path, self.calc_path,
            "calc",
            "get_status_line",
            (),
        )
