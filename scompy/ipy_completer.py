"""
"""

import posixpath
import sys
import traceback

from IPython.core.ipapi import get as ipget

from scompy.core import HierarchyError
from scompy.environment import Environment


def hierarchy_glob(text):
    try:
        env = Environment()
    except:
        return []
    assert text[-1] == '*'
    path, target = posixpath.split(text[:-1])
    try:
        items = [posixpath.join(path, name) for name in env.db.list_node_children(path) if name.startswith(target)]
    except HierarchyError:
        return []
    return items


def hierarchy_matches(self, text):
    """
    Look at IPython.core.completer.file_matches to understand.
    """
    old_glob = self.glob
    try:
        self.glob = hierarchy_glob
        matches = self.file_matches(text)
        return [m.rstrip('/') for m in matches]
    finally:
        self.glob = old_glob


def load_ipython_extension(ip=None):
    if ip is None:
        ip = ipget()
    ip.set_custom_completer(hierarchy_matches)
