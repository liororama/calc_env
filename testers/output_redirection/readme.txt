Story 35 - Output Redirection
=============================

The desire is that when the user invokes commands such as clone or pre_run from
the IPython console, their stdout & stderr will go to the user's console AND to
to the calculation's calc_out.txt. This will make calc_out.txt like an
incremental log of everything that happened to the calculation. It may contain
output from the cloning process, then output from some failed pre_run's, and
eventually output from a successful submit and run.

To achieve this, we need to redirect the current process's stderr & stdout to
something like the linux tee command. The emphasis is that we need to
temporarily redirect the *current* process's streams, as we don't run the
commands in a subprocess. This directory contains some python attempts to do
so.

Until this story is done (if ever), we decide that commands invoked from the
console send their stdout & stderr to the user's screen and not to the
calc_out.txt file, and commands run using the resource manager will have thier
stdout & stderr redirected to the file by the resource manager, and not by some
other python mechanism.

