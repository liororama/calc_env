
import os
import sys
import shutil
import subprocess

for p in sys.path:
    print p


import scompy.environment
from scompy.environment import Environment
from scompy.api import Calculation

env_dir = sys.argv[1]

print "Clearing db"
env = Environment(env_dir)
env.db.disconnect()
# Clearing the db an generating the tables
env.db_admin.init_db()
env.db.connect()

# Adding a single calculation
print "Generating a single calculation"
c = Calculation.new(title="c1", description="c1 desc", hpath="/c1")
shutil.copy2("testers/api/calc.py", os.path.join(c.path, scompy.environment.CALC_INTERNAL_DIR_NAME))

# Adding a wrf calulations
wrf_base_calc = "/opt/wrf/wrf_calc"
if os.path.exists(wrf_base_calc):
    print "Creating an example WRF calculation"
    c = Calculation.new(title="wrf", description="An example wrf calculation", hpath="/wrf_base")
    subprocess.call(["cp", "-a", os.path.join(wrf_base_calc, "wrf"), c.path])
    subprocess.call(["cp", "-a", os.path.join(wrf_base_calc, "wps"), c.path])
    shutil.copy2("apps/wrf/WRFCalcSpec.py", os.path.join(c.path, scompy.environment.CALCSPEC_MODULE_NAME + ".py"))
