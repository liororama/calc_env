
import sys
import os
from pprint import pformat
import subprocess
import time

from scompy.core import Stage


class Calc(object):
    """
    An ICalc implementation for testing purposes.

    """

    def get_extra_rm_info(self, extra_rm_info):
        """
        Process extra_rm_info and return the extra_rm_info that should be passed to IResourceManager.submit.
        """
        if 'job_name' in extra_rm_info:
            extra_rm_info['job_name'] += '2_3' # to get 'testing_1_2_3'
        else:
            extra_rm_info['job_name'] = 'test_calc'

        return extra_rm_info

    def clone(self, target_dir):
        """
        Clone the calculation directory to target_dir.
        """
        retcode = subprocess.call("cp -f * " + target_dir, shell=True)
        return True

    def get_go_sequence(self):
        return [["build"], ["run", "process"]]

    def get_status_line(self):
        return "blabla"

    class run(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            total = 30
            for i in range(total):
                time.sleep(1)
                print "Done {0}/{1}".format(i, total)
                sys.stdout.flush()
            return True

        def reset(self, status):
            return True

    class build(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            total = 5
            for i in range(total):
                time.sleep(1)
                print "Build {0}/{1}".format(i, total)
                sys.stdout.flush()
            return True

    class process(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            total = 3
            for i in range(total):
                time.sleep(1)
                print "Process {0}/{1}".format(i, total)
                sys.stdout.flush()
            return True



