# Setting up the calcenv environment.
# This is to be used with ipython
# ipython -i setup_api.py env_dir

import sys
import os

import scompy.environment
from scompy import environment
from scompy.environment import Environment
from scompy.api import Calculation
from scompy.api import Calculation as Calc

import scompy.api
from scompy.api import debug_on, debug_off
from scompy.api import from_existing
from scompy.api import my_calcs
from scompy.api import my_running_calcs
from scompy.api import watch

env_dir = sys.argv[1]
env = Environment(env_dir)
#api.debug_off()

calc_env_banner = """
===========================================
Abacus project

Scientific calculations made easy

===========================================

type ab_101 to print a few help lines

"""


class abacus_101(object):
    """
    A Pseuodo object that provides the calcenv_101 repr
    """
    def __repr__(self, ):

        return """
Usefull commands:
-------------------------------------
    c1 = from_existing(1)
    c2 = c1.clone()
    c2.cd()
    c2.edit()
    c2.submit()
    print c2

    """
ab_101 = abacus_101()

## The banner
## Nothing should be printed after this !!!
print calc_env_banner

# Allowing user defined code to run here
scompy.api.load_user_startup_code()

