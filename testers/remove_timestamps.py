"""
Remove timestamps from given files
"""

import re
import sys


def remove_timestamps(st):
    st = re.sub('\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d,\d\d\d', '', st)
    st = re.sub('\d\d:\d\d:\d\d', '', st)
    return st

if __name__ == '__main__':
    for fn in sys.argv[1:]:
        st = remove_timestamps(open(fn).read())
        open(fn, 'w').write(st)
