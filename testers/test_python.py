#!/usr/bin/env python
import sys
import os

for i in range(10):
    sys.stdout.write("output: Hello Output {0}\n".format(i))
    sys.stderr.write("error: Hello Error {0}\n".format(i))
sys.stdout.write("sys.argv:\n{0!r}\n".format(sys.argv))
sys.stdout.write("os.environ:\n{0!r}\n".format(os.environ))
