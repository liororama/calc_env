t_body = """\
class C(object):
    def f(self):
        return {0}
"""

t_pkg = "pkg_{0}"
t_name = t_pkg + ".m"
t_filename = t_name.replace('.', '/') + ".py"

N = 10000


def generate():
    import os
    for i in range(N):
        pkg = t_pkg.format(i)
        filename = t_filename.format(i)
        if not os.path.isdir(pkg):
            os.mkdir(pkg)
            open(os.path.join(pkg, '__init__.py'), 'w').write('')
        open(filename, 'w').write(t_body.format(i))


def use():
    print "Importing {0} modules".format(N)
    import imp
    modules = []
    for i in range(N):
        name = t_name.format(i)
        filename = t_filename.format(i)
        modules.append(imp.load_source(name, filename))
    print "Testing {0} modules".format(N)
    for i in range(N):
        assert modules[i].C().f() == i
    print "Done"
    return modules


def delete():
    import sys
    for i in range(N):
        name = t_name.format(i)
        del sys.modules[name]
