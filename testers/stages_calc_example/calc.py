from scompy.core import Stage


class CalcBase(object):

    def get_extra_rm_info(self, extra_rm_info):
        if 'job_name' not in extra_rm_info:
            extra_rm_info['job_name'] = "some job"
        return extra_rm_info

    def clone(self, target_dir):
        pass

    def reset(self, status):
        pass

    class build(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            subprocess.check_call(["make"])
            return True

    class verify_data_file(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            subprocess.check_call(["make"])
            return True

    class run(Stage):
        def get_np(self):
            return 10

        def go(self, resource_allocation):
            subprocess.check_call(["make", "run"])
            return True

        def reset(self, status):
            subprocess.check_call(["make", "rclean"])
            return True

    class post_process(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            subprocess.check_call(["python", "post.py"])
            return True

    def get_go_sequence(self):
        return [["verify_data_file", "build"], ["run", "post_process"]]

    def get_status_line(self):
        return "OK"


class Calc(CalcBase):
    pass
