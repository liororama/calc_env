import mysql_calcdb
from mysql_calcdb import SQLNodeRecord

root = SQLNodeRecord(name='', parent=None)
env.db._session.add(root)
env.db._session.commit()

root = env.db._session.query(mysql_calcdb.SQLNodeRecord).filter(SQLNodeRecord.name == '', SQLNodeRecord.parent_id == None).first()
print root.dump()

def generate_tree(levels, degree, root, env):
    from mysql_calcdb import SQLNodeRecord
    position = ['']
    node = root
    while node is not None:
        if len(node.children) == degree or len(position) > levels:
            node = node.parent
            position.pop()
            continue
        child = SQLNodeRecord(name=str(len(node.children)), parent=node)
        env.db._session.add(child)
        position.append(child.name)
        node = child
        #print position

def generate_narrow_tree(levels, degree, root, env):
    from mysql_calcdb import SQLNodeRecord
    node = root
    for i in range(levels):
        for j in range(degree):
            child = SQLNodeRecord(name=str(len(node.children)), parent=node)
            env.db._session.add(child)
        node = node.children['0']

#env.db._session.commit()

"""

In <12>: 3**10
Out<12>: 59049

In <16>: %time generate_tree(10,3,root,env)
CPU times: user 23.09 s, sys: 0.34 s, total: 23.43 s
Wall time: 24.24 s

In <17>: %time env.db._session.commit()
CPU times: user 103.79 s, sys: 2.21 s, total: 105.99 s
Wall time: 122.66 s

# memory consumption is ~400MB

In <18>: %time env.db._get_node_from_path('/0/1/0/1/0/1/0/1/0/1')
CPU times: user 0.06 s, sys: 0.01 s, total: 0.07 s
Wall time: 0.07 s
Out<18>: SQLNodeRecord(name='1', node_id=36906L, parent_id=12302L)

# after restart:

In <3>: %time env.db._get_node_from_path('/0/1/0/1/0/1/0/1/0/1')
CPU times: user 0.04 s, sys: 0.00 s, total: 0.04 s
Wall time: 0.05 s
Out<3>: SQLNodeRecord(name='1', node_id=36906L, parent_id=12302L)

# so it looks like _get_node_from_path is pretty fast

# what about narrow tree?

In <11>: %time generate_narrow_tree(50,50,root,env)
CPU times: user 0.48 s, sys: 0.00 s, total: 0.48 s
Wall time: 0.49 s

In <12>: %time env.db._session.commit()
CPU times: user 1.36 s, sys: 0.05 s, total: 1.40 s
Wall time: 1.70 s

# restart interpretter

In <1>: %time len(env.db._get_node_from_path('/'.join([''] + ['0']*49)).children)
CPU times: user 0.20 s, sys: 0.00 s, total: 0.20 s
Wall time: 0.20 s
Out<1>: 50

# and it looks like there is some caching by sqlalchemy, but it was gone when I
# inserted self._session_reopen() into _get_node_from_path

"""


################
################

from mysql_calcdb import *
cr = CalcRecord(uid=1)
env.db.new_node('/a/b/c', True)
env.db.insert_calc(cr, 'a/b/c')
env.db.get_calc_from_node('/a/b/c')
env.db.get_node_from_calc(cr)

################
################

env.db.new_node('/a/b/c', True)
Calculation.new('c1', 'c 1', '/a/b')
api.from_hpath('/a/b')
type(api.from_hpath('/a/b')) is api.Calculation
tests.utils.put_calcspec(api.from_hpath('/a/b').path)
api.from_hpath('/a/b').clone('/a/b/c')
api.from_hpath('/a/b/c')

###########
###########

from scompy.api import H
import scompy.ipy_completer
scompy.ipy_completer.load_ipython_extension()
env = scompy.environment.Environment('/home/odedp/scompy_env')

###########
###########

import scompy.ipy_completer
from scompy.core import HierarchyError
import posixpath
env = scompy.environment.Environment('/home/odedp/scompy_env')
ip = scompy.ipy_completer.ipget()

def f(*args, **kwargs):
    global g
    return g(*args, **kwargs)
ip.set_custom_completer(f)

def g(self, text):
    return []

def g(*args, **kwargs):
    print args, kwargs
    return []

def my_glob(text):
    assert text[-1] == '*'
    path, target = posixpath.split(text[:-1])
    try:
        items = [posixpath.join(path, name) for name in env.db.list_node_children(path) if name.startswith(target)]
    except HierarchyError:
        return []
    return items
    #return ['X' + m for m in matches if m.startswith(text[:-1])]

def g(self, text):
    """
    Uses IPython.core.completer and slightly modified. (just replaced the
    glob function).
    """
    old_glob = self.glob
    try:
        self.glob = my_glob
        matches = self.file_matches(text)
        return [m.rstrip('/') for m in matches]
    finally:
        self.glob = old_glob
