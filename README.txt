# Calculation Environment Project 

Another line added by odedp.


Required Packages:
-------------------
The following packages are needed (on ubuntu) prior to using calcenv

* python-sqlalchemy
* python-nose
* mysql-server
* python-mysqldb
* slurm-llnl

Python packages:
* dateutil

Helpfull packages:
* mysql-workbench

Assumptions:
------------

* Id's over the system are the same
* Shared file system acrros the cluster

Multiple Users for Development:
-------------------------------
adduser ceuser1
adduser ceuser2

add the following lines to /etc/sudoers (via sudo visudo):
# for calc_env
odedp ALL=(ceuser1,ceuser2) NOPASSWD: ALL


MYSQL Note:
Make sure to change the bind option in the mysql config file my.cnf
to be the address of the server (instead of 127.0.0.1)

MYSQL commands:
------------------------------------------------------
mysql --user=root --password=mysql123
show databases;
create database scompy;
CREATE USER 'scompy'@'localhost' IDENTIFIED BY 'scompy123';
drop user 'scompy'@'localhost';
show grants for 'scompy'@'%';

GRANT ALL PRIVILEGES ON *.* TO 'scompy'@'%' WITH GRANT OPTION;

Git on centos:
 env GIT_SSL_NO_VERIFY=true  git clone https://liororama@bitbucket.org/liororama/calc_env.git

