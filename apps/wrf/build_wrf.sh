#!/bin/bash

PREFIX=/opt/wrf
ARCHIVE_DIR=/home/lior/src/wrf/archives

SZIP_DIR=$ARCHIVE_DIR/szip-2.1
ZLIB_DIR=$ARCHIVE_DIR/zlib-1.2.6
JASPER_DIR=$ARCHIVE_DIR/jasper-1.900.1
LIBPNG_DIR=$ARCHIVE_DIR/libpng-1.5.8
HDF5_DIR=$ARCHIVE_DIR/hdf5-1.8.9
NETCDF_DIR=$ARCHIVE_DIR/netcdf-4.1.3
WRF_DIR=$ARCHIVE_DIR/WRFV3
WPS_DIR=$ARCHIVE_DIR/WPS

# Packages to install
# gfortran
# g++
# csh
# libcloog-p0
# m4
# apt-get install gfortran gfortran-4.6-multilib csh build-essential libcloog-ppl0 m4

# Downloaded all packages from: http://tadeucruz.com/WRF/src/"

stage_start() {
    echo "****************************"
    echo $1
    echo "****************************"
    sleep 0.5
}

install_zlib() {
    stage_start "Installing zlib"

    cd $ZLIB_DIR
    ./configure --shared --prefix $PREFIX
    make   
    make check   
    make install 
}

install_jasper(){
    stage_start "Install JasPer"
    cd $JASPER_DIR
    ./configure --prefix $PREFIX --enable-shared  
    make  
    make install   
}

install_szip() {
    stage_start "Install szip"
    cd $SZIP_DIR
    ./configure --prefix=$PREFIX
    make
    make check
    make install
}

install_libpng() {
    stage_start "Install libpng"
    cd $LIBPNG_DIR
    env LDFLAGS=-L$PREFIX/lib CFLAGS=-I$PREFIX/include ./configure --prefix $PREFIX
    make check  
    make install     
}

install_prereq() {
    install_szip
    install_zlib
    install_jasper
    install_libpng
}

install_hdf5() {
    stage_start "Installing hdf5"
    cd $HDF5_DIR

    ./configure --prefix $PREFIX  --with-zlib=$PREFIX  --with-szip=$PREFIX --enable-fortran --enable-cxx   
    make -j2   
    make check   
    make install  
}


install_netcdf() {
    stage_start "Installing netcdf"
    cd $NETCDF_DIR
    LD_LIBRARY_PATH=$PREFIX/lib/:$LD_LIBRARY_PATH CPPFLAGS=-I$PREFIX/include LDFLAGS=-L$PREFIX/lib ./configure --with-hdf5=$PREFIX --prefix=$PREFIX --enable-shared  

    make  
    make check  
    make install  

    #echo "netcdf options"
    #./nc-config --all  
}


install_netcd2f() {
    stage_start "Installing netcdf"
    cd $NETCDF_C_DIR
    CPPFLAGS=-I$PREFIX/include LDFLAGS=-L$PREFIX/lib ./configure --with-hdf5=$PREFIX --prefix=$PREFIX --enable-shared  
    make  
    make check  
    make install  

    echo "netcdf options"
    ./nc-config --all  

    stage_start "Installing netcdf fortran"
    cd $NETCDF_F_DIR
    CPPFLAGS=-I$PREFIX/include LDFLAGS=-L$PREFIX/lib ./configure --with-hdf5=$PREFIX --prefix=$PREFIX --enable-shared  
    make  
    make check  
    make install  


}

install_wrf() {
    stage_start "Installing wrf"
    cd $WRF_DIR
    echo "********************************************************"
    echo "For wrf 3.3.1 you need to edit vi phys/module_cu_g3.F"
    echo "There is a line with integer, dimension (8) it should be "
    echo "Replaced with integer, dimension (12)"
    echo "********************************************************"
    sleep 5
    ./clean -a
    
    export NETCDF=$PREFIX 
    export JASPERLIB=$PREFIX/lib
    export JASPERINC=$PREFIX/include   
    export WRFIO_NCD_LARGE_FILE_SUPPORT=1    

    ./configure  

    stage_start "Compiling wrf - may take a while"
    ./compile -j 4 em_real  2>&1 | tee compile.out

}

install_wps(){
    stage_start "Installing wps"
    cd $WPS_DIR

    export NETCDF=$PREFIX 
    export JASPERLIB=$PREFIX/lib
    export JASPERINC=$PREFIX/include   
    export WRFIO_NCD_LARGE_FILE_SUPPORT=1    
    
    ./configure
    vim configure.wps
    ./compile 2>&1 | tee compile_wps.log
# Do same exports as before. 
# Run configure and select the g95 option (since there is no gfortran option) 
# Edit the configure.wps and replace '''g95''' with '''gfortran''' 
# Compile using ./compile >& compile_wps.log 
# If compilation is successful, 3 binaries will be created: geogrid.exe, ungrib.exe, metgrid.exe    


}
### MAIN ###@
#install_prereq
#install_hdf5
#install_netcdf
#install_wrf
#install_wps
#install_zlib
#install_jasper
#install_libpng