"""
CalcSpec for WRF
"""

import sys
import os
from pprint import pformat
import subprocess


forecast_install_dir="/opt/wrf"


class Calc(object):
    """
    An ICalc implementation for testing purposes.

    """

    reset_patterns = ["wps/FILE:*",
                      "wps/geo_em.d*",
                      "wps/met_em.d*",
                      "wps/GRIBFILE.*",
                      "wrf/met_em.d*"]
    clone_pattern = ["wps/geogrid",
                     "wps/geogrid.exe",
                     "wps/link_grib.csh",
                     "wps/metgrid",
                     "wps/metgrid.exe",
                     "wps/namelist.wps",
                     "wps/ungrib",
                     "wps/ungrib.exe",
                     "wps/util",
                     "wps/Vtable",
                     "wrf/wrf.exe",
                     "wrf/real.exe",
                     "wrf/*.TBL",
                     "wrf/*.tbl",
                     "wrf/namelist*",
                     "wrf/*DATA",
                     "wrf/*DBL",
                     "wrf/*txt",
                     "wrf/ozone*",
                     "calc.py"
                     ]

    def get_np(self):
        """
        Return the number processes needed to run.
        """

        return 1

    def get_extra_rm_info(self, extra_rm_info):
        """
        Process extra_rm_info and return the extra_rm_info that should be passed to IResourceManager.submit.
        """

        return extra_rm_info

    def pre_run(self):
        """
        Perform local actions before submission, and return success status.
        """

        return True

    def print_stage_message(self, name):
        print "\n\n\n"
        print "===================================="
        print "Running: {0}".format(name)
        print "===================================="
        sys.stdout.flush()

    def run_stage(self, name, command):
        self.print_stage_message(name)
        if isinstance(command, list):
            status = subprocess.call(command, env=self.wrf_env)
        else:
            status = subprocess.call(command, shell=True, env=self.wrf_env)

        print "\n\n"
        print "{0} exited with: {1}".format(name, status)
        sys.stdout.flush()
        if status != 0:
            return False
        return True

    def run(self, resource_allocation):
        """
        Run the calculation, assuming we are in the given resource allocation.
        """
        self.wrf_env = os.environ.copy()
        self.wrf_env["NCARG_ROOT"] = forecast_install_dir
        self.wrf_env["LD_LIBRARY_PATH"] = os.path.join(forecast_install_dir, "lib")
        self.wrf_env["PATH"] = os.path.join(forecast_install_dir, "bin") + ":" + os.environ["PATH"]

        print "Chaning dir to wps"
        os.chdir("wps")

        if not self.run_stage("geogrid", ["./geogrid.exe"]):
            return False
        data_path = os.path.join(forecast_install_dir, "data", "test", "fnl*")
        if not self.run_stage("link_grib.csh", "./link_grib.csh  " + data_path):
            return False
        if not self.run_stage("ungrib", ["./ungrib.exe"]):
            return False
        if not self.run_stage("metgrid", ["./metgrid.exe"]):
            return False

        print "Changing dir to ../wrf"
        os.chdir("../wrf")

        if not self.run_stage("linking em files", "ln -s ../wps/met_em* ."):
            return False
        if not self.run_stage("real.exe", ["./real.exe"]):
            return False

        return True

    def clone(self, target_dir):
        """
        Clone the calculation directory to target_dir.
        """
        tar_cmd = "tar cvf - {0} | tar xvf - -C {1} ".format(" ".join(self.clone_pattern), target_dir)
        print "Tar cmd:", tar_cmd
        status = subprocess.check_call(tar_cmd, shell=True)
        if status != 0:
            return False
        return True

    def reset(self, status):
        print "$$$$$$$ Resseting wrf calculation"
        subprocess.check_call("rm -f calc_out.txt", shell=True)
        subprocess.check_call("rm -f " + " ".join(self.reset_patterns), shell=True)

        return True

    def cancel(self, ):
        return None
