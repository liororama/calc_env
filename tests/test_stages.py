#!/usr/bin/python

"""
"""

import os
import sys
import time
import platform
import subprocess
from textwrap import dedent
from pwd import getpwnam
from stat import S_IRUSR, S_IWUSR, S_IXUSR

from nose.tools import *
import nose.tools

from utils import put_calcspec

from scompy.structures import CalcRecord, StageRecord, StageStatus
from scompy.environment import Environment
from scompy.environment import CALC_INTERNAL_DIR_NAME, CALCSPEC_MODULE_NAME

from scompy.core import now
from scompy.calculation import Calculation, CalcWrapper
from scompy.calculation import CalculationStageError, CalculationResetError
from scompy.core import CalcSpecError
from scompy.permissions import PermissionError
from tests.utils import CalcEnvTest, MultiUserCalcEnvTest

d1 = dict(
    title="title1",
    description="description 1",
    parent_id=None,
)
d2 = dict(
    title="title2",
    description="description 2",
    parent_id=None,
)


def dict_repr(d):
    return dict((k, dict_repr(v)) for k, v in d.iteritems()) if isinstance(d, dict) else repr(d)


class TestCalcs(CalcEnvTest):

    def teardown(self):
        self.env.log.debug("teardown: Entering")
        # 'unimport' calculation modules
        to_unload = [sys.modules[mod].Calc() for mod in sys.modules.keys() if hasattr(sys.modules[mod], 'Calc')]
        while len(to_unload) > 0:
            self.env.unload_calcspec(to_unload.pop())
        self.env.log.debug("teardown: Leaving")
        # unimport all packages too, even those without a Calc class (like failed imports)
        packages_to_unload = set(mod.split('.')[0] for mod in sys.modules.keys() if mod.startswith('calc_'))
        for pkg in packages_to_unload:
            self.env._unload_pkg(pkg)
        super(TestCalcs, self).teardown()

    #@nottest
    def test_new(self):
        t0 = now()
        c = Calculation.new(**d1)
        t1 = now()

        assert c.id is not None
        assert_equal(c.title, d1['title'])
        assert_equal(c.description, d1['description'])
        assert_equal(c.parent_id, d1['parent_id'])
        #assert c.status is CalcStatus.IN_PREPARATION
        assert_equal(c.uid, os.getuid())
        assert os.path.isdir(c.path)
        assert t0 <= c.time_create <= t1
        #assert c.times.submit is None
        #assert c.times.start_run is None
        #assert c.times.done is None
        #assert c._calc_record.rm_job is None

    #@nottest
    def test_from_existing(self):
        c1 = Calculation.new(**d1)
        c2 = Calculation.from_exisiting(eval(repr(c1.id), {}, {}))
        assert c1 is not c2
        assert_equal(c1._calc_record, c2._calc_record)

    #@nottest
    def test_all_calculations(self):
        titles = set("calculation #{0}".format(i) for i in range(10))
        d = d1.copy()
        calcs_1 = []
        for d['title'] in titles:
            calcs_1.append(Calculation.new(**d))

        set_1 = set(repr(c._calc_record) for c in calcs_1)
        set_2 = set(repr(c._calc_record) for c in Calculation.all_calculations())
        assert_equal(set_1, set_2)

    def check_after_calc_method(self, calc_path, tag, name, expected_locals):
        calc_name = os.path.basename(calc_path)
        fn = os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'for_test.{0}.txt'.format(name))
        assert os.path.isfile(fn)
        info = eval(open(fn).read(), {}, {})
        assert_equal(info['cwd'], repr(calc_path))
        assert_equal(info['globals']['TAG'], repr(tag))
        assert_equal(info['globals']['Calc'], "<class '{0}.calc.Calc'>".format(CALC_INTERNAL_DIR_NAME))
        assert info['globals']['__file__'] in (repr(os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'calc.py')),
                                               repr(os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'calc.pyc')))

        assert_equal(info['globals']['__name__'], repr('{0}.calc'.format(CALC_INTERNAL_DIR_NAME)))
        assert_equal(info['globals']['__package__'], repr(CALC_INTERNAL_DIR_NAME))
        for k, v in expected_locals.iteritems():
            assert k in info['locals'], "{0} is not in locals".format(k)
            assert_equal(info['locals'][k], dict_repr(v))
        return info

    def check_cc_call(self, name, args, kwargs, expected_locals, stream_output=False):
        """
        Create a new calculation and call it's cc.name(*args, **kwargs).

        Test the output written to for_test.name.txt for general stuff (like
        working directory). Also test that the local namespace of the function
        has expected_locals in it.

        Returns (c, tag, result):
        c - the calculation object
        tag - the tag from put_calcspec
        result - the result of the called function

        """
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        result = getattr(c.cc, name)(*args, **kwargs)
        calcspec_vars = self.check_after_calc_method(c.path, tag, name, expected_locals)
        # TODO check if there is another test that can be here
        #assert_equal(info['locals']['self'], repr(c.cc._impl))
        return c, tag, result, calcspec_vars

    #@nottest
    def test_cc_get_extra_rm_info(self):
        rm_info = dict(some_param=12.34, partition="the partition", job_name="jj")
        c, tag, result, calcspec_vars = self.check_cc_call('get_extra_rm_info', (rm_info, ), {}, dict(
            extra_rm_info=rm_info.copy()))

    #@nottest
    def test_cc_get_info(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        result = getattr(c.cc, "get_info")()
        print result

        # Checking stages
        assert('stages' in result)
        assert('stage1' in result['stages'])
        assert('stage2' in result['stages'])
        assert('stage3' in result['stages'])
        assert_equal(result['stages']['stage1']['np'], 1)
        assert_equal(result['stages']['stage2']['np'], 1)
        assert_equal(result['stages']['stage3']['np'], 1)

        # Checking the go_sequence lists
        assert(result["go_sequence"][0][0] == "stage1")
        assert(result["go_sequence"][1][0] == "stage2")
        assert(result["go_sequence"][1][1] == "stage3")

    def test_get_info_exhaustive(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)

        start_time = time.time()
        n = 2000
        limit = 100
        for i in range(n):
            result = c.cc.get_info()
            print "Tested ", i
            assert('stages' in result)
            assert('stage1' in result['stages'])
            assert(result["go_sequence"][0][0] == "stage1")
        end_time = time.time()
        total_time = end_time - start_time
        assert total_time < limit, "Time of running {0} get info is {1} > {2}".format(
            n, total_time, limit)

    def check_after_stage_go(self, stage_name, calc_path, tag, expected_locals):

        stage_out = os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'stage_' + stage_name + '.out')
        assert(os.path.isfile(stage_out))
        stage_err = os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'stage_' + stage_name + '.err')
        self.check_after_calc_method(calc_path, tag, stage_name + ".go", expected_locals)

        lines = open(stage_out).read().splitlines()
        assert "calc.py: {0}.go: Hello output!".format(stage_name) in lines
        lines = open(stage_err).read().splitlines()
        assert "calc.py: {0}.go: Hello error!".format(stage_name) in lines

    #@nottest
    def test_cc_do_stage(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        result = getattr(c.cc, "do_stage")('stage1', False)
        print result
        assert(result)
        self.check_after_stage_go('stage1', c.path, tag, {})

    #@nottest
    def test_cc_clone(self):
        source_dir = "just a string"
        self.check_cc_call('clone', (source_dir, ), {}, dict(
            source_dir=source_dir))

    #@nottest
    def test_bad_calcspec(self):
        c = Calculation.new(**d1)
        open(os.path.join(c.path, CALC_INTERNAL_DIR_NAME, 'calc.py'), 'w').write(dedent(
            """\
            bad python syntax
            """))
        assert_raises(CalcSpecError, c.cc.get_info)

    def _get_stages(self, calc):
        calc.stages

    #@nottest
    def test_bad_go_sequence(self):
        c = Calculation.new(**d1)
        put_calcspec(c.path, bad_go_sequence=True)

        assert_raises(CalcSpecError, self._get_stages, c)

    def wait_for_stage(self, calc_id, stage_name, stage_time, expect_success=True):
        """
        Wating for a stage to compleate running.
        """
        for i in range(1, stage_time):
            time.sleep(1)
            c = Calculation.from_exisiting(calc_id)
            stage_status = c.get_stage(stage_name).status

            print "{0} Status {1}:  {2}".format(i, stage_name, stage_status)
            # An optimization if the stage is done
            if stage_status is StageStatus.DONE or stage_status is StageStatus.FAILED:
                break
        # If status is not done after waiting for the job, exception is raised
        c = Calculation.from_exisiting(calc_id)
        if expect_success is True:
            assert c.get_stage(stage_name).status is StageStatus.DONE, "Error: stage {0} status is not done".format(stage_name)
        else:
            assert c.get_stage(stage_name).status is StageStatus.FAILED

    # Submit one stage via the submit_stages method
    #@nottest
    def test_submit_stage(self):
        hostname = platform.node()

        c = Calculation.new(**d1)
        tag = put_calcspec(c.path, from_rm=True)
        job_name_before = 'testing_1_'
        job_name_after = 'testing_1_2_3'  # calcspec's get_extra_rm_info adds the '2_3'
        c.submit_stages(['stage1'], job_name=job_name_before)

        self.check_after_calc_method(c.path, tag, 'get_extra_rm_info', dict(
                extra_rm_info=dict(job_name=job_name_before)))

        self.wait_for_stage(c.id, "stage1", 10)
        self.check_after_stage_go('stage1', c.path, tag, dict(
            allocated_hosts=(hostname, ),
            job_name=job_name_after))

    # Submit 2 stages via the submit_stages method
    #@nottest
    def test_submit_stage_2(self):
        hostname = platform.node()

        c = Calculation.new(**d1)
        tag = put_calcspec(c.path, from_rm=True)
        job_name_before = 'testing_1_'
        job_name_after = 'testing_1_2_3'  # calcspec's get_extra_rm_info adds the '2_3'
        c.submit_stages(['stage1', 'stage2'], job_name=job_name_before)

        self.check_after_calc_method(c.path, tag, 'get_extra_rm_info', dict(
                extra_rm_info=dict(job_name=job_name_before)))

        self.wait_for_stage(c.id, "stage1", 10)
        self.wait_for_stage(c.id, "stage2", 10)

        self.check_after_stage_go('stage1', c.path, tag, dict(
            allocated_hosts=(hostname, ),
            job_name=job_name_after))
        self.check_after_stage_go('stage2', c.path, tag, dict(
            allocated_hosts=(hostname, ),
            job_name=job_name_after))

    #@nottest
    def test_go(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        job_name_before = 'testing_1_'
        job_name_after = 'testing_1_2_3'  # calcspec's get_extra_rm_info adds the '2_3'
        c.go(job_name=job_name_before)

        self.wait_for_stage(c.id, "stage1", 2)
        self.wait_for_stage(c.id, "stage2", 10)
        self.wait_for_stage(c.id, "stage3", 10)

    #@nottest
    def test_do_bad_stage(self):
        hostname = platform.node()
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path, from_rm=False, stage1_result=False)

        res = c.do_stage("stage1", None)
        assert res is False

    #@nottest
    def test_submit_failed_stage(self):
        hostname = platform.node()
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path, from_rm=True, stage1_result=False)
        job_name_before = 'testing_1_'
        c.submit_stages(['stage1'], job_name=job_name_before)
        self.wait_for_stage(c.id, "stage1", 10, expect_success=False)

    #@nottest
    def test_reset_stage(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        c.do_stage('stage1', None)
        self.check_after_stage_go('stage1', c.path, tag, {})

        c.reset_stage("stage1")
        assert c.get_stage('stage1') is None

        c.do_stage('stage1', None)
        self.check_after_stage_go('stage1', c.path, tag, {})

    #@nottest
    def test_reset(self):

        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        job_name_before = 'testing_1_'
        job_name_after = 'testing_1_2_3'  # calcspec's get_extra_rm_info adds the '2_3'
        c.go(job_name=job_name_before)

        self.wait_for_stage(c.id, "stage1", 1)
        self.wait_for_stage(c.id, "stage2", 10)
        self.wait_for_stage(c.id, "stage3", 10)

        c.reset()
        c.go(job_name=job_name_before)
        self.wait_for_stage(c.id, "stage1", 1)
        self.wait_for_stage(c.id, "stage2", 10)
        self.wait_for_stage(c.id, "stage3", 10)

    #@nottest
    def test_bad_reset(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path, from_rm=False, stage1_reset_result=False)
        c.do_stage('stage1', None)
        self.check_after_stage_go('stage1', c.path, tag, {})
        assert_raises(CalculationResetError, c.reset_stage, "stage1")

    #@nottest
    def test_exception_reset(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path, from_rm=False, stage1_reset_exception=True)
        c.do_stage('stage1', None)
        self.check_after_stage_go('stage1', c.path, tag, {})
        assert_raises(CalcSpecError, c.reset_stage, "stage1")

    #@nottest
    def test_submit_no_calcspec(self):
        c = Calculation.new(**d1)
        assert_raises(CalcSpecError, c.go)

    def check_cloned_calc(self, parent, cloned, t0, t1, tag):
        assert cloned.id is not None
        assert cloned.id != parent.id
        assert_equal(cloned.parent_id, parent.id)

        assert_equal(cloned.title, parent.title)
        assert_equal(cloned.description, parent.description)
        #assert cloned.status is CalcStatus.IN_PREPARATION
        assert_equal(cloned.uid, os.getuid())  # TODO test cloning another user's calculation

        #assert t0 <= cloned.times.create <= t1
        #assert cloned.times.submit is None
        #assert cloned.times.start_run is None
        #assert cloned.times.done is None
        #assert cloned._calc_record.rm_job is None

        assert os.path.isdir(cloned.path)
        calcspec_path = os.path.join(cloned.path, CALC_INTERNAL_DIR_NAME, CALCSPEC_MODULE_NAME + ".py")
        assert os.path.isfile(calcspec_path), "File {0} does not exists".format(calcspec_path)

        #self.check_after_calc_method(parent.path, tag, 'clone', dict(
        #    target_dir=cloned.path))

        # TODO check where stdout and stderr of calcspec.clone go, after story 35 is done

    #@nottest
    def test_clone_new(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        t0 = now()
        c2 = c.clone()
        t1 = now()
        self.check_cloned_calc(c, c2, t0, t1, tag)

        c2.go()
        self.wait_for_stage(c2.id, "stage1", 1)
        self.wait_for_stage(c2.id, "stage2", 10)
        self.wait_for_stage(c2.id, "stage3", 10)

    #@nottest
    def test_clone_after_go(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        job_name_before = 'testing_1_'
        job_name_after = 'testing_1_2_3'  # calcspec's get_extra_rm_info adds the '2_3'
        c.go(job_name=job_name_before)

        self.wait_for_stage(c.id, "stage1", 1)
        self.wait_for_stage(c.id, "stage2", 10)
        self.wait_for_stage(c.id, "stage3", 10)

        # now clone c and check everything, this makes sure that status and times aren't cloned
        t0 = now()
        c2 = c.clone()
        t1 = now()
        self.check_cloned_calc(c, c2, t0, t1, tag)

        c2.go()
        self.wait_for_stage(c2.id, "stage1", 1)
        self.wait_for_stage(c2.id, "stage2", 10)
        self.wait_for_stage(c2.id, "stage3", 10)

    #@nottest
    def test_properties(self):
        c = Calculation.new(**d1)
        put_calcspec(c.path)

        # test mutable properties
        for name in ["title", "description"]:
            setattr(c, name, d2[name])
            assert_equal(getattr(c, name), d2[name])
            assert_equal(getattr(Calculation.from_exisiting(c.id), name), d2[name])

        # test immutable properties
        for name in ["id", "parent_id", "uid", "path"]:
            assert_raises(AttributeError, setattr, c, name, getattr(c, name))

        import pprint
        stages_info = c.stages
        pprint.pprint(stages_info)
        assert "stage1" == stages_info[0]["name"]
        assert "stage2" == stages_info[1]["name"]
        assert "stage3" == stages_info[2]["name"]

        # TODO test times

    #@nottest
    def test_permissions(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        calc_id = c.id
        path = c.path
        del c

        try:
            os.chmod(path, 0)
            assert_raises(PermissionError, Calculation.from_exisiting, calc_id)

            os.chmod(path, S_IRUSR | S_IXUSR)
            c = Calculation.from_exisiting(calc_id)
            assert_raises(PermissionError, setattr, c, "title", "new title")
            assert_raises(PermissionError, setattr, c, "description", "new description")
            assert_raises(PermissionError, c.go)
            assert_raises(PermissionError, c.reset)
        finally:
            os.chmod(path, S_IRUSR | S_IXUSR | S_IWUSR)

    #@nottest
    def test_get_status_line(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        assert "Status line with tag={0!r}".format(tag) == c.get_status_line()



class TestCalcsMultiUser(MultiUserCalcEnvTest):

    def teardown(self):
        self.env.log.debug("teardown: Entering")
        # 'unimport' calculation modules
        to_unload = [sys.modules[mod].Calc() for mod in sys.modules.keys() if hasattr(sys.modules[mod], 'Calc')]
        while len(to_unload) > 0:
            self.env.unload_calcspec(to_unload.pop())
        self.env.log.debug("teardown: Leaving")
        # unimport all packages too, even those without a Calc class (like failed imports)
        packages_to_unload = set(mod.split('.')[0] for mod in sys.modules.keys() if mod.startswith('calc_'))
        for pkg in packages_to_unload:
            self.env._unload_pkg(pkg)
        super(TestCalcsMultiUser, self).teardown()

    def run_as(self, user, code):
        """
        Run the given python code, as the given user.

        The code will be run in the user's home directory.

        Returns a tuple (returncode, stdout, stderr)
        """
        code_to_run = dedent("""\
            from tests.test_stages import *

            env = Environment({env_dir!r})
            """).format(env_dir=self.env_dir, d1=d1, d2=d2).strip() + '\n' + code
        print "code_to_run=", code_to_run

        p = subprocess.Popen(
            ['sudo', '-i', '-u', user, 'env', 'PYTHONPATH={0}'.format(os.getcwd()), 'python'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        x = p.communicate(code_to_run)
        return (p.returncode, x[0], x[1])

    def assert_runs_as(self, user, code):
        returncode, out, err = self.run_as(user, code)
        assert_equal(returncode, 0, err)

    def assert_raises_as(self, user, code, exception):
        if isinstance(exception, type):
            exception = exception.__name__
        returncode, out, err = self.run_as(user, code)
        assert_equal(returncode, 1)
        assert exception in err

    #@nottest
    def test_test(self):
        self.assert_runs_as('ceuser1', dedent(
            """\
            print Calculation
            """))
        self.assert_raises_as('ceuser1', dedent(
            """\
            print 1/0
            """), ZeroDivisionError)

    #@nottest
    def test_multiuser(self):
        # make sure we get a clean start
        for k in sys.modules.keys():
            assert not k.startswith('calc_'), k

        # create new calculations as ceuser1
        self.assert_runs_as('ceuser1', dedent(
            """\
            c1 = Calculation.new(**d1)
            assert c1.id == 1
            put_calcspec(c1.path)
            c2 = c1.clone()
            assert c2.id == 2
            os.chmod(c2.path, S_IRUSR | S_IXUSR | S_IWUSR) # no group permissions
            """))

        # access the first calculation from the current user (not ceuser1)
        c1 = Calculation.from_exisiting(1)
        assert_equal(c1.uid, getpwnam('ceuser1').pw_uid)

        # try to access the second calculation and get PermissionError
        assert_raises(PermissionError, Calculation.from_exisiting, 2)

        # clone the first calculation and see that it clones
        c3 = c1.clone()
        put_calcspec(c3.path)
        assert_equal(c3.id, 3)
        assert_equal(c3.uid, os.getuid())

        # access c3 from ceuser1 and clone it
        self.assert_runs_as('ceuser1', dedent(
            """\
            c3 = Calculation.from_exisiting(3)
            c4 = c3.clone()
            """))

        # make sure that ceuser1 can't submit c3
        self.assert_raises_as('ceuser1', dedent(
            """\
            c3 = Calculation.from_exisiting(3)
            c3.go()
            """), PermissionError)
