"""
A module with general utilities for calc_env tests.
"""

import time
import sys
import os
from os.path import join, dirname, realpath
from shutil import rmtree
import random

from scompy.environment import Environment
from scompy.environment import CALC_INTERNAL_DIR_NAME, CALCSPEC_MODULE_NAME


TEST_RUNS_DIR = realpath(join(dirname(__file__), "test_runs"))
CURRENT_TEST = "current_test_run"
ENV = "env"

current_test_id = None


def check_after_calc_method(calc_path, tag, name, expected_locals):
    calc_name = os.path.basename(calc_path)
    fn = os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'for_test.{0}.txt'.format(name))
    assert os.path.isfile(fn)
    info = eval(open(fn).read(), {}, {})
    assert_equal(info['cwd'], repr(calc_path))
    assert_equal(info['globals']['TAG'], repr(tag))
    assert_equal(info['globals']['Calc'], "<class '{0}.calc.Calc'>".format(CALC_INTERNAL_DIR_NAME))
    assert info['globals']['__file__'] in (repr(os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'calc.py')),
                                           repr(os.path.join(calc_path, CALC_INTERNAL_DIR_NAME, 'calc.pyc')))

    assert_equal(info['globals']['__name__'], repr('{0}.calc'.format(CALC_INTERNAL_DIR_NAME)))
    assert_equal(info['globals']['__package__'], repr(CALC_INTERNAL_DIR_NAME))
    for k, v in expected_locals.iteritems():
        assert k in info['locals'], "{0} is not in locals".format(k)
        assert_equal(info['locals'][k], dict_repr(v))
    return info


def put_calcspec(path,
                 from_rm=False,
                 tag=None,
                 stage1_result=True,
                 stage1_reset_result=True,
                 stage1_reset_exception=False,
                 makefile=True,
                 bad_go_sequence=False):
    """
    Generate a calcspec file for testing and write it in path

    The calcpsec is generated according to the arguments to this method. The tag is a
    variable containing a string that will appear as a global variable in the calcspec
    module. The tag used is returned.

    from_rm : Indicate whether the calcspec is going to be activated from within
              a resource allocation
    tag : If not tag is given, a new random integer is used.
    stage1_result : the result (True/False) to return from stage1 go method
    stage_1_reset_result : the result (True False) to return from stage1 reset
    stage_1_reset_exception : Whether to raise execption in stage1 reset or not
    """

    if tag is None:
        tag = str(random.randint(0, sys.maxint))

    calc_spec_str = r'''
import sys
import os
import subprocess
from pprint import pformat

from scompy.core import Stage


def dict_repr(d):
    return dict((k, dict_repr(v)) for k, v in d.iteritems()) if isinstance(d, dict) else repr(d)


def output_test_info(name, g, l, output_to_streams=False):

    if output_to_streams:
        sys.stdout.write("calc.py: {0}: Hello output!\n".format(name))
        sys.stderr.write("calc.py: {0}: Hello error!\n".format(name))

    info = dict(
        cwd=os.getcwd(),
        locals=l,
        globals=g,
        )
    open(os.path.join(os.path.dirname(__file__), "output.{0}.txt".format(name)), 'w').write(
        "output from {0}\n\n".format(name) +
        '\n'.join('\n'.join((
            k,
            '='*80,
            pformat(v),
            '',
            )) for k, v in info.iteritems()
            ))
    open(os.path.join(os.path.dirname(__file__), 'for_test.{0}.txt'.format(name)), 'w').write(
        repr(dict_repr(info)))


class Calc(object):
    """
    An ICalc implementation for testing purposes.

    """
    def _get_tag(self):
        return TAG

    def get_status_line(self):
        return "Status line with tag={0!r}".format(self._get_tag())

    def get_extra_rm_info(self, extra_rm_info):
        output_test_info('get_extra_rm_info', globals(), locals())
        if extra_rm_info:
            extra_rm_info['job_name'] += '2_3' # to get 'testing_1_2_3'
    '''

#    if not from_rm:
#        calc_spec_str += r'''
#        extra_rm_info['tag'] = TAG
#    '''
    calc_spec_str += r'''
        return extra_rm_info

    def clone(self, source_dir):
        try:
            output_test_info('clone', globals(), locals())
        except IOError: # in case we don't have permissions
            pass
        makefile_path = os.path.join(source_dir, "Makefile")
        retcode = subprocess.call(["cp", "-f", makefile_path, '.'])
        return True

    def reset(self, status):
        pass

    class stage1(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            if resource_allocation:
                allocated_hosts = tuple(resource_allocation.get_hosts())
                job_name = subprocess.Popen(
                    ['squeue', '--format=%.80j'],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE
                    ).communicate()[0].split()[1]

            output_test_info('stage1.go', globals(), locals(), output_to_streams=True)
            for i in range(1,10):
                print "Stage 1 - i = ", i
'''
    calc_spec_str += r'''
            return {0}
'''.format(repr(stage1_result))

    calc_spec_str += r'''
        def reset(self, status):
            print "In reset calcspec reset stage1"
            if {0}:
                raise ValueError("Exception in reset")
            return {1}
'''.format(stage1_reset_exception, stage1_reset_result)

    calc_spec_str += r'''

    class stage2(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            if resource_allocation:
                allocated_hosts = tuple(resource_allocation.get_hosts())
                job_name = subprocess.Popen(
                    ['squeue', '--format=%.80j'],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE
                    ).communicate()[0].split()[1]

            output_test_info('stage2.go', globals(), locals(), output_to_streams=True)
            for i in range(1,10):
                print "Stage 2 - i = ", i
            return True

    class stage3(Stage):
        def get_np(self):
            return 1

        def go(self, resource_allocation):
            if resource_allocation:
                allocated_hosts = tuple(resource_allocation.get_hosts())
                job_name = subprocess.Popen(
                    ['squeue', '--format=%.80j'],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE
                    ).communicate()[0].split()[1]

            output_test_info('stage3.go', globals(), locals(), output_to_streams=True)
            for i in range(1,10):
                print "Stage 3 - i = ", i
            return True
'''

    # Adding the get_go_sequence part
    calc_spec_str += r'''
    def get_go_sequence(self):
'''
    if bad_go_sequence is False:
        calc_spec_str += r'''
        return [["stage1"], ["stage2", "stage3"]]
'''
    else:
        calc_spec_str += r'''
        return [["stage999"], ["stage2", "stage3"]]
'''

    calc_spec_path = os.path.join(path, CALC_INTERNAL_DIR_NAME, 'calc.py')

    open(calc_spec_path, 'w').write("TAG = {0!r}\n\n".format(tag) + calc_spec_str)

    if makefile:
        open(os.path.join(path, 'Makefile'), 'w').write(r'''
# A sample file
.PHONY: clean
clean:
    \rm -f *.o
''')

    return tag


class CalcEnvTest(object):
    """
    A base class for tests that need a calc_env Environment.

    Adds setup and teardown and has self.env as an instance variable.

    """

    def setup(self):
        """
        Create a new clean Environment and set self.env
        """
        self.test_id = current_test_id
        # sys.stderr.write("\n{0} (setup)\n".format(self.test_id))

        # drop existing Environment
        Environment.reset_singleton()

        # create empty test run directory (self.test_dir) - this is a place to
        # put temporary files for the test
        self.test_dir = join(TEST_RUNS_DIR, CURRENT_TEST)
        if os.path.exists(self.test_dir):
            rmtree(self.test_dir)
            sys.stderr.write("\nWarning: CalcEnvTest.setup removed {0!r}\n".format(self.test_dir))
        os.makedirs(self.test_dir)

        # create empty env directory
        self.env_dir = join(self.test_dir, ENV)
        os.mkdir(self.env_dir)
        open(join(self.env_dir, "config.ini"), 'w').write(self.get_config())

        # create new Environment instance
        self.env = Environment(self.env_dir)

        # get a clean RM
        self.env.rm.cancel_all_jobs()

        # get a clean db
        self.env.db.disconnect()
        self.env.db_admin.init_db()
        self.env.db.connect()

    def teardown(self):
        """
        Nothing happens here as it all happens in the nose plugin.
        """
        # sys.stderr.write("{0} (teardown)\n".format(self.test_id))
        # self.env.db.disconnect()
        pass

    def get_config(self):
        """
        Return a string to be written to the config.ini file. Called by setup.

        This can be overriden in subclasses.
        """
        return open(join(dirname(__file__), "basic_config.ini")).read()


class MultiUserCalcEnvTest(CalcEnvTest):
    """
    A parent for tests with multiple users, that sets up the config.ini to put
    calculations in a separate directory for each test.

    """

    def get_config(self):
        """
        Return a string to be written to the config.ini file. Called by setup.

        This can be overriden in subclasses.
        """
        return open(join(dirname(__file__), "multiuser_config.ini")).read().format(test_id=self.test_id)
