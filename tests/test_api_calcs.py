#!/usr/bin/python

"""
Refs:
http://lucumr.pocoo.org/2011/2/1/exec-in-python/
http://www.python.org/dev/peps/pep-0302/
"""

import os
import sys
import time
import platform
import random

from nose.tools import *
import nose.tools

from utils import put_calcspec, check_after_calc_method

#from structures import CalcRecord, CalcTimes, CalcStatus
from scompy.structures import StageStatus

from scompy.environment import Environment
from scompy.api import Calculation, from_hpath
from scompy.core import now
from scompy.calculation import Calculation as InternalCalculation
from tests.utils import CalcEnvTest

d1 = dict(
    title="title1",
    description="description 1",
    hpath=None,
)
d2 = dict(
    title="title2",
    description="description 2",
    hpath=None,
)


def dict_repr(d):
    return dict((k, dict_repr(v)) for k, v in d.iteritems()) if isinstance(d, dict) else repr(d)


class TestAPICalcs(CalcEnvTest):

    def teardown(self):
        # 'unimport' calculation modules
        self.env.log.debug("teardown: Entering")
        to_unload = [sys.modules[mod].Calc() for mod in sys.modules.keys() if hasattr(sys.modules[mod], 'Calc')]
        while len(to_unload) > 0:
            self.env.unload_calcspec(to_unload.pop())
        self.env.log.debug("teardown: Leaving")
        super(TestAPICalcs, self).teardown()

    def test_new(self):
        t0 = now()
        c = Calculation.new(**d1)
        t1 = now()
        assert os.path.isdir(c.path)
#       assert c.status is CalcStatus.IN_PREPARATION
        assert c.id
        assert_equal(c.title, d1['title'])
        assert_equal(c.description, d1['description'])
        assert c.parent_id is None
        assert_equal(c.uid, os.getuid())

 #       assert t0 <= c.times.create <= t1
 #       assert c.times.submit is None
 #       assert c.times.start_run is None
 #       assert c.times.done is None

    def test_update(self):
        c = Calculation.new(**d1)
        c.title = "c_title_new"
        c.description = "c_new_description"
        c2 = Calculation.from_exisiting(c.id)
        assert_equal(c2.title, "c_title_new")
        assert_equal(c2.description, "c_new_description")
        assert_raises(AttributeError, setattr, c2, "parent_id", "bbbbbb")
        assert_raises(AttributeError, setattr, c2, "uid", 123)
        assert_raises(AttributeError, setattr, c2, "path", "bbbbbb")

        # Updating while 2 objects exist
        c.title = "c_title_new_2"
        assert_equal(c2.title, "c_title_new_2")

    def check_after_calc_method(self, calc_path, tag, name, expected_locals):
        calc_name = os.path.basename(calc_path)
        fn = os.path.join(calc_path, 'for_test.{0}.txt'.format(name))
        assert os.path.isfile(fn)
        info = eval(open(fn).read(), {}, {})
        assert_equal(info['cwd'], repr(calc_path))
        assert_equal(info['globals']['TAG'], repr(tag))
        assert_equal(info['globals']['Calc'], "<class '{0}.calc.Calc'>".format(calc_name))
        assert info['globals']['__file__'] in (repr(os.path.join(calc_path, 'calc.py')),
                                               repr(os.path.join(calc_path, 'calc.pyc')))

        assert_equal(info['globals']['__name__'], repr('{0}.calc'.format(calc_name)))
        assert_equal(info['globals']['__package__'], repr(calc_name))
        for k, v in expected_locals.iteritems():
            assert k in info['locals'], "{0} is not in locals".format(k)
            assert_equal(info['locals'][k], dict_repr(v))
        return info
    #@nottest

    def wait_for_stage(calc, stage_name, stage_time, expect_success=True):
        """
        Wating for a stage to compleate running using the api info.
        """
        for i in range(1, stage_time):
            time.sleep(1)
            stage_status = c.get_stage(stage_name).status

            print "{0} Status {1}:  {2}".format(i, stage_name, stage_status)
            # An optimization if the stage is done
            if stage_status is StageStatus.DONE or stage_status is StageStatus.FAILED:
                break
        # If status is not done after waiting for the job, exception is raised
        c = Calculation.from_exisiting(calc_id)
        if expect_success is True:
            assert c.get_stage(stage_name).status is StageStatus.DONE
        else:
            assert c.get_stage(stage_name).status is StageStatus.FAILED

    def test_go(self):

        c = Calculation.new(**d1)
        tag = put_calcspec(c.path)
        job_name_before = 'testing_1_'
        job_name_after = 'testing_1_2_3'  # calcspec's get_extra_rm_info adds the '2_3'
        c.go(job_name=job_name_before)

        #self.wait_for_stage(c.id, "stage1", 1)
        #self.wait_for_stage(c.id, "stage2", 10)
        #self.wait_for_stage(c.id, "stage3", 10)

    def test_clone(self):
        c = Calculation.new(**d1)
        tag = put_calcspec(c.path, makefile=True)
        c2 = c.clone(hpath=None)
        assert os.path.isfile(os.path.join(c2.path, "Makefile")), "No Makefile in {0}".format(c2.path)
        assert_equal(c2.parent_id, c.id, "Parent id of c2 is not id of c")

    def test_all_calculations(self):
        parent_calc = Calculation.new(**d1)
        tag = put_calcspec(parent_calc.path)

        calc_list = []
        for i in range(1, 10):
            calc_list.append(parent_calc.clone(hpath=None))
        l2 = list(Calculation.all_calculations())

        assert_equal(len(l2), 10, "Number of all calcs is not 10")

    def test_by_parent_id(self):
        parent_calc1 = Calculation.new(**d1)
        tag = put_calcspec(parent_calc1.path)

        parent_calc2 = Calculation.new(**d2)
        tag = put_calcspec(parent_calc2.path)

        calc_list = []
        for i in range(1, 11):
            calc_list.append(parent_calc1.clone(hpath=None))

        for i in range(1, 9):
            calc_list.append(parent_calc2.clone(hpath=None))

        l1 = list(Calculation.by_parent_id(parent_calc1.id))
        l2 = list(Calculation.by_parent_id(parent_calc2.id))

        assert_equal(len(l1), 10, "Number of calcs with parent1 is not 10")
        assert_equal(len(l2), 8, "Number of calcs with parent2 is not 10")
        assert_equal(l1[0].parent_id, parent_calc1.id, "list1 parent id is not parent1")
        assert_equal(l1[0].parent_id, parent_calc1.id, "list2 parent id is not parent2")

    def test_calcs_with_hpath(self):
        # self.env.db.new_node('/a/b/c', True)
        c = Calculation.new('c1', 'c 1', '/a/b')
        c2 = from_hpath('/a/b')
        assert type(c2) is Calculation
        assert c2.id == c.id
        put_calcspec(from_hpath('/a/b').path)
        c3 = from_hpath('/a/b').clone('/a/b/c')
        c4 = from_hpath('/a/b/c')
        assert c3.id == c4.id
