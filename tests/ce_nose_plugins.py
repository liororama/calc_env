import datetime
import time
import os
import sys
import shutil
from os.path import dirname, join, realpath
import logging

import nose
from nose.plugins import Plugin

from scompy.environment import Environment
from tests.utils import CalcEnvTest, TEST_RUNS_DIR, CURRENT_TEST
import tests.utils


class RenameTables(Plugin):
    """ Rename the mysql tables after each test """

    name = "rename-tables"

    def options(self, parser, env=os.environ):
        super(RenameTables, self).options(parser, env=env)

    def configure(self, options, conf):
        super(RenameTables, self).configure(options, conf)
        if not self.enabled:
            return
        self.util_env_path = realpath(join(dirname(__file__), 'util_env'))

    #def stopTest(self, test):
    #        #time.sleep(5)
    #        #print "bbbbb"

    def begin(self):
        env = Environment(self.util_env_path)
        env.set_log_level(logging.INFO)
        env.db_admin.clear_tests_table(test_prefix="_cet_")
        Environment.reset_singleton()

    def beforeTest(self, test):
        tests.utils.current_test_id = test.id()

    def afterTest(self, test):
        """
        """
        tests.utils.current_test_id = None

        # drop existing Environment
        Environment.reset_singleton()

        # rename test directory
        current_path = join(TEST_RUNS_DIR, CURRENT_TEST)
        target_path = join(TEST_RUNS_DIR, test.id())
        if os.path.exists(current_path):
            if os.path.exists(target_path):
                shutil.rmtree(target_path)
            os.rename(current_path, target_path)

        # rename DB tables
        env = Environment(self.util_env_path)
        prefix = test.id()
        prefix = prefix.replace(".", "_")
        prefix = prefix[-32:]  # don't get a table name too long
        env.db_admin.rename_tables(test_prefix="_cet_", prefix=prefix)
        Environment.reset_singleton()

    #def addError(self, test, err, capt=None):
    #    print "Test Name", test.id()
    #    time.sleep(4)
    #
    #def addFailure(self, test, err, capt=None, tb_info=None):
    #    print "Test Name", test.id()
    #    time.sleep(4)
    #
    #def addSuccess(self, test, capt=None):
    #    print "Test Name", test.id()
    #    time.sleep(4)

    #def finalize(self, result):
    #    #print "aaaaaaaa"
