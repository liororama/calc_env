#!/usr/bin/python

import os
from tempfile import mkdtemp
import time
import re

from nose.tools import *
import nose.tools

from scompy.structures import CalcRecord, RMJobStatus
from scompy.environment import Environment
from scompy.core import ResourceManagerError, NotInResourceAllocationError
from tests.utils import CalcEnvTest


class TestSlurmRM(CalcEnvTest):
    """
    Testing for the slurm_rm plugin
    """

    def test_parse_slurm_tasks_per_node(self):
        from slurm_rm import parse_slurm_tasks_per_node_str

        tpn = "32(x4)"
        gold = [32] * 4
        print "Testing {0!r}".format(tpn)
        l = parse_slurm_tasks_per_node_str(tpn)
        print "Got {0!r}".format(l)
        assert_equal(l, gold)

        tpn = "32(x4),55,12(x3)"
        gold = [32] * 4 + [55] + [12] * 3
        print "Testing {0!r}".format(tpn)
        l = parse_slurm_tasks_per_node_str(tpn)
        print "Got {0!r}".format(l)
        assert_equal(l, gold)

    def test_slurm_resource_allocation(self):
        from slurm_rm import SlurmResourceAllocation
        ra = SlurmResourceAllocation(self.env.rm, "vega[1-4]", "8(x2),4(x2)")
        gold = (['vega1'] * 8 +
                ['vega2'] * 8 +
                ['vega3'] * 4 +
                ['vega4'] * 4)
        hosts = list(ra.get_hosts())
        assert_equal(hosts, gold)

        ra = SlurmResourceAllocation(self.env.rm, "vega[1-2],yogurt[32-35]", "8(x2),5(x4)")
        gold = (['vega1'] * 8 +
                ['vega2'] * 8 +
                ['yogurt32'] * 5 +
                ['yogurt33'] * 5 +
                ['yogurt34'] * 5 +
                ['yogurt35'] * 5)
        hosts = list(ra.get_hosts())
        assert_equal(hosts, gold)

    def test_get_resource_allocation_bad(self):
        assert_raises(NotInResourceAllocationError, self.env.rm.get_current_allocation)

    def test_submit(self):
        script_name = 'submit_script.py'
        open(os.path.join(self.test_dir, script_name), 'w').write(
            r"""#!/usr/bin/env python
import sys
import os

for i in range(10):
    sys.stdout.write("output number: {0}\n".format(i))
    sys.stderr.write("error number: {0}\n".format(i))
sys.stdout.write("cwd: {0!r}\n".format(os.getcwd()))
sys.stdout.write("sys.argv: {0!r}\n".format(sys.argv))
sys.stdout.write("os.environ: {0!r}\n".format(os.environ))
""")
        args = [
            "this_is_easy",
            #"can you handle spaces",
            #"""can you "handle" 'quotes'""",
            #"can you handle *, ? and $",
            #"what \n about \n newlines",
            ]
        np = 1
        job_name = 'test_job_' + self.test_dir
        slurm_job = self.env.rm.submit(
            ['python', script_name] + args,
            np,
            self.test_dir,
            'out.txt',
            job_name=job_name,
            partition=None, # TODO
            account=None, # TODO
            constraint=None, # TODO
            )
        time.sleep(3)
        out_fn = os.path.join(self.test_dir, 'out.txt')
        assert os.path.isfile(out_fn)
        lines = open(out_fn).read().splitlines()

        # the first 20 lines should be numbers, but the order may be different
        # due to buffering
        next_number = {'output': 0, 'error': 0}
        for i in range(20):
            l = lines.pop(0)
            m = re.match("(output|error) number: (\d+)", l)
            assert m
            stream = m.group(1)
            number = int(m.group(2))
            assert_equal(next_number[stream], number, "Bad order for stream {0}".format(stream))
            next_number[stream] += 1

        # check working directory
        l = lines.pop(0)
        assert_equal(l, "cwd: {0!r}".format(self.test_dir))

        # check argv
        l = lines.pop(0)
        assert l.startswith("sys.argv: ")
        argv = eval(l[len("sys.argv: "):], {}, {})
        assert_equal(argv, [script_name] + args)

        # check slurm environment variables
        l = lines.pop(0)
        assert l.startswith("os.environ: ")
        environ = eval(l[len("os.environ: "):], {}, {})
        assert_equal(environ.get('SLURM_JOBID'), str(slurm_job.job_id))
        assert_equal(environ.get('SLURM_JOB_NAME'), job_name)
        assert_equal(environ.get('SLURM_JOB_NUM_NODES'), str(np))  # TODO maybe another variable?

        # make sure there are no more lines
        assert_equal(lines, [])

    def _submit_sleep_job(self, job_name, sleep_time=5):
        job_dir = os.path.realpath(mkdtemp(dir=self.test_dir))
        job = self.env.rm.submit(
            ['sleep', str(sleep_time)],
            1,
            job_dir,
            'out.txt',
            job_name=job_name,
            partition=None,  # TODO
            account=None,  # TODO
            constraint=None,  # TODO
            )
        return job

    def test_get_job_status(self):
        """
        Getting the status of a job
        """

        # Run sleep in a tmp dir
        job1 = self._submit_sleep_job("sleep_tester", 5)

        # Get status of sleep process, allowing slurm to submit the process
        time.sleep(1)
        assert job1.get_status() is RMJobStatus.RUNNING

        jobs = []
        for i in range(1,10):
            jobs.append(self._submit_sleep_job("sleep_tester", 60))

        assert jobs[-1].get_status() is RMJobStatus.PENDING

        self.env.rm.cancel_all_jobs()

    def test_job_kill(self):

        job1 = self._submit_sleep_job("sleep_tester1", 60)
        time.sleep(1)
        status = job1.get_status()
        assert(status is RMJobStatus.RUNNING or status is RMJobStatus.PENDING)

        job1.kill()
        time.sleep(1)
        assert_equal(job1.get_status(), RMJobStatus.NOT_FOUND)

        # Trying to kill already killed job
        # Since the plugin is loaded with a name which is not plugins.slurm_rm but slurm_rm
        # The exception raised from within the plugin is slurm_rm.SlurmRMError and becuase of
        # that we need to give this exception to assert_raises and this is way the following code
        # will faile
        # from plugins.slurm_rm import SlurmRMError
        # assert_raises(SlurmRMError, job1.kill)
        # import sys
        # assert_raises(sys.modules['slurm_rm'].SlurmRMError, job1.kill)
        from slurm_rm import SlurmRMError  # this import works only after Environment has been set up and 'slurm_rm' is in sys.modules
        assert_raises(SlurmRMError, job1.kill)
        # TODO: think more about the above, and change loader.py to something better

    def test_tofrom_string(self):

        job1 = self._submit_sleep_job("sleep_tester1", 5)
        job1_str = job1.to_string()
        job2 = self.env.rm.job_from_string(job1_str)
        time.sleep(1)
        status = job2.get_status()
        assert(status is RMJobStatus.RUNNING or status is RMJobStatus.PENDING)

        job2.kill()
        time.sleep(1)
        assert_equal(job1.get_status(), RMJobStatus.NOT_FOUND)
