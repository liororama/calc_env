#!/usr/bin/python


import os
import sys
import time
import platform
import random

from nose.tools import *
import nose.tools

from utils import put_calcspec, check_after_calc_method

#from structures import CalcRecord, CalcTimes, CalcStatus
from scompy.structures import StageStatus

import scompy.environment as environment
from scompy.environment import Environment
from scompy.api import Calculation
from scompy.core import now
from scompy.calculation import Calculation as InternalCalculation
from tests.utils import CalcEnvTest

d1 = dict(
    title="title1",
    description="description 1",
    hpath=None,
)
d2 = dict(
    title="title2",
    description="description 2",
    hpath=None,
)


def dict_repr(d):
    return dict((k, dict_repr(v)) for k, v in d.iteritems()) if isinstance(d, dict) else repr(d)


class TestDeploy(CalcEnvTest):

    def teardown(self):
        # 'unimport' calculation modules
        self.env.log.debug("teardown: Entering")
        to_unload = [sys.modules[mod].Calc() for mod in sys.modules.keys() if hasattr(sys.modules[mod], 'Calc')]
        while len(to_unload) > 0:
            self.env.unload_calcspec(to_unload.pop())
        self.env.log.debug("teardown: Leaving")
        super(TestDeploy, self).teardown()

    def wait_for_stage(self, calc_id, stage_name, stage_time, expect_success=True):
        """
        Wating for a stage to compleate running using the api info.
        """
        for i in range(1, stage_time):
            time.sleep(1)
            c = Calculation.from_exisiting(calc_id)
            stage_status = c.get_stage(stage_name).status

            print "{0} Status {1}:  {2}".format(i, stage_name, stage_status)
            # An optimization if the stage is done
            if stage_status is StageStatus.DONE or stage_status is StageStatus.FAILED:
                break
        # If status is not done after waiting for the job, exception is raised
        c = Calculation.from_exisiting(calc_id)
        if expect_success is True:
            assert c.get_stage(stage_name).status is StageStatus.DONE
        else:
            assert c.get_stage(stage_name).status is StageStatus.FAILED

    def test_sample_calcspec(self):

        c = Calculation.new(**d1)
        scompy_dir = os.path.dirname(environment.__file__)
        sample_calcspec = os.path.join(scompy_dir, "misc", "sample_calc.py")
        import shutil
        target_calcspec = os.path.join(c.path, environment.get_calcspec_internal_path())
        shutil.copy(sample_calcspec, target_calcspec)
        c.go()


        self.wait_for_stage(c.id, "stage1", 1)
        self.wait_for_stage(c.id, "stage2", 30)
        self.wait_for_stage(c.id, "stage3", 60)
