#!/usr/bin/python

import os
import socket

import unittest

from nose.tools import *
import nose.tools

from scompy.structures import CalcRecord, StageRecord, StageStatus
from scompy.environment import Environment
from scompy.core import now, HierarchyError, NODE_SEPARATOR, CalculationRemoveError
from tests.utils import CalcEnvTest

calc_d1 = dict(
    title="title1",
    description="description 1",
    uid=1,
    path="path1",
    time_create=now(),
)
calc_d2 = dict(
    title="title2",
    description="description 2",
    uid=2,
    path="path2",
    time_create=now(),
)

stage_d1 = dict(
    status=StageStatus.DONE,
    rm_job="rm job 1",
    time_submit=now(),
    time_begin=now(),
    time_end=now(),
)
stage_d2 = dict(
    status=StageStatus.RUNNING,
    rm_job="rm job 2",
    time_submit=None,
    time_begin=None,
    time_end=None,
)


class TestDB(CalcEnvTest):

    def test_insert_calc(self):
        c1 = CalcRecord(**calc_d1)
        assert self.env.db.insert_calc(c1) is c1
        c2 = self.env.db.get_calc(c1.id)
        assert_equal(c1, c2)

    def test_get_calc(self):
        c1 = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c1)
        c2 = self.env.db.get_calc(c1.id)
        assert_equal(c1, c2)
        c3 = self.env.db.get_calc(eval(repr(c1.id), {}, {}))
        assert_equal(c1, c3)

    def test_update_calc(self):
        c1 = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c1)
        for k, v in calc_d2.iteritems():
            setattr(c1, k, v)
        self.env.db.update_calc(c1)
        c2 = self.env.db.get_calc(c1.id)
        for k, v in calc_d2.iteritems():
            assert_equal(v, getattr(c2, k))

    def test_parents(self):
        p1 = CalcRecord(**calc_d1)
        self.env.db.insert_calc(p1)
        p2 = CalcRecord(**calc_d1)
        self.env.db.insert_calc(p2)

        c1 = CalcRecord(parent_id=p1.id, **calc_d1)
        self.env.db.insert_calc(c1)
        c2 = self.env.db.get_calc(c1.id)
        assert_equal(c1.parent_id, c2.parent_id)

        c1.parent_id = p2.id
        self.env.db.update_calc(c1)
        c2 = self.env.db.get_calc(c1.id)
        assert_equal(c1.parent_id, c2.parent_id)

    #@nottest
    def test_stages(self):
        name1 = "test_stage_1"
        name2 = "test_stage_2"

        # test inserting two new stages and getting them
        c = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c)
        sr1 = StageRecord(c.id, name1, **stage_d1)
        sr2 = StageRecord(c.id, name2, **stage_d2)
        assert self.env.db.set_stage(sr1) is sr1
        assert self.env.db.set_stage(sr2) is sr2
        stages = self.env.db.get_stages(c.id)
        assert_equal(stages, {name1: sr1, name2: sr2})

        # test inserting a stage for a non existing calculation
        assert_raises(ValueError, self.env.db.set_stage, StageRecord(c.id + 1, name1, **stage_d1))

        # test querying for stages of a non existing calculation
        assert_raises(ValueError, self.env.db.get_stages, c.id + 1)

        # test querying for stages of a calculation without any stages
        c2 = CalcRecord(**calc_d2)
        self.env.db.insert_calc(c2)
        stages = self.env.db.get_stages(c2.id)
        assert_equal(stages, {})

        # test updating an existing stage's status
        for k, v in stage_d2.iteritems():
            setattr(sr1, k, v)
        self.env.db.set_stage(sr1)
        sr3 = self.env.db.get_stages(sr1.calc_id)[sr1.name]
        for k, v in stage_d2.iteritems():
            assert_equal(v, getattr(sr2, k))

    #@nottest
    def test_delete_stage(self):
        name1 = "test_stage_1"
        name2 = "test_stage_2"

        # test inserting two new stages and getting them
        c = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c)
        sr1 = StageRecord(c.id, name1, **stage_d1)
        sr2 = StageRecord(c.id, name2, **stage_d2)
        assert self.env.db.set_stage(sr1) is sr1
        assert self.env.db.set_stage(sr2) is sr2
        stages = self.env.db.get_stages(c.id)
        assert_equal(stages, {name1: sr1, name2: sr2})

        assert self.env.db.delete_stage(sr1) is True
        stages = self.env.db.get_stages(c.id)
        assert name1 not in stages
        assert name2 in stages

    #@nottest
    def test_status_conversion(self):
        """
        test_db.TestDB.test_status_conversion

        Test that the status obtained from the database can be tested with is.

        """
        c = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c)
        s1 = StageRecord(c.id, "test_stage", **stage_d1)
        status_names = [n for n in dir(StageStatus) if not n.startswith('_')]
        for name in status_names:
            s1.status = getattr(StageStatus, name)
            self.env.db.set_stage(s1)
            s2 = self.env.db.get_stages(s1.calc_id)[s1.name]
            assert s2.status is getattr(StageStatus, name), "{0!r} is not {1!r}".format(
                s2.status, getattr(StageStatus, name))

    def test_all_calculations(self):
        titles = set("calculation #{0}".format(i) for i in range(100))
        for t in titles:
            c = CalcRecord(**calc_d1)
            c.title = t
            self.env.db.insert_calc(c)

        titles2 = list(c.title for c in self.env.db.all_calculations())
        assert len(titles2) == len(titles)
        assert set(titles2) == titles

    #@nottest
    def test_errors(self):
        print "get a non existing calc"
        assert_raises(ValueError, self.env.db.get_calc, 0)

        print "insert_calc with non existing parent"
        c1 = CalcRecord(**calc_d1)
        c1.parent_id = 0
        assert_raises(ValueError, self.env.db.insert_calc, c1)

        print "now insert_calc"
        c1.parent_id = None
        self.env.db.insert_calc(c1)

        print "update_calc non existing id"
        calc_id = c1.id
        c1.id = c1.id + 1
        assert_raises(ValueError, self.env.db.update_calc, c1)

        print "update_calc to recursive parent"
        c1.id = calc_id
        c1.parent_id = calc_id
        assert_raises(ValueError, self.env.db.update_calc, c1)

        print "update_calc to non existing parent"
        c1.parent_id = calc_id + 1
        assert_raises(ValueError, self.env.db.update_calc, c1)

    #@nottest
    def test_locking(self):
        print "adding few calculations"
        c1 = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c1)
        c2 = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c2)

        print "locking a new calculation"
        res = self.env.db.lock(c1.id)
        assert res is True

        li = self.env.db.get_lock_info(c1.id)
        print "Lock Info: ", li
        assert li.pid == os.getpid()
        assert li.host == socket.gethostname()

        print "getting all locked calculations"
        locked_calculations = self.env.db.locked_calculations()
        assert locked_calculations[0].id == c1.id

        print "locking the already locked calculation"
        res = self.env.db.lock(c1.id)
        assert res is False

        self.env.db.unlock(c1.id)
        li = self.env.db.get_lock_info(c1.id)
        print "Lock Info: ", li
        assert li is None

    #@nottest
    def test_hierarchy(self):
        assert len(list(self.env.db.list_node_children('/'))) == 0
        assert_raises(HierarchyError, self.env.db.list_node_children, '/non_existing')
        self._generate_tree(3, 3)
        c = set(str(i) for i in range(3))
        assert set(self.env.db.list_node_children('/')) == c
        assert set(self.env.db.list_node_children('/0')) == c
        assert set(self.env.db.list_node_children('/0/1')) == c
        assert set(self.env.db.list_node_children('/0/1/2')) == set([])

        assert_raises(HierarchyError, self.env.db.new_node, '/a/b/c')
        self.env.db.new_node('/a/b/c', True)
        assert_raises(HierarchyError, self.env.db.new_node, '/a/b')

        assert_raises(HierarchyError, self.env.db.move_node, '/a', '/A/AA')
        self.env.db.move_node('/a', '/A/AA', True)
        assert set(self.env.db.list_node_children('/A/AA/b')) == set(['c'])

        assert_raises(HierarchyError, self.env.db.remove_node, '/')
        assert_raises(HierarchyError, self.env.db.remove_node, '/', True)
        assert_raises(HierarchyError, self.env.db.remove_node, '/0/0')
        self.env.db.remove_node('/0/0/0')
        assert set(self.env.db.list_node_children('/0/0')) == set(['1', '2'])
        self.env.db.remove_node('/0/0/1')
        self.env.db.remove_node('/0/0/2')
        self.env.db.remove_node('/0/0')
        assert set(self.env.db.list_node_children('/0')) == set(['1', '2'])
        assert_raises(HierarchyError, self.env.db.remove_node, '/0/1')
        self.env.db.remove_node('/0/1', True)
        assert set(self.env.db.list_node_children('/0')) == set(['2'])

    #@nottest
    def test_calcs_and_hierarchy(self):
        c1 = CalcRecord(**calc_d1)
        c2 = CalcRecord(**calc_d2)
        c3 = CalcRecord(**calc_d1)
        self.env.db.new_node('/a/b/c', True)
        self.env.db.insert_calc(c1, 'a/b/c')
        self.env.db.insert_calc(c2, 'a/b')
        self.env.db.insert_calc(c3)

        assert self.env.db.get_calc_from_node('/a/b/c') == c1
        assert self.env.db.get_calc_from_node('/a/b') == c2
        assert self.env.db.get_calc_from_node('/a') is None
        assert self.env.db.get_node_from_calc(c1) == '/a/b/c'
        assert self.env.db.get_node_from_calc(c2) == '/a/b'
        assert self.env.db.get_node_from_calc(c3) is None

        self.env.db.remove_node('/a/b', True)
        assert self.env.db.get_node_from_calc(c1) is None
        assert self.env.db.get_node_from_calc(c2) is None

    #@nottest
    def test_remove_calc(self):
        self.env.db.new_node('/a/b/c', True)
        c1 = CalcRecord(**calc_d1)
        self.env.db.insert_calc(c1, '/a')
        c2 = CalcRecord(**calc_d2)
        c2.parent_id = c1.id
        self.env.db.insert_calc(c2, '/a/b')

        assert_raises(CalculationRemoveError, self.env.db.remove_calc, c1.id)
        self.env.db.remove_calc(c2.id)
        assert self.env.db.get_calc_from_node('/a/b') is None
        self.env.db.remove_calc(c1.id)
        assert self.env.db.get_calc_from_node('/a') is None
        assert_raises(ValueError, self.env.db.remove_calc, c1.id)

    def _generate_tree(self, levels, degree):
        position = ['']
        while len(position) > 0:
            path = NODE_SEPARATOR.join(position)
            n_children = len(list(self.env.db.list_node_children(path)))
            if n_children == degree or len(position) > levels:
                position.pop()
                continue
            position.append(str(n_children))
            self.env.db.new_node(NODE_SEPARATOR.join(position))
