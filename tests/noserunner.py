import subprocess
import nose
from ce_nose_plugins import RenameTables

if __name__ == '__main__':
    subprocess.check_call(['make', 'clean'])
    nose.main(addplugins=[RenameTables()])
