from distutils.core import setup
execfile("scompy/version.py")

# Make sure the structures file is there if not run makefile
import os
import subprocess
structures_file = os.path.join(os.path.dirname(__file__), "scompy", "structures.py")
if not os.path.isfile(structures_file):
    print "scompy/structures.py does not exists ... Generating"
    subprocess.check_call(["make", "structures"])

setup(
    name = "scompy",
    version=__version__,
    author="Oded Padon and Lior Amar",
    author_email="liororama@gmail.com",
    url="https://bitbucket.org/liororama/calc_env",
    packages=['scompy', 'scompy.externals', 'scompy.plugins'],
    scripts=['bin/scompy', 'bin/scompy-admin'],
    license='LICENSE.txt',
    description='Scientific COmputations Management in PYthon',
    long_description = open('README.txt').read(),
    install_requires = [
        "sqlalchemy >= 0.7.0",
    ],
    package_data={'scompy': ['misc/*.ini', 'misc/sample_calc.py'],
                  'scompy.plugins': ['slurm_runner.sh']
                    },
)
