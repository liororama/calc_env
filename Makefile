api_env = testers/api/ce/
mu_env = testers/api/multiuser_env/

.PHONY: test
test: scompy/structures.py
	env PYTHONPATH=./ python  tests/noserunner.py  -x -v --with-rename-tables -v tests

.PHONY: test-s
test-s: scompy/structures.py
	env PYTHONPATH=./ python  tests/noserunner.py  -x -s -v --with-rename-tables -v tests

.PHONY: structures
structures: scompy/structures.py

scompy/structures.py: scompy/generate_structures.py scompy/externals/recordtype.py
	python $< > $@
	python $@ # just to test for errors

.PHONY: clean
clean:
	@rm -vrf tests/test_runs
	@find . -name "*.pyc" | xargs rm -f
	sudo -i -u ceuser1 rm -rf calc_env_tests
	sudo -i -u ceuser2 rm -rf calc_env_tests
	rm -rf ~/calc_env_tests
	rm -rf build
	rm -rf dist

.PHONY: clean_all
clean_all: clean
	@rm -vf scompy/structures.py

.PHONY: init_api
init_api:
	rm -rf testers/api/ce/calculations
	env PYTHONPATH=./ python testers/api/init_api.py  $(api_env)

.PHONY: start_api
start_api:
	env PYTHONPATH=./ ipython -i testers/api/setup_api.py $(api_env)

.PHONY: init_mu
init_mu:
	env PYTHONPATH=./ python testers/api/init_api.py  $(mu_env)

.PHONY: start_mu
start_mu:
	ipython -i testers/api/setup_api.py $(mu_env)



.PHONY: help
help:
	@echo "Usage:"
	@echo "make init_api   : initialize api testing environment"
	@echo "make start_api  : start an ipython shell with calc_env api"
	@echo "make init_mu    : initialize multi user environment"
	@echo "make start_mu   : start an ipython shell with calcenv multiuser"
	@echo "make structures : generate the structures"
	@echo "make test       : (DEFAULT) run unit tests"
	@echo "make clean      : Remove files written during tests"
	@echo
